## 1.0.0

- Added support for SPHN Dataset Template 2025.1 while preserving backwards compatibility
- Added more robust handling of standard and valuesets
- Added a suite of input validation checks for project-specific dataset
- Fixed a bug when formatting named individuals
- Fixed a bug where `skos:note` was being missed in certain scenarios


## 0.9.0

- Added check to enforce cardinalities
- Parse terminology metadata from 'Coding System and Version' tab
- Added support to parse 'Metadata' tab
- Moved sphn core metadata to 'Metadata' tab
- Added some sensible checks for project and terminology metadata

## 0.8.0

- Improved logging of messages
- Fixed a bug with an implicit assumption of IRI with a fragment separator

## 0.7.0

- Added ability for generating project-specific RDF Schema
- Added ability for extendable valuesets
- Added checks to ensure that the input values from dataset are sanitized
- Added ability to parse cardinality directly from the concepts sheet
- Made parsing of cardinality pattern more robust

## 0.6.0

- Fixed bug where hasValue property was not being constrained to appropriate types
- Fixed bug where `@base` and default prefix was not bound in the output graph
- Fixed ontology base IRI in the output

## 0.5.0

- Added ability to parse deprecated concepts properly
- Added ability to incorporate pre-defined replaces annotations
- Removed bug where all concepts had the root concept as its domain
- Fixed bug where cardinalities of inherited concepts were not parsed
- Default to Code concept if a standard (and valueset) does not belong to the canonical standards defined in the config.yaml

## 0.4.0

- Move config.yaml to dataset2rdf module scope
- Fixed a bug where range was not being parsed properly for certain properties
- Fixed missing cardinality
- Use proper encoding when opening files
- Fixed imports in config
- Added root nodes for imports
- Added ability to use default config.yaml if none provided at runtime

## 0.3.0

- Added ability to define worksheet information in the config YAML
- Added ability to incorporate domain of a sub property to parent property
- Fixed a bug when parsing ranges for a property
- Fixed a bug when parsing parent of a property
- Fixed a bug when inferring domain of a property

## 0.2.0

- Improved how standard and valuesets are parsed for a given composedOf
- Made meaning binding more generic
- Fixed bugs
- Added suite of tests

## 0.1.0

- First release