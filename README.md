# Dataset2RDF

Converter to transform a SPHN Dataset into SPHN RDF schema.

## Installation

To install the dataset2rdf:

```sh
python setup.py install
```

To test whether the installation is successful you can try invoking `dataset2rdf` from the command line:

```sh
dataset2rdf --help
```

And you should see the Usage of the `dataset2rdf` tool:

```sh
Usage: dataset2rdf [OPTIONS]

Options:
  --dataset TEXT         Path to the SPHN Dataset (Excel spreadsheet) [required]
  --output TEXT          Output filename  [required]
  --project-output TEXT  Output filename for project specific TTL
  --config TEXT          Path to config.yaml
  --logfile TEXT         The file where the logs go to
  --debug TEXT           Debugging output enabled, boolean variable
  --extras TEXT          Any extra triples (as TTL files) to add to the output
  --help                 Show this message and exit.
```


For example,

```sh
dataset2rdf --dataset input/SPHN_Dataset.xlsx \
            --output output/sphn-schema.ttl \
            --config dataset2rdf/config.yaml
```

This should translate the `SPHN_Dataset.xlsx` into the RDF Schema.


# Extending the SPHN Dataset for a specific project

The dataset2rdf parses concepts and composedOfs defined in the SPHN Dataset.

Optionally, the SPHN Dataset can be extended for a project-specific use-case
by adding concepts and conposedOfs relevant for that particular project.

## Guidelines for extending the SPHN Dataset

One can add project-specific concepts and composedOfs to the SPHN Dataset
as new rows to the 'Concepts' worksheet in the SPHN Dataset XLSX.

Following are guidelines for adding concepts and/or composedOfs:

- Ensure that the 'release' column states the release version for the concept
- Ensure that the 'prefix' column has a unique prefix corresponding to the project
- Ensure that the 'active status (yes/no)' column is set to 'yes' if the concept
is to be treated as an active concept
- Ensure that you follow the instructions on [Generating New Concepts and Modifying Existing Ones](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_schema_generation.html#new-concepts-and-modification-of-existing-ones)
- When referring to a concept in the 'parent' column, be sure to prefix the concept with the project prefix if the parent concept is a project-specific concept

## Running dataset2rdf to generate project-specific RDF Schema

You can parse the SPHN Dataset to generate the SPHN RDF Schema TTL.

Alternatively, you can also parse the [SPHN Dataset Template](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tree/master/templates/dataset_template) to generate a project-specific RDF Schema TTL.

You can run the dataset2rdf tool with the SPHN Dataset Template as follows:

```sh
dataset2rdf --dataset SPHN_dataset_template_2023_3.xlsx \
    --output sphn-schema.ttl \
    --project-output project-schema.ttl
    --config dataset2rdf/config.yaml \
```

This would generate,
- `sphn-schema.ttl`: This is where all SPHN concepts and properties are defined; In almost all the cases, this file should be identical to the official **SPHN RDF Schema**
- `project-schema.ttl`: This is where all project-specific concepts and properties are defined. This is the project-specific RDF Schema


**Note:** Before running dataset2rdf with the SPHN Dataset Template be sure to update the 'Metadata' tab of the template with missing information. See [Generate a project-specific RDF Schema](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_schema_generation.html#add-project-s-metadata) guide for more information.


## License

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics.

This software is licensed under the GNU General Public License version 3 (see [License](https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf/-/blob/main/LICENSE)).
