# Dataset2RDF

Converter to transform a SPHN Dataset into SPHN RDF schema.

## Installation

To install the dataset2rdf:

```sh
python setup.py install
```

To test whether the installation is successful you can try invoking `dataset2rdf` from the command line:

```sh
dataset2rdf --help
```

And you should see the Usage of the `dataset2rdf` tool:

```sh
Usage: dataset2rdf [OPTIONS]

Options:
  --dataset TEXT  Path to the SPHN Dataset (Excel spreadsheet)  [required]
  --output TEXT   Output filename  [required]
  --config TEXT   Path to config.yaml
  --extras TEXT   Any extra triples (as TTL files) to add to the output
  --help          Show this message and exit.
```


## License

© Copyright 2023, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics.

This software is licensed under the GNU General Public License version 3 (see [License](https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf/-/blob/main/LICENSE)).
