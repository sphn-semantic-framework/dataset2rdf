# Installation

You can install the dataset2rdf tool as follows.

## Clone and install

First you should clone the dataset2rdf repository from: https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf

```sh
git clone https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf
```

Then you can install the tool directly via:

```sh
cd dataset2rdf
python setup.py install
```

Alternatively, you can also use `pip` to install:

```sh
pip install -e .
```

> The pip install is recommended for Developers.


## Clone, setup a virtual environment, and install

First you should clone the dataset2rdf repository from: https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf

```sh
git clone https://git.dcc.sib.swiss/sphn-semantic-framework/dataset2rdf
```

`cd` into the `dataset2rdf` directory:

```sh
cd dataset2rdf
```

Then set up a virtual environment as follows:

```sh
python3 -m venv env
```

This should create an `env` folder that represents your Python virtual environment.

You can activate this virtual environment as follows:

```sh
source env/bin/activate
```

> To deactivate, you can use the following:
> ```sh
> source env/bin/deactivate
> ```

Finally, you can install the tool inside your virtual environment:

```sh
python setup.py install
```

Alternatively, you can also use `pip` to install:

```sh
pip install -e .
```

> The pip install is recommended for Developers.