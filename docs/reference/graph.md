# OntologyGraph

`dataset2rdf.graph.OntologyGraph` is an abstraction for a graph that represents concepts, properties, cardinalities, and value sets.

The `OntologyGraph` class provides convenience methods for building and working with the graph.


::: dataset2rdf.graph
    options:
        heading_level: 3
        show_root_heading: false
        show_root_full_path: true