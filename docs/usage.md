# Usage

You can run the Dataset2RDF tool via the CLI or as a dependency in Python.

## Running Dataset2RDF via CLI

You can invoke the `dataset2rdf` via the command line:

```sh
dataset2rdf --help

Usage: dataset2rdf [OPTIONS]

Options:
  --dataset TEXT  Path to the SPHN Dataset (Excel spreadsheet)  [required]
  --output TEXT   Output filename  [required]
  --config TEXT   Path to config.yaml
  --extras TEXT   Any extra triples (as TTL files) to add to the output
  --help          Show this message and exit.
```

To convert a SPHN Dataset to its corresponding SPHN RDF Schema:

```sh
dataset2rdf --dataset input/SPHN_Dataset.xlsx \
            --output output/sphn-ontology.ttl \
            --config config.yaml
```

The above command should generate `sphn-ontology.ttl` in the `output` folder. This generated TTL file represents the RDF iterpretation of the input SPHN Dataset.

