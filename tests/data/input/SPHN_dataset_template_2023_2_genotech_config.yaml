prefix: sphn
title: The SPHN RDF Schema
description: The RDF schema describing concepts defined in the official SPHN dataset
license: https://creativecommons.org/licenses/by/4.0/
version: 2023.2
prior_version: 2022.2
copyright: "© Copyright 2023, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics"
create_root_node: true
root_node:
  - SPHNConcept
worksheet:
  concepts: Concepts
  project_metadata: Metadata
iri:
  canonical_iri: https://biomedit.ch/rdf/sphn-ontology/sphn#
  versioned_iri: https://biomedit.ch/rdf/sphn-ontology/sphn/2023/2
imports:
  ATC:
    prefix: atc
    iri:
      canonical_iri: https://www.whocc.no/atc_ddd_index/?code=
  SPHN-ATC:
    prefix: sphn-atc
    iri: 
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/atc/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/atc/2023/1
    create_root_node: true
    root_node:
      - ATC
  CHOP:
    prefix: chop
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/chop/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/chop/2023/4
    create_root_node: true
    root_node:
      - CHOP
  ICD-10-GM:
    prefix: icd-10-gm
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/icd-10-gm/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/3
    create_root_node: true
    root_node:
      - ICD-10-GM
  ICD-10:
    prefix: icd-10-gm
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/icd-10-gm/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/3
    create_root_node: true
    root_node:
      - ICD-10-GM
  LOINC:
    prefix: loinc
    iri:
      canonical_iri: https://loinc.org/rdf/
  SPHN-LOINC:
    prefix: sphn-loinc
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/loinc/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/loinc/2.74/1
    create_root_node: true
    root_node:
      - LOINC
  SNOMED CT:
    prefix: snomed
    iri:
      canonical_iri: http://snomed.info/id/
      versioned_iri: http://snomed.info/sct/900000000000207008/version/20221231
    create_root_node: true
    root_node:
      - snomed:138875005
  UCUM:
    prefix: ucum
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/ucum/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/ucum/2023/1
    create_root_node: true
    root_node:
      - UCUM
  SO:
    prefix: so
    iri:
      canonical_iri: http://purl.obolibrary.org/obo/SO_
      versioned_iri: http://purl.obolibrary.org/obo/so/2021-11-22/so.owl
  SPHN-SO:
    prefix: sphn-so 
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/so/
    create_root_node: true
    root_node:
      - SO
  GENO: 
    prefix: geno
    iri:
      canonical_iri: http://purl.obolibrary.org/obo/GENO_
      versioned_iri: http://purl.obolibrary.org/obo/geno/releases/2022-08-10/geno.owl
  SPHN-GENO:
    prefix: sphn-geno
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/geno/
    create_root_node: true
    root_node:
      - GENO
  HGNC:
    prefix: hgnc
    iri:
      canonical_iri: https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/
  SPHN-HGNC:
    prefix: sphn-hgnc
    iri:
      canonical_iri: https://biomedit.ch/rdf/sphn-resource/hgnc/
      versioned_iri: https://biomedit.ch/rdf/sphn-resource/hgnc/20221107
    create_root_node: true
    root_node:
      - HGNC
concepts:
  SPHNConcept:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNConcept
    label: "SPHN Concept"
    description: "SPHN Concepts defined by the SPHN dataset"
  Terminology:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#Terminology
    label: "Terminology"
    description: "Terminology class for grouping external resources used in the SPHN project"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNConcept
  ValueSet:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#ValueSet
    label: "Value Set"
    description: "List of value sets provided by SPHN"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNConcept
  Comparator:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#Comparator
    label: "Comparator"
    description: "qualifier describing whether the value is the precise one or not"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#ValueSet
  DataRelease:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease
    label: "Data Release"
    description: "Metadata about the release of the data"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNConcept
    restrictions:
      https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease-hasExtractionDateTime:
        class_identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease
        property_identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasExtractionDateTime
        min_cardinality: 1
        max_cardinality: 1
      https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease-hasDataProviderInstitute:
        class_identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease
        property_identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasDataProviderInstitute
        min_cardinality: 1
        max_cardinality: 1
      https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease-http://purl.org/dc/terms/conformsTo:
        class_identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease
        property_identifier: http://purl.org/dc/terms/conformsTo
        min_cardinality: 1
        max_cardinality: 1
  Deprecated:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#Deprecated
    label: "Deprecated"
    description: "Deprecated classes of SPHN that existed in the previous version"
properties:
  SPHNAttributeObject:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeObject
    label: "SPHN attribute object"
    description: "SPHN object attribute defined by the dataset"
    type: objectProperty # Use the proper one
  SPHNAttributeDatatype:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeDatatype
    label: "SPHN attribute datatype"
    description: "SPHN datatype attribute defined by the dataset"
    type: dataProperty # Use the proper one
  hasLocation:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasLocation
    label: "has location"
    description: "statement about a physical location"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeObject
    range:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#Location
    type: objectProperty # Use the proper one
  hasExtractionDateTime:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasExtractionDateTime
    label: "has extraction datetime"
    description: "date of the extraction of the data"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#hasDateTime
    type: dataProperty
    domain:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#DataRelease
    range:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#temporal
  hasSubjectPseudoIdentifier:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasSubjectPseudoIdentifier
    label: "has subject pseudo identifier"
    description: "link to a pseudo code assigned as unique identifier to an individual by a data provider institute"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeObject
    range:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#SubjectPseudoIdentifier
  hasDataProviderInstitute:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasDataProviderInstitute
    label: "has data provider institute"
    description: "link to the unique business identification number (UID) of the healthcare institute providing the data"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeObject
    range:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#DataProviderInstitute
  hasAdministrativeCase:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasAdministrativeCase
    label: "has administrative case"
    description: "link to an interaction between an individual and healthcare provider(s) via an administrative case identifier for the purpose of providing healthcare service(s) or assessing the health status of an individual"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeObject
    range:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#AdministrativeCase
  replaces:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#replaces
    label: "replaces"
    description: "Property that links the new version of an SPHN class replacing an SPHN old class"
    type: annotationProperty
  hasSharedIdentifier:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#hasSharedIdentifier
    label: "has shared identifier"
    description: "identifier that is shared across data providers"
    parent: https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNAttributeDatatype
    domain:
      - https://biomedit.ch/rdf/sphn-ontology/sphn#SubjectPseudoIdentifier
      - https://biomedit.ch/rdf/sphn-ontology/sphn#Sample
    range:
      - http://www.w3.org/TR/2001/REC-xmlschema-2-20010502/#anyURI
    type: dataProperty
  subjectToDeIdentification:
    identifier: https://biomedit.ch/rdf/sphn-ontology/sphn#subjectToDeIdentification
    label: "Subject to De-Identification"
    description: "Property that marks a resource subject to de-identification rules at the time of data preparation"
    type: annotationProperty