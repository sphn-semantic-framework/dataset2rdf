from rdflib import XSD, Graph
from rdflib.term import URIRef, BNode, Literal
from rdflib import RDF, RDFS, SKOS, OWL, DC, DCTERMS
from pathlib import Path

from dataset2rdf.cli import main
from dataset2rdf.config import get_config
from dataset2rdf.models import Concept
from dataset2rdf.utils import get_iri_for_named_individual, load_coding_system_information, parse_coding_system_information


BASE_DIR = Path(__file__).parent.resolve()
DATA_DIR = BASE_DIR / '..' / 'data'


def test_sphn_dataset():
    input_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2023_2.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2023_2_config.yaml'
    output_filename = DATA_DIR / 'output' / 'SPHN_Dataset_2023_2_ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=None,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)

    graph = Graph()
    graph.parse(output_filename)

    sphn_concept = URIRef(config.iri.canonical_iri + 'SPHNConcept')
    valueset_concept = URIRef(config.iri.canonical_iri + 'ValueSet')
    ac_concept = URIRef(config.iri.canonical_iri + 'AdministrativeCase')
    assert (ac_concept, RDF.type, OWL.Class) in graph
    assert (ac_concept, RDFS.subClassOf, sphn_concept) in graph
    assert (ac_concept, RDFS.label, Literal("Administrative Case")) in graph
    #assert (ac_concept, RDFS.comment, Literal("administrative artefact for billing according to Swiss DRG")) in graph
    assert (ac_concept, SKOS.definition, Literal("administrative artefact for billing according to Swiss DRG")) in graph


    results = graph.query("""

        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>

        SELECT DISTINCT ?s
        WHERE {
            ?s rdfs:subClassOf+ sphn:SPHNConcept .
        }

    """)

    concepts = []
    for row in results:
        concepts.append(row[0])
    assert len(set(concepts)) == 115

    results = graph.query("""

        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>

        SELECT DISTINCT ?s
        WHERE {
            ?s rdfs:subPropertyOf*/rdf:type owl:ObjectProperty .
        }

    """)
    object_properties = []
    for row in results:
        object_properties.append(row[0])
    assert len(object_properties) == 106

    results = graph.query("""

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>

    SELECT DISTINCT ?s
    WHERE {
        ?s rdfs:subPropertyOf*/rdf:type owl:DatatypeProperty .
    }

    """)
    datatype_properties = []
    for row in results:
        datatype_properties.append(row[0])
    assert len(datatype_properties) == 52

    individuals = [x for x in graph.subjects(RDF.type, OWL.NamedIndividual)]
    assert len(individuals) == 150

    bn = [x for x in graph.objects(ac_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        assert (node, RDF.type, OWL.Class) in graph
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            elements = flatten_rdf_list(graph, intersection_node)
            for element in elements:
                if (element, OWL.minCardinality, None) in graph:
                    min_cardinality = [x for x in graph.objects(element, OWL.minCardinality)]
                    assert min_cardinality
                if (element, OWL.maxCardinality, None) in graph:
                    max_cardinality = [x for x in graph.objects(element, OWL.maxCardinality)]
                    assert max_cardinality

    care_handling_concept = URIRef(config.iri.canonical_iri + 'CareHandling')
    valueset = {URIRef('http://snomed.info/id/394656005'), URIRef('http://snomed.info/id/371883000'), URIRef('http://snomed.info/id/304903009')}
    bn = [x for x in graph.objects(care_handling_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        assert (node, RDF.type, OWL.Class) in graph
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            elements = flatten_rdf_list(graph, intersection_node)
            for element in elements:
                assert (element, OWL.onProperty, URIRef(config.iri.canonical_iri + 'hasTypeCode')) in graph
                if (element, OWL.minCardinality, None) in graph:
                    min_cardinality = [x for x in graph.objects(element, OWL.minCardinality)]
                    assert min_cardinality
                if (element, OWL.maxCardinality, None) in graph:
                    max_cardinality = [x for x in graph.objects(element, OWL.maxCardinality)]
                    assert max_cardinality
                if (element, OWL.someValuesFrom, None) in graph:
                    assert (element, RDF.type, OWL.Restriction) in graph
                    svf_n = [x for x in graph.objects(element, OWL.someValuesFrom)]
                    uo_n = [x for x in graph.objects(svf_n[0], OWL.unionOf)]
                    values = flatten_rdf_list(graph, uo_n[0])
                    assert valueset == set(values)

    data_release_concept = URIRef(config.iri.canonical_iri + "DataRelease")
    expected_props = {
        URIRef(config.iri.canonical_iri + "hasExtractionDateTime"),
        URIRef(config.iri.canonical_iri + "hasDataProviderInstitute"),
        URIRef("http://purl.org/dc/terms/conformsTo")
    }
    bn = [x for x in graph.objects(data_release_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    props = set()
    for node in bn:
        assert (node, RDF.type, OWL.Class) in graph
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            assert len(restrictions) == 2
            for restriction in restrictions:
                #print_triples(graph, restriction)
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                props.add(prop)
                if (restriction, OWL.minCardinality, None) in graph:
                    min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                    assert min_cardinality
                if (restriction, OWL.maxCardinality, None) in graph:
                    max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                    assert max_cardinality
    assert expected_props == props

    # check named individuals
    adverse_event_consequences_collection = URIRef(config.iri.canonical_iri + "AdverseEvent_consequences")
    assert (adverse_event_consequences_collection, RDF.type, OWL.Class) in graph
    assert (adverse_event_consequences_collection, RDFS.subClassOf, valueset_concept) in graph
    values = ['CongenitalAbnormality', 'Death', 'HospitalisationOrProlongation', 'LifeThreatening', 'NoneOfTheConsequencesMentioned', 'PermanentDamageOrDisability', 'TemporarilySeriousImpactMedicallyImportant']
    iri_header = get_iri_for_named_individual(config.iri.canonical_iri)
    for value in values:
        value_uri = URIRef(iri_header + value)
        assert (value_uri, RDF.type, OWL.NamedIndividual) in graph
        assert (value_uri, RDF.type, adverse_event_consequences_collection) in graph

    has_coding_system_and_version_property = URIRef(config.iri.canonical_iri + "hasCodingSystemAndVersion")
    assert (has_coding_system_and_version_property, RDF.type, OWL.DatatypeProperty) in graph


def test_sphn_dataset_2024():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_release_2024_1_20240111.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_1_config.yaml'
    output_filename = DATA_DIR / 'output' / 'SPHN_Dataset_2024_1_ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=None,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)
    coding_system_df = load_coding_system_information(input_filename)
    coding_system_info = parse_coding_system_information(coding_system_df, config=config)

    graph = Graph()
    graph.parse(output_filename)

    terminology_concept_iri = URIRef(config.iri.canonical_iri + "Terminology")
    eco_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/eco/ECO")
    assert (eco_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['ECO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['ECO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, eco_root_iri) in graph

    edam_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/edam/EDAM")
    assert (edam_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['EDAM'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['EDAM'].iri.canonical_iri + node), RDFS.subClassOf, edam_root_iri) in graph

    efo_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/efo/EFO")
    assert (efo_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['EFO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['EFO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, efo_root_iri) in graph

    emdn_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/endm/EMDN")
    assert (efo_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph

    genepio_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/genepio/GENEPIO")
    assert (genepio_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['GENEPIO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['GENEPIO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, genepio_root_iri) in graph

    geno_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/geno/GENO")
    assert (geno_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['GENO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['GENO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, geno_root_iri) in graph

    hgnc_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/hgnc/HGNC")
    assert (hgnc_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph

    icd10gm_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/icd-10-gm/ICD-10-GM")
    assert (icd10gm_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph

    loinc_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/loinc/LOINC")
    assert (loinc_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph

    obi_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/obi/OBI")
    assert (obi_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['OBI'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['OBI'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, obi_root_iri) in graph

    ordo_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/ordo/ORDO")
    assert (ordo_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['ORDO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['ORDO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, ordo_root_iri) in graph

    root_nodes = coding_system_info['SNOMED CT'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['SNOMED CT'].iri.canonical_iri + node), RDFS.subClassOf, terminology_concept_iri) in graph

    so_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/so/SO")
    assert (so_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph
    root_nodes = coding_system_info['SO'].root_node
    for node in root_nodes:
        assert (URIRef(coding_system_info['SO'].iri.canonical_iri + node.split(':')[1]), RDFS.subClassOf, so_root_iri) in graph

    ucum_root_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/ucum/UCUM")
    assert (ucum_root_iri, RDFS.subClassOf, terminology_concept_iri) in graph

    attribute_datatype_iri = URIRef(config.iri.canonical_iri + "SPHNAttributeDatatype")
    has_shared_identifier_iri = URIRef(config.iri.canonical_iri + "hasSharedIdentifier")
    assert (has_shared_identifier_iri, RDF.type, OWL.DatatypeProperty) in graph
    assert (has_shared_identifier_iri, RDFS .subPropertyOf, attribute_datatype_iri) in graph
    assert (has_shared_identifier_iri, RDFS.label, Literal("has shared identifier")) in graph
    assert (has_shared_identifier_iri, SKOS.definition, Literal("identifier that is shared across data providers")) in graph
    assert (has_shared_identifier_iri, RDFS.range, XSD.anyURI) in graph

    # Microorganism Identification Result
    microorganism_identification_result_concept = URIRef(config.iri.canonical_iri + "MicroorganismIdentificationResult")
    assert (microorganism_identification_result_concept, RDF.type, OWL.Class) in graph
    expected_props = {
        URIRef(config.iri.canonical_iri + "hasNumericalReference"),
        URIRef(config.iri.canonical_iri + "hasCode"),
        URIRef(config.iri.canonical_iri + "hasStringValue"),
        URIRef(config.iri.canonical_iri + "hasOrganism"),
        URIRef(config.iri.canonical_iri + "hasTimeToPositivity"),
    }
    bn = [x for x in graph.objects(microorganism_identification_result_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    props = set()
    for node in bn:
        assert (node, RDF.type, OWL.Class) in graph
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            for restriction in restrictions:
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                props.add(prop)
                if (restriction, OWL.minCardinality, None) in graph:
                    min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                    assert min_cardinality is not None
                if (restriction, OWL.maxCardinality, None) in graph:
                    max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                    assert max_cardinality is not None
    assert expected_props == props

    # Microbiology Microscopy Result
    microbiology_microscopy_result_concept = URIRef(config.iri.canonical_iri + "MicrobiologyMicroscopyResult")
    assert (microbiology_microscopy_result_concept, RDF.type, OWL.Class) in graph
    expected_props = {
        URIRef(config.iri.canonical_iri + "hasStringValue"),
        URIRef(config.iri.canonical_iri + "hasQuantity"),
        URIRef(config.iri.canonical_iri + "hasCellMorphology"),
        URIRef(config.iri.canonical_iri + "hasNumericalReference"),
        URIRef(config.iri.canonical_iri + "hasCellOrganisation"),
        URIRef(config.iri.canonical_iri + "hasCellCountEstimateCode"),
        URIRef(config.iri.canonical_iri + "hasStainingResultCode"),
    }
    bn = [x for x in graph.objects(microbiology_microscopy_result_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    props = set()
    for node in bn:
        assert (node, RDF.type, OWL.Class) in graph
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            for restriction in restrictions:
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                props.add(prop)
                if (restriction, OWL.minCardinality, None) in graph:
                    min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                    assert min_cardinality is not None
                if (restriction, OWL.maxCardinality, None) in graph:
                    max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                    assert max_cardinality is not None
    assert expected_props == props

    # Check hasQuantity restriction for when there is a property path restriction
    # Radiotherapy Procedure
    radiotherapy_procedure_concept = URIRef(config.iri.canonical_iri + "RadiotherapyProcedure")
    has_radiation_quantity_iri = URIRef(config.iri.canonical_iri + "hasRadiationQuantity")
    assert (radiotherapy_procedure_concept, RDF.type, OWL.Class) in graph
    bn = [x for x in graph.objects(radiotherapy_procedure_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            for restriction in restrictions:
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                if prop == has_radiation_quantity_iri:
                    if (restriction, OWL.minCardinality, None) in graph:
                        min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                        assert str(min_cardinality) == "0"
                if (restriction, OWL.maxCardinality, None) in graph:
                    max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                    assert str(max_cardinality) == "1"
                if (restriction, OWL.someValuesFrom, None) in graph:
                    objects = [x for x in graph.objects(restriction, OWL.someValuesFrom)]
                    print(objects)

    # Check parent restrictions are pushed to child
    sample_concept = URIRef(config.iri.canonical_iri + "Sample")
    has_primary_container_iri = URIRef(config.iri.canonical_iri + "hasPrimaryContainer")
    has_fixation_type_iri = URIRef(config.iri.canonical_iri + "hasFixationType")
    assert (sample_concept, RDF.type, OWL.Class) in graph
    bn = [x for x in graph.objects(sample_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            for restriction in restrictions:
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                if prop == has_primary_container_iri:
                    if (restriction, OWL.minCardinality, None) in graph:
                        min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                        assert str(min_cardinality) == "0"
                    if (restriction, OWL.maxCardinality, None) in graph:
                        max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                        assert str(max_cardinality) == "1"
                    if (restriction, OWL.someValuesFrom, None) in graph:
                        objects = [x for x in graph.objects(restriction, OWL.someValuesFrom)]
                        assert len(objects) == 1
                        assert URIRef(config.iri.canonical_iri + "Sample_primaryContainer") in objects
                if prop == has_fixation_type_iri:
                    if (restriction, OWL.minCardinality, None) in graph:
                        min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                        assert str(min_cardinality) == "0"
                    if (restriction, OWL.maxCardinality, None) in graph:
                        max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                        assert str(max_cardinality) == "1"
                    if (restriction, OWL.someValuesFrom, None) in graph:
                        objects = [x for x in graph.objects(restriction, OWL.someValuesFrom)]
                        assert len(objects) == 1
                        assert URIRef(config.iri.canonical_iri + "Sample_fixationType") in objects

    # Check type restrictions are applied
    tumor_stage_assessment_event_concept = URIRef(config.iri.canonical_iri + "TumorStageAssessmentEvent")
    has_assessment_iri = URIRef(config.iri.canonical_iri + "hasAssessment")
    assert (tumor_stage_assessment_event_concept, RDF.type, OWL.Class) in graph
    bn = [x for x in graph.objects(tumor_stage_assessment_event_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(graph, intersection_node)
            for restriction in restrictions:
                assert (restriction, RDF.type, OWL.Restriction) in graph
                prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                if prop == has_assessment_iri:
                    if (restriction, OWL.minCardinality, None) in graph:
                        min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                        assert str(min_cardinality) == "1"
                    if (restriction, OWL.maxCardinality, None) in graph:
                        max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                        assert str(max_cardinality) == "1"
                    if (restriction, OWL.someValuesFrom, None) in graph:
                        objects = [x for x in graph.objects(restriction, OWL.someValuesFrom)]
                        assert len(objects) == 1
                        assert URIRef(config.iri.canonical_iri + "TumorStageAssessment") in objects

    # Check exclude type descendants
    electrocardiogram_concept = URIRef(config.iri.canonical_iri + "Electrocardiogram")
    note = Literal("For sphn:hasDataFile, instances of sphn:TimeSeriesDataFile are not allowed")
    assert (electrocardiogram_concept, RDF.type, OWL.Class) in graph
    assert (electrocardiogram_concept, SKOS.scopeNote, note) in graph

    reference_interpretation_concept = URIRef(config.iri.canonical_iri + "ReferenceInterpretation")
    note = Literal("For sphn:hasInput, instances of sphn:TumorGradeAssessmentResult, sphn:TumorStageAssessmentResult, sphn:VitalStatus are not allowed")
    assert (reference_interpretation_concept, RDF.type, OWL.Class) in graph
    assert (reference_interpretation_concept, SKOS.scopeNote, note) in graph

    # Check subjectToDeidentification annotations
    age_concept = URIRef(config.iri.canonical_iri + "Age")
    assert (age_concept, RDF.type, OWL.Class) in graph
    subject_to_deidentification_iri = URIRef(config.iri.canonical_iri + "subjectToDeIdentification")
    assert (subject_to_deidentification_iri, RDF.type, OWL.AnnotationProperty) in graph
    assert (age_concept, subject_to_deidentification_iri, Literal("true", datatype=XSD.boolean)) in graph

    # Check extendable valuesets
    oxygen_saturation_measurement_concept = URIRef(config.iri.canonical_iri + "OxygenSaturationMeasurement")
    note = Literal("sphn:hasBodySite/sphn:hasCode recommended values: SNOMED CT: 29707007 |Toe structure (body structure)|, SNOMED CT: 7569003 |Finger structure (body structure)|, SNOMED CT: 48800003 |Ear lobule structure (body structure)|")
    assert (oxygen_saturation_measurement_concept, RDF.type, OWL.Class) in graph
    assert (oxygen_saturation_measurement_concept, SKOS.note, note) in graph


def print_triples(graph, node):
    for s, p, o in graph.triples((node, None, None)):
        print(s, p, o)


def flatten_rdf_list(graph, node):
    elements = []
    element = [x for x in graph.objects(node, RDF.first)]
    elements.extend(element)
    rest = [x for x in graph.objects(node, RDF.rest)]
    if rest:
        elements.extend(flatten_rdf_list(graph, rest[0]))
    return elements
