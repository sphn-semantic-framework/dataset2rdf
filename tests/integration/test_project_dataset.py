import pytest
from rdflib import XSD, Graph
from rdflib.term import URIRef, BNode, Literal
from rdflib import RDF, RDFS, SKOS, OWL, DC, DCTERMS
from pathlib import Path

from dataset2rdf.cli import main
from dataset2rdf.config import get_config
from dataset2rdf.models import Concept
from dataset2rdf.validation_utils import get_namespace_iri
from dataset2rdf.utils import get_iri_for_named_individual


BASE_DIR = Path(__file__).parent.resolve()
DATA_DIR = BASE_DIR / '..' / 'data'


def test_genotech_project_dataset1():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-1.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-1.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    sphn_terminology_concept = URIRef(config.iri.canonical_iri + 'Terminology')
    project_terminology_concept = URIRef(config.project_metadata.iri.canonical_iri + 'Terminology')
    assert (sphn_terminology_concept, RDF.type, OWL.Class) in graph
    assert (project_terminology_concept, None, None) not in graph

    assert (project_terminology_concept, RDF.type, OWL.Class) in project_graph
    assert (project_terminology_concept, RDFS.label, Literal("Terminology")) in project_graph
    assert (project_terminology_concept, RDFS.label, None) in project_graph
    assert (project_terminology_concept, RDFS.subClassOf, sphn_terminology_concept) in project_graph

    data_release_concept = URIRef(config.iri.canonical_iri + "DataRelease")
    assert (data_release_concept, None, None) in graph
    assert (data_release_concept, None, None) not in project_graph


def test_genotech_project_dataset2():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_with_orpha.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-2.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-2.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    sphn_terminology_concept = URIRef(config.iri.canonical_iri + 'Terminology')
    project_terminology_concept = URIRef(config.project_metadata.iri.canonical_iri + "Terminology")
    orpha_resource_concept = URIRef("https://biomedit.ch/rdf/sphn-resource/orpha/ORPHA")

    assert (sphn_terminology_concept, RDF.type, OWL.Class) in graph
    assert (project_terminology_concept, None, None) not in graph

    assert (project_terminology_concept, RDF.type, OWL.Class) in project_graph
    assert (project_terminology_concept, RDFS.label, Literal("Terminology")) in project_graph
    assert (project_terminology_concept, RDFS.label, None) in project_graph
    assert (project_terminology_concept, RDFS.subClassOf, sphn_terminology_concept) in project_graph

    assert (orpha_resource_concept, RDFS.subClassOf, project_terminology_concept) in project_graph
    assert (orpha_resource_concept, None, None) not in graph

    genotech_cost_concept = URIRef(config.project_metadata.iri.canonical_iri + "Cost")
    has_rare_disease_code_property = URIRef(config.project_metadata.iri.canonical_iri + "hasRareDiseaseCode")
    bn = [x for x in project_graph.objects(genotech_cost_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        if (node, RDF.type, OWL.Restriction) in project_graph:
            assert (node, OWL.onProperty, has_rare_disease_code_property) in project_graph
            assert (node, OWL.someValuesFrom, orpha_resource_concept) in project_graph

    data_release_concept = URIRef(config.iri.canonical_iri + "DataRelease")
    assert (data_release_concept, None, None) in graph
    assert (data_release_concept, None, None) not in project_graph


def test_metadata_from_metadata_sheet():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_example.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_example_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-3.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-3.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    sphn_iri = URIRef("https://biomedit.ch/rdf/sphn-ontology/sphn")
    assert (sphn_iri, DC.title, Literal("The SPHN RDF Schema")) in graph
    assert (sphn_iri, DC.description, Literal("The RDF schema describing concepts defined in the official SPHN dataset")) in graph
    assert (sphn_iri, DCTERMS.license, URIRef("https://creativecommons.org/licenses/by/4.0/")) in graph
    assert (sphn_iri, DC.rights, Literal("© Copyright 2023, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics")) in graph
    assert (sphn_iri, OWL.versionIRI, URIRef("https://biomedit.ch/rdf/sphn-ontology/sphn/2023/2")) in graph
    assert (sphn_iri, OWL.priorVersion, URIRef("https://biomedit.ch/rdf/sphn-ontology/sphn/2023/1")) in graph

    project_graph = Graph()
    project_graph.parse(project_output_filename)

    project_iri = URIRef(get_namespace_iri(config.project_metadata.iri.canonical_iri))
    assert (project_iri, DC.title, Literal("Genotech")) in project_graph
    assert (project_iri, DC.description, Literal("genotech")) in project_graph
    assert (project_iri, DCTERMS.license, URIRef("https://creativecommons.org/licenses/by/4.0/")) in project_graph
    assert (project_iri, DC.rights, Literal("Genotech copyright")) in project_graph
    assert (project_iri, OWL.versionIRI, URIRef(config.project_metadata.iri.versioned_iri)) in project_graph


def test_terminology_root_scenario1():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_with_terminology_root_nodes.xlsx'
    config_filename = DATA_DIR / 'input' / 'config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )
    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    sphn_iri = URIRef("https://biomedit.ch/rdf/sphn-ontology/sphn#")
    sphn_resource_iri = URIRef("https://biomedit.ch/rdf/sphn-resource/")
    genotech_iri = URIRef("https://www.biomedit.ch/rdf/sphn-ontology/genotech/")
    genotech_resource_iri = URIRef("https://biomedit.ch/rdf/genotech-resource/")

    atc_root_iri = sphn_resource_iri + "atc" + "/ATC"
    assert (atc_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    icd_root_iri = sphn_resource_iri + "icd-10-gm" + "/ICD-10-GM"
    assert (icd_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    chop_root_iri = sphn_resource_iri + "chop" + "/CHOP"
    assert (chop_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    loinc_root_iri = sphn_resource_iri + "loinc" + "/LOINC"
    assert (loinc_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    ucum_root_iri = sphn_resource_iri + "ucum" + "/UCUM"
    assert (ucum_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    hgnc_root_iri = sphn_resource_iri + "hgnc" + "/HGNC"
    assert (hgnc_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    snomed_base_iri = URIRef("http://snomed.info/id/")
    snomed_root_iri = snomed_base_iri + "138875005"
    assert (snomed_root_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    geno_roots = ["GENO_0000536", "GENO_0000614", "GENO_0000701", "GENO_0000713", "GENO_0000815", "GENO_0000897", "GENO_0000904", "GENO_0000921", "GENO_0000965"]
    geno_base_iri = URIRef("http://purl.obolibrary.org/obo/")
    geno_root_iris = [geno_base_iri + x for x in geno_roots]
    geno_grouping_node_iri = sphn_resource_iri + "geno" + "/GENO"
    for iri in geno_root_iris:
        assert (iri, RDFS.subClassOf, geno_grouping_node_iri) in graph
    assert (geno_grouping_node_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    so_roots = ["SO_0000110", "SO_0000400", "SO_0001060", "SO_0001260"]
    so_base_iri = URIRef("http://purl.obolibrary.org/obo/")
    so_root_iris = [so_base_iri + x for x in so_roots]
    so_grouping_node_iri = sphn_resource_iri + "so" + "/SO"
    for iri in so_root_iris:
        assert (iri, RDFS.subClassOf, so_grouping_node_iri) in graph
    assert (so_grouping_node_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    hp_root_iri = URIRef("http://purl.obolibrary.org/obo/HP_0000001")
    hp_grouping_node_iri = sphn_resource_iri + "hp" + "/HP"
    assert not (hp_root_iri, RDFS.subClassOf, hp_grouping_node_iri) in graph
    assert not (hp_grouping_node_iri, RDFS.subClassOf, sphn_iri + "Terminology") in graph

    hp_grouping_node_iri = sphn_resource_iri + "hp" + "/HP"
    assert (hp_root_iri, RDFS.subClassOf, hp_grouping_node_iri) in project_graph
    assert (hp_grouping_node_iri, RDFS.subClassOf, genotech_iri + "Terminology") in project_graph
    assert (genotech_iri + "Terminology", RDFS.subClassOf, sphn_iri + "Terminology") in project_graph


def test_project_extension_scenario1():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech.xlsx'
    config_filename = DATA_DIR / 'input' / 'config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

def test_project_extension_scenario2():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_example_2.xlsx'
    config_filename = DATA_DIR / 'input' / 'config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
            dataset=input_filename,
            output=output_filename,
            project_output=project_output_filename,
            config=config_filename,
            logfile=None,
            debug=False,
            extras=()
    )
    project_graph = Graph()
    project_graph.parse(project_output_filename)
    project_iri = URIRef("https://biomedit.ch/rdf/sphn-ontology/genotech")
    ancestry_concept = project_iri + "#Ancestry"
    assert (ancestry_concept, RDF.type, OWL.Class) in project_graph
    valueset = {URIRef("http://purl.obolibrary.org/obo/HANCESTRO_0004")}
    bn = [x for x in project_graph.objects(ancestry_concept, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        assert (node, RDF.type, OWL.Class) in project_graph
        io_n = [x for x in project_graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            elements = flatten_rdf_list(project_graph, intersection_node)
            for element in elements:
                assert (element, OWL.onProperty, URIRef('https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode')) in project_graph
                if (element, OWL.minCardinality, None) in project_graph:
                    min_cardinality = [x for x in project_graph.objects(element, OWL.minCardinality)]
                    assert min_cardinality == [Literal("1", datatype=XSD.nonNegativeInteger)]
                if (element, OWL.maxCardinality, None) in project_graph:
                    max_cardinality = [x for x in project_graph.objects(element, OWL.maxCardinality)]
                    assert max_cardinality == [Literal("1", datatype=XSD.nonNegativeInteger)]
                if (element, OWL.someValuesFrom, None) in project_graph:
                    assert (element, RDF.type, OWL.Restriction) in project_graph
                    values = [x for x in project_graph.objects(element, OWL.someValuesFrom)]
                    assert valueset == set(values)


def test_project_dataset_violation1():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_test_violation_1.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-3.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-3.ttl'

    with pytest.raises(ValueError) as ex:
        try:
            main(
                dataset=input_filename,
                output=output_filename,
                project_output=project_output_filename,
                config=config_filename,
                logfile=None,
                debug=False,
                extras=()
            )
        except Exception as e:
            assert len(e.args) == 1
            raise

    assert ex
    errors = ex.value.args[0]
    assert len(errors) == 1
    assert "[Row 845] inherited composedOf 'product code' has cardinality which is broader (1:n) than what is expected (0:1)" in errors


def test_project_dataset_violation2():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_test_violation_2.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-3.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-3.ttl'

    with pytest.raises(ValueError) as ex:
        try:
            main(
                dataset=input_filename,
                output=output_filename,
                project_output=project_output_filename,
                config=config_filename,
                logfile=None,
                debug=False,
                extras=()
            )
        except Exception as e:
            assert len(e.args) == 1
            raise

    assert ex
    errors = ex.value.args[0]
    assert len(errors) == 2
    assert "[Row 840] Concept 'genotech:Cancer Diagnosis' has 'cardinality for concept to Subject Pseudo Identifier' which is broader (0:1) than its parent (1:1)" in errors
    assert "[Row 846] inherited composedOf 'code' has a standard 'CHEBI' that is not allowed since parent composedOf 'code' allows only SNOMED CT, ATC, GTIN" in errors


@pytest.mark.skip
def test_project_dataset_violation3():
    """
    TODO: To be implemented
    """
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_test_violation_3.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology-3.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology-3.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )


@pytest.mark.skip
def test_project_dataset_with_sensitive_tags():
    # This test is skipped until there is an appropriate template derived from 2024.1
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_with_sensitive_column.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )
    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    sensitive_annotation_property = URIRef(config.iri.canonical_iri + "subjectToDeIdentification")
    sensitive_concepts = {
        URIRef(config.iri.canonical_iri + "AdministrativeCase"),
    }
    for concept in sensitive_concepts:
        assert (concept, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) in graph
        assert (concept, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) not in project_graph

    sensitive_properties = {
        URIRef(config.iri.canonical_iri + "hasIdentifier"),
        URIRef(config.iri.canonical_iri +  "hasAdmissionDateTime"),
        URIRef(config.iri.canonical_iri + "hasDischargeDateTime"),
    }
    for property in sensitive_properties:
        assert (property, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) in graph
        assert (property, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) not in project_graph

    sensitive_project_concepts = {
        URIRef(config.iri.canonical_iri + "hasCodingDateTime"),
    }
    for concept in sensitive_project_concepts:
        assert (concept, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) not in graph
        assert (concept, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) in project_graph

    sensitive_project_properties = {
        URIRef(config.iri.canonical_iri + "hasRecordDateTime"),
    }
    for property in sensitive_project_properties:
        assert (property, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) not in graph
        assert (property, sensitive_annotation_property, Literal(True, datatype=XSD.boolean)) in project_graph


@pytest.mark.skip
def test_project_dataset_with_source_system_cardinalities():
    # Test skipped; need a better dataset template that is derived off of 2024.1
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_with_source_system.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )
    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    source_system_property = URIRef(config.iri.canonical_iri + "hasSourceSystem")
    concept_list = [
        URIRef(config.iri.canonical_iri + "AccessDevicePresence"),
        URIRef(config.iri.canonical_iri + "AdministrativeCase"),
        URIRef(config.iri.canonical_iri + "AdministrativeGender")
    ]

    for concept in concept_list:
        bn = [x for x in graph.objects(concept, RDFS.subClassOf) if isinstance(x, BNode)]
        for node in bn:
            io_n = [x for x in graph.objects(node, OWL.intersectionOf)]
            for intersection_node in io_n:
                restrictions = flatten_rdf_list(graph, intersection_node)
                assert restrictions
                for restriction in restrictions:
                    assert (restriction, RDF.type, OWL.Restriction) in graph
                    prop = next(graph.triples((restriction, OWL.onProperty, None)))[2]
                    if prop == source_system_property:
                        if (restriction, OWL.minCardinality, None) in graph:
                            min_cardinality = next(graph.triples((restriction, OWL.minCardinality, None)))[2]
                            assert min_cardinality
                        if (restriction, OWL.maxCardinality, None) in graph:
                            max_cardinality = next(graph.triples((restriction, OWL.maxCardinality, None)))[2]
                            assert max_cardinality


def test_project_dataset_roots():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_concepts.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    config = get_config(config_filename)
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    genotech_root_concept = URIRef(config.project_metadata.iri.canonical_iri + "GENOTECHConcept")
    genotech_ao_root_concept = URIRef(config.project_metadata.iri.canonical_iri + "GENOTECHAttributeObject")
    genotech_adt_root_concept = URIRef(config.project_metadata.iri.canonical_iri + "GENOTECHAttributeDatatype")

    assert (genotech_root_concept, None, None) not in graph
    assert (genotech_ao_root_concept, None, None) not in graph
    assert (genotech_adt_root_concept, None, None) not in graph

    assert (genotech_root_concept, RDF.type, OWL.Class) in project_graph
    assert (genotech_root_concept, RDFS.subClassOf, URIRef(config.iri.canonical_iri + "SPHNConcept")) in project_graph

    assert (genotech_ao_root_concept, RDF.type, OWL.ObjectProperty) in project_graph
    assert (genotech_ao_root_concept, RDFS.subPropertyOf, URIRef(config.iri.canonical_iri + "SPHNAttributeObject")) in project_graph

    assert (genotech_adt_root_concept, RDF.type, OWL.DatatypeProperty) in project_graph
    assert (genotech_adt_root_concept, RDFS.subPropertyOf, URIRef(config.iri.canonical_iri + "SPHNAttributeDatatype")) in project_graph


def test_project_dataset_with_named_individuals1():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_2_genotech_concepts_2.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2023_3_genotech_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    # check named individuals
    valueset_concept = URIRef("https://biomedit.ch/rdf/sphn-ontology/genotech#ValueSet")
    observation_collection = URIRef("https://biomedit.ch/rdf/sphn-ontology/genotech#RareDiseaseDiagnosis_observation")
    assert (observation_collection, RDF.type, OWL.Class) not in graph
    assert (observation_collection, RDF.type, OWL.Class) in project_graph
    assert (observation_collection, RDFS.subClassOf, valueset_concept) not in graph 
    assert (observation_collection, RDFS.subClassOf, valueset_concept) in project_graph

    values = ['General', 'Specific', 'Unclear']
    iri_header = get_iri_for_named_individual("https://biomedit.ch/rdf/sphn-ontology/genotech#")
    for value in values:
        value_uri = URIRef(iri_header + value)
        assert (value_uri, RDF.type, OWL.NamedIndividual) not in graph
        assert (value_uri, RDF.type, OWL.NamedIndividual) in project_graph
        assert (value_uri, RDF.type, observation_collection) not in graph
        assert (value_uri, RDF.type, observation_collection) in project_graph


def test_project_dataset_with_named_individuals2():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_genotech.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_1_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )
    graph = Graph()
    graph.parse(output_filename)
    project_graph = Graph()
    project_graph.parse(project_output_filename)

    sphn_valueset_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#ValueSet")
    project_valueset_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#ValueSet")
    sphn_attribute_object_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#SPHNAttributeObject")
    project_attribute_object_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#GENOTECHAttributeObject")
    sphn_attribute_datatype_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#SPHNAttributeDatatype")
    project_attribute_datatype_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#GENOTECHAttributeDatatype")

    assert (project_valueset_concept, RDFS.subClassOf, sphn_valueset_concept) in project_graph
    assert (project_valueset_concept, RDFS.label, Literal("Value Set")) in project_graph
    assert (project_valueset_concept, SKOS.definition, Literal("List of value sets provided by genotech project")) in project_graph

    assert (project_attribute_object_concept, RDFS.subPropertyOf, sphn_attribute_object_concept) in project_graph
    assert (project_attribute_object_concept,  RDFS.label, Literal("GENOTECHAttributeObject")) in project_graph
    assert (project_attribute_object_concept,  SKOS.definition, Literal("GENOTECHAttributeObject defined by the genotech project")) in project_graph

    assert (project_attribute_datatype_concept, RDFS.subPropertyOf, sphn_attribute_datatype_concept) in project_graph
    assert (project_attribute_datatype_concept,  RDFS.label, Literal("GENOTECHAttributeDatatype")) in project_graph
    assert (project_attribute_datatype_concept,  SKOS.definition, Literal("GENOTECHAttributeDatatype defined by the genotech project")) in project_graph

    collection_concept = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#Cost_category")
    assert (collection_concept, RDF.type, OWL.Class) in project_graph
    assert (collection_concept, RDFS.subClassOf, project_valueset_concept) in project_graph
    assert (collection_concept, RDFS.label, Literal("Cost category")) in project_graph
    assert (collection_concept, SKOS.definition, Literal("category of the concept")) in project_graph

    values = ['Primary', 'Secondary', 'Unclear']
    iri_header = get_iri_for_named_individual("https://biomedit.ch/rdf/sphn-schema/genotech#")
    for value in values:
        value_uri = URIRef(iri_header + value)
        assert (value_uri, RDF.type, OWL.NamedIndividual) in project_graph
        assert (value_uri, RDF.type, collection_concept) in project_graph
        assert (value_uri, RDFS.label, Literal(value)) in project_graph


def test_project_dataset_for_violations():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_with_errors.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'

    try:
        main(
            dataset=input_filename,
            output=output_filename,
            project_output=project_output_filename,
            config=config_filename,
            logfile=None,
            debug=False,
            extras=()
        )
    except ValueError as e:
        assert len(e.args[0]) == 50
        errors = set(e.args[0])
        assert "[Row 1506] Project concept 'genotech:Cost' occurs more than once (Row 1506, Row 1543)" in errors
        assert "[Row 1506] Project concept 'genotech:Cost' should have 'genotech:GENOTECHConcept' as its parent instead of 'SPHNConcept'" in errors
        assert "[Row 1507] Project composedOf 'genotech:value' has type 'quantitative'. Either 'value set or subset' should be defined OR the parent should be an Attribute Datatype" in errors
        assert "[Row 1508] Project composedOf 'genotech:currency code' should have 'genotech:GENOTECHAttributeObject' as its parent instead of 'SPHNAttributeObject'" in errors
        assert "[Row 1509] Project concept 'genotech:Rare Disease Diagnosis' has parent 'Diagnosis' with type 'Billed Diagnosis'; Type and parent should always refer to the same concept" in errors
        assert "[Row 1510] inherited composedOf 'code' has a standard 'ORDO or other' that is not allowed since parent composedOf 'code' allows only ICD-10-GM" in errors
        assert "[Row 1510] inherited composedOf 'code' has cardinality which is broader (1:n) than what is expected (1:1)" in errors
        assert "[Row 1511] inherited composedOf 'record datetime' has cardinality which is broader (0:n) than what is expected (0:1)" in errors
        assert "[Row 1513] Project composedOf 'genotech:cost' should have 'genotech:GENOTECHAttributeDatatype' as its parent instead of 'genotech:GENOTAttributeDatatype'" in errors
        assert "[Row 1514] Concept 'genotech:Imputed Rare Disease Diagnosis' has 'cardinality for concept to Source System' which is broader (0:1) than its parent (1:n)" in errors
        assert "[Row 1516] composedOf 'record datetime' has type (string) which is not in the same scope when comparing with composedOf 'record datetime' with type (temporal) in parent concept 'genotech:Rare Disease Diagnosis'" in errors
        assert "[Row 1518] inherited project composedOf 'genotech:cost' should have 'genotech:GENOTECHAttributeDatatype' as its parent instead of 'genotech:AttributeDatatype'" in errors
        assert "[Row 1521] inherited SPHN composedOf 'software' should have 'SPHNAttributeObject' as its parent instead of 'genotech:GENOTECHAttributeObject'" in errors
        #assert "[Row 1528] Concept 'genotech:Secondary Data Processing' inherits from genotech:PrimaryDataProcessing but all inherited composedOfs are not listed. The following parent composedOfs are missing: 'input', 'start datetime'" in errors
        assert "[Row 1528] Concept 'genotech:Secondary Data Processing' has 'cardinality for concept to Subject Pseudo Identifier' which is broader (0:n) than its parent (1:1)" in errors
        assert "[Row 1529] SPHN composedOf 'code' has an invalid IRI 'https://www.biomedit.ch/rdf/sphn-schema/genotech/2024/1#hasCode'. SPHN composedOf should have SPHN IRI" in errors
        assert "[Row 1529] inherited composedOf 'code' has a standard 'EDAM' that is not allowed since parent composedOf 'code' allows only OBI" in errors
        assert "[Row 1531] inherited SPHN composedOf 'input' has a reference 'genotech:SecondaryData Processing' that does not exist" in errors
        assert "[Row 1532] composedOf 'output' has type (Data Files) which is not in the same scope when comparing with composedOf 'output' with type (Data File) in parent concept 'genotech:Primary Data Processing'" in errors
        assert "[Row 1533] inherited SPHN composedOf 'start date time' has a parent 'hasDate Time' that does not exist" in errors
        assert "[Row 1533] inherited SPHN composedOf 'start date time' does not exist in parent concept 'genotech:Primary Data Processing'" in errors
        assert "[Row 1534] inherited SPHN composedOf 'quality control metric' should have 'SPHNAttributeObject' as its parent instead of 'SPHNAttributeDatatype'" in errors
        assert "[Row 1536] composedOf 'standard operating procedure' has type (Data File) which is not in the same scope when comparing with composedOf 'standard operating procedure' with type (Standard Operating Procedure) in parent concept 'genotech:Primary Data Processing'" in errors
        assert "[Row 1538] Repeated concept 'Software' has a composedOf 'name' with different values for column 'parent'" in errors
        assert "[Row 1538] SPHN composedOf 'name' has type 'string'. Either 'value set or subset' should be defined OR the parent should be an Attribute Datatype" in errors
        assert "[Row 1543] Project concept 'genotech:Cost' should have 'genotech:GENOTECHConcept' as its parent instead of 'genotech:GENOTECH Concept'" in errors
        assert "[Row 1543] Concept 'genotech:Cost' is repeated but with different values for column 'parent'" in errors
        assert "[Row 1544] SPHN concept 'Software' should have 'SPHNConcept' as its parent instead of 'SPHN Concept'" in errors
        assert "[Row 1544] Concept 'Software' is repeated but with different values for column 'meaning binding', 'parent'" in errors
        assert "[Row 1545] SPHN composedOf 'name' has an invalid IRI 'https://www.biomedit.ch/rdf/sphn-schema/genotech/2024/1#hasName'. SPHN composedOf should have SPHN IRI" in errors
        assert "[Row 1547] Repeated concept 'Software' has a composedOf 'uniform resource locator' with different values for column 'parent'" in errors
        assert "[Row 1547] SPHN composedOf 'uniform resource locator' has type 'string'. Either 'value set or subset' should be defined OR the parent should be an Attribute Datatype" in errors
        assert "[Row 1548] Repeated concept 'Software' has a composedOf 'version' with different values for column 'general description', 'parent', 'type'" in errors
        assert "[Row 1549] Repeated concept 'Software' has a composedOf 'genotech:dependency' with different values for column 'cardinality for composedOf'" in errors
        assert "[Row 1550] SPHN composedOf 'wrong parent' does not exist. If this is a project composedOf then the name should have a project prefix" in errors
        assert "[Row 1550] SPHN composedOf 'wrong parent' has an invalid IRI 'https://biomedit.ch/rdf/sphn-schema/genotech/2024/1#hasWrongParent'. SPHN composedOf should have SPHN IRI" in errors
        assert "[Row 1550] SPHN composedOf 'wrong parent' has type 'Assay'. Thus its parent should be an Attribute Object" in errors
        assert "[Row 1550] SPHN composedOf 'wrong parent' has type 'genotech:Secondary Data Processing'. Thus its parent should be an Attribute Object" in errors
        assert "[Row 1551] SPHN composedOf 'algorithm used' does not exist. If this is a project composedOf then the name should have a project prefix" in errors
        assert "[Row 1565] composedOf 'output' has type (Data File) which is not in the same scope when comparing with composedOf 'output' with type (genotech:GENOTECHConcept) in parent concept 'genotech:Specific Interpretation'" in errors
        assert "[Row 1568] inherited composedOf 'method code' renamed as 'measurement method code' in concept 'genotech:Automated Measurement'. But 'measurement method code' does not exist." in errors
        assert "[Row 1569] Project composedOf 'genotech:quantity' has a custom valueset 'unit -> code restricted to: %' but the property type is 'Quantity' instead of 'qualitative'." in errors
        assert "[Row 1569] Project composedOf 'genotech:quantity' has values restricted to a valueset 'unit -> code restricted to: %' but no 'standard' is provided." in errors
        assert "[Row 1577] concept 'Derived Automated Measurement' is expected to have an IRI 'https://www.biomedit.ch/rdf/sphn-schema/genotech/2024/1#DerivedAutomatedMeasurement' instead of 'https://www.biomedit.ch/rdf/sphn-schema/genotech/2024/1#AutomatedMeasurement'." in errors
        assert "[Row 1577] inherited composedOf 'method code' renamed as 'genotech:measurement method code' in concept 'genotech:Derived Automated Measurement'. Renaming an inherited SPHN composedOf to a project composedOf is not permitted."
        assert "[Row 1582] Project composedOf 'genotech:measurement method code' has values restricted to a valueset 'descendant of: 128927009 |Procedure by method (procedure)|' but no 'standard' is provided." in errors
        assert "[Row 1582] Project composedOf 'genotech:measurement method code' has a custom valueset 'descendant of: 128927009 |Procedure by method (procedure)|' but the property type is 'Code' instead of 'qualitative'." in errors


def test_project_dataset_scenario():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_1.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_2.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_3.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )


def test_project_dataset_with_exception():
    # Tests the exception added to accommodate the 2024.2-bugfix-cardinalities
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_4.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

def test_project_dataset_scenario_5():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_5.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    project_graph = Graph()
    project_graph.parse(project_output_filename)

    # genotech:Lung Function Test Interpretation Result Imputed
    concept_iri = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#LungFunctionTestInterpretationResultImputed")
    stat_code_property_iri = URIRef("https://biomedit.ch/rdf/sphn-schema/genotech#hasStatCode")
    assert (concept_iri, RDF.type, OWL.Class) in project_graph
    bn = [x for x in project_graph.objects(concept_iri, RDFS.subClassOf) if isinstance(x, BNode)]
    for node in bn:
        io_n = [x for x in project_graph.objects(node, OWL.intersectionOf)]
        for intersection_node in io_n:
            restrictions = flatten_rdf_list(project_graph, intersection_node)
            for restriction in restrictions:
                prop = next(project_graph.triples((restriction, OWL.onProperty, None)))[2]
                if prop == stat_code_property_iri:
                    if (restriction, OWL.minCardinality, None) in project_graph:
                        min_cardinality = next(project_graph.triples((restriction, OWL.minCardinality, None)))[2]
                        assert min_cardinality
                    if (restriction, OWL.maxCardinality, None) in project_graph:
                        max_cardinality = next(project_graph.triples((restriction, OWL.maxCardinality, None)))[2]
                        assert max_cardinality
                    if (restriction, OWL.someValuesFrom, None) in project_graph:
                        svf_n = next(project_graph.triples((restriction, OWL.someValuesFrom, None)))[2]
                        uo_n = next(project_graph.triples((svf_n, OWL.unionOf, None)))[2]
                        values = flatten_rdf_list(project_graph, uo_n)
                        assert URIRef("https://biomedit.ch/rdf/sphn-resource/icd-10-gm/ICD-10-GM") in values
                        assert URIRef("https://biomedit.ch/rdf/sphn-resource/spo/icd-o-3/ICD-O-3") in values


def test_project_dataset_scenario_6():
    input_filename = DATA_DIR / 'input' / 'SPHN_dataset_template_2024_2_project_scenarios_6.xlsx'
    config_filename = DATA_DIR / 'input' / 'SPHN_Dataset_2024_config.yaml'
    output_filename = DATA_DIR / 'output' / 'sphn-ontology.ttl'
    project_output_filename = DATA_DIR / 'output' / 'genotech-ontology.ttl'
    main(
        dataset=input_filename,
        output=output_filename,
        project_output=project_output_filename,
        config=config_filename,
        logfile=None,
        debug=False,
        extras=()
    )

    project_graph = Graph()
    project_graph.parse(project_output_filename)


def print_triples(graph, node):
    for s, p, o in graph.triples((node, None, None)):
        print(s, p, o)


def flatten_rdf_list(graph, node):
    elements = []
    element = [x for x in graph.objects(node, RDF.first)]
    elements.extend(element)
    rest = [x for x in graph.objects(node, RDF.rest)]
    if rest:
        elements.extend(flatten_rdf_list(graph, rest[0]))
    return elements
