import pytest
from dataset2rdf.models import Concept, Individual, Property, Restriction
from dataset2rdf.graph import OntologyGraph
from dataset2rdf.config import Config
from rdflib import RDF, RDFS, DC, DCTERMS, OWL, XSD, SKOS
from rdflib.term import URIRef, BNode, Literal


@pytest.mark.parametrize(
    "config",
    [
        (
            {
                "prefix": "ABC",
                "title": "Test ontology",
                "description": "Test ontology for testing purposes",
                "version": "123",
                "license": "https://creativecommons.org/licenses/by/4.0/",
                "iri": {
                    "canonical_iri": "https://example.org/",
                    "versioned_iri": "https://example.org/123",
                },
            }
        ),
        (
            {
                "prefix": "ABC",
                "title": "Test ontology",
                "description": "Test ontology for testing purposes",
                "version": "123",
                "license": "https://creativecommons.org/licenses/by/4.0/",
                "iri": {"canonical_iri": "https://example.org/"},
            }
        ),
        (
            {
                "prefix": "ABC",
                "title": "Test ontology",
                "description": "Test ontology for testing purposes",
                "version": "123",
                "prior_version": "001",
                "license": "https://creativecommons.org/licenses/by/4.0/",
                "copyright": "Copyright",
                "iri": {"canonical_iri": "https://example.org/"},
            }
        ),
        (
            {
                "prefix": "ABC",
                "title": "Test ontology",
                "description": "Test ontology for testing purposes",
                "version": "123",
                "prior_version": "001",
                "license": "https://creativecommons.org/licenses/by/4.0/",
                "copyright": "Copyright",
                "iri": {"canonical_iri": "https://example.org/"},
                "imports": {
                    "ATC": {
                        "prefix": "atc",
                        "iri": {"canonical_iri": "https://www.whocc.no/atc_ddd_index/?code="},
                        "create_root_node": "true",
                        "root_node": ["ATC"],
                    }
                },
            }
        ),
    ],
)
def test_graph_metadata(config):
    graph = OntologyGraph(config=Config(**config))
    assert len(graph.graph) > 0
    assert graph.namespace == config["iri"]["canonical_iri"]
    assert graph.version == config["version"]
    assert str(graph.version_IRI) == "https://example.org/123"

    for subject in graph.graph.objects(graph.namespace_IRI, DC.title):
        assert str(subject) == config["title"]
    for subject in graph.graph.objects(graph.namespace_IRI, DC.description):
        assert str(subject) == config["description"]
    for subject in graph.graph.objects(graph.namespace_IRI, OWL.versionIRI):
        assert subject == graph.version_IRI
    if "copyright" in config:
        for subject in graph.graph.objects(graph.namespace_IRI, DC.rights):
            assert str(subject) == config["copyright"]
    if "prior_version" in config:
        for subject in graph.graph.objects(graph.namespace_IRI, OWL.priorVersion):
            assert str(subject) == config["iri"]["canonical_iri"] + config["prior_version"]

    for prop in graph.ANNOTATION_PROPERTIES:
        assert (prop, RDF.type, OWL.AnnotationProperty) in graph.graph

    for data_type in graph.DATA_TYPES:
        assert (data_type, RDF.type, RDFS.Datatype) in graph.graph

    print(graph.graph.serialize())
    #print(graph.project_graph.serialize())

    if "imports" in config:
        ns_prefixes = []
        ns_iris = []
        for ns in graph.graph.namespaces():
            ns_prefixes.append(ns[0])
            ns_iris.append(str(ns[1]))
        for key in config["imports"]:
            prefix = config["imports"][key]["prefix"]
            assert prefix in ns_prefixes
            iri = config["imports"][key]["iri"]["canonical_iri"]
            assert iri in ns_iris
        if "root_node" in config["imports"][key]:
            root_node = config["imports"][key]["root_node"][0]
            root_node_iri = URIRef(config["imports"][key]["iri"]["canonical_iri"] + root_node)
            terminology_node_iri =   URIRef(config["iri"]["canonical_iri"] + "#" + "Terminology")
            assert (
                (root_node_iri, RDFS.subClassOf, terminology_node_iri)
            ) in graph.graph


def test_add_classes():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))
    concepts = {
        "https://example.org/#ConceptA": Concept(
            identifier="https://example.org/#ConceptA", label="Concept A", description="Concept A", parent="https://example.org/#Concept"
        ),
        "https://example.org/#ConceptB": Concept(
            identifier="https://example.org/#ConceptB", label="Concept B", description="Concept B", parent="https://example.org/#Concept"
        ),
        "https://example.org/#ConceptC": Concept(
            identifier="https://example.org/#ConceptC", label="Concept C", description="Concept C", parent="https://example.org/#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)
    subjects = [
        x
        for x in graph.graph.subjects(
            RDFS.subClassOf, graph.namespace_IRI + "Concept"
        )
    ]
    assert len(subjects) == 3


def test_add_properties():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))
    concepts = {
        "https://example.org/#ConceptA": Concept(
            identifier="https://example.org/#ConceptA", label="Concept A", description="Concept A", parent="https://example.org/#Concept"
        ),
        "https://example.org/#ConceptB": Concept(
            identifier="https://example.org/#ConceptB", label="Concept B", description="Concept B", parent="https://example.org/#Concept"
        ),
        "https://example.org/#ConceptC": Concept(
            identifier="https://example.org/#ConceptC", label="Concept C", description="Concept C", parent="https://example.org/#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org/#Property1": Property(
            identifier="https://example.org/#Property1",
            label="Property 1",
            description="Property 1",
            domain=[concepts["https://example.org/#ConceptA"]],
            range=["https://example.org/string"],
        ),
        "https://example.org/#Property2": Property(
            identifier="https://example.org/#Property2",
            label="Property 2",
            description="Property 2",
            domain=[concepts["https://example.org/#ConceptB"]],
            range=["https://example.org/temporal"],
        ),
        "https://example.org/#Property3": Property(
            identifier="https://example.org/#Property3",
            label="Property 3",
            description="Property 3",
            domain=[concepts["https://example.org/#ConceptA"]],
            range=[concepts["https://example.org/#ConceptC"]],
        ),
        "https://example.org/#Property4": Property(
            identifier="https://example.org/#Property4",
            label="Property 4",
            description="Property 4",
            domain=[concepts["https://example.org/#ConceptA"]],
            range=["https://example.org/quantitative"],
        ),
        "https://example.org/#Property5": Property(
            identifier="https://example.org/#Property5",
            label="Property 5",
            description="Property 5",
            domain=[concepts["https://example.org/#ConceptA"]],
            range=["https://example.org/qualitative"],
        ),
        "https://example.org/#hasDay": Property(
            identifier="https://example.org/#hasDay",
            label="has day",
            description="has day",
            domain=[concepts["https://example.org/#ConceptA"]],
            range=["https://example.org/temporal"],
        ),
        "https://example.org/#Property5.1": Property(
            identifier="https://example.org/#Property5.1",
            label="Property 5.1",
            description="Property 5.1",
            parent="https://example.org/#Property5",
        ),
        "https://example.org/#Property6": Property(
            identifier="https://example.org/#Property6",
            label="Property 6",
            description="Property 6",
            domain=[concepts["https://example.org/#ConceptA"], concepts["https://example.org/#ConceptB"]],
            range=["https://example.org/qualitative", "https://example.org/string"],
        ),
    }
    # TODO: Note that the assumption that the range IRIs will be be SPHN IRI is wrong and needs to be revisited.
    graph.add_properties(concepts=concepts, properties=properties)
    root_concept_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "SPHNConcept")
    conceptA_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "ConceptA")
    conceptB_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "ConceptB")
    conceptC_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "ConceptC")
    root_concept_iri = URIRef(config["iri"]["canonical_iri"] + "#" + config["root_node"][0])

    prop1_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property1")
    prop2_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property2")
    prop3_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property3")
    prop4_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property4")
    prop5_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property5")
    hasDay_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "hasDay")
    prop_5_1_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property5.1")
    prop6_iri = URIRef(config["iri"]["canonical_iri"] + "#" + "Property6")


    b_node = next(graph.graph.objects(prop1_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptA_iri in prop_domains
    assert root_concept_iri in prop_domains
    assert (prop1_iri, RDFS.range, XSD.string) in graph.graph

    b_node = next(graph.graph.objects(prop2_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptB_iri in prop_domains
    assert root_concept_iri in prop_domains
    assert (prop2_iri, RDFS.range, XSD.dateTime) in graph.graph

    b_node = next(graph.graph.objects(prop3_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptA_iri in prop_domains
    assert root_concept_iri in prop_domains
    assert (prop3_iri, RDFS.range, conceptC_iri) in graph.graph

    b_node = next(graph.graph.objects(prop4_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptA_iri in prop_domains
    assert root_concept_iri in prop_domains
    assert (prop4_iri, RDFS.range, XSD.double) in graph.graph

    b_node = next(graph.graph.objects(prop5_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptA_iri in prop_domains
    assert root_concept_iri in prop_domains
    assert (prop5_iri, RDFS.range, root_concept_iri) in graph.graph

    b_node = next(graph.graph.objects(hasDay_iri, RDFS.domain))
    assert (b_node, RDF.type, OWL.Class) in graph.graph
    assert (b_node, OWL.unionOf, None) in graph.graph
    prop_domains = flatten_rdf_list(graph.graph, next(graph.graph.objects(b_node, OWL.unionOf)))
    assert len(prop_domains) == 2
    assert conceptA_iri in prop_domains
    assert root_concept_iri in prop_domains
    # # TODO: the line below fails due to hardcoded SPHN IRIs
    # assert (hasDay_iri, RDFS.range, XSD.gDay) in graph.graph

    assert (prop_5_1_iri, RDFS.subPropertyOf, prop5_iri) in graph.graph

    bn = [x for x in graph.graph.objects(prop6_iri, RDFS.domain)][0]
    assert (bn, RDF.type, OWL.Class) in graph.graph
    un = [x for x in graph.graph.objects(bn, OWL.unionOf)][0]
    elements = flatten_rdf_list(graph.graph, un)
    assert conceptA_iri in elements
    assert conceptB_iri in elements

    bn = [x for x in graph.graph.objects(prop6_iri, RDFS.range)][0]
    assert (bn, RDF.type, OWL.Class) in graph.graph
    un = [x for x in graph.graph.objects(bn, OWL.unionOf)][0]
    elements = flatten_rdf_list(graph.graph, un)
    assert root_concept_iri in elements
    assert XSD.string in elements


def test_add_named_individuals():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))

    concepts = {
        "https://example.org#ConceptA": Concept(
            identifier="https://example.org#ConceptA", label="Concept A", description="Concept A", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptB": Concept(
            identifier="https://example.org#ConceptB", label="Concept B", description="Concept B", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptC": Concept(
            identifier="https://example.org#ConceptC", label="Concept C", description="Concept C", parent="https://example.org#Concept"
        ),
        "https://example.org#CollectionConcept": Concept(
            identifier="https://example.org#CollectionConcept",
            label="Collection Concept",
            description="Collection Concept",
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org#Property1": Property(
            identifier="https://example.org#Property1",
            label="Property 1",
            description="Property 1",
            domain=[concepts["https://example.org#ConceptA"]],
            range=["https://biomedit.ch/rdf/sphn-schema/sphn#string"],
        ),
        "https://example.org#Property2": Property(
            identifier="https://example.org#Property2",
            label="Property 2",
            description="Property 2",
            domain=[concepts["https://example.org#ConceptB"]],
            range=["https://biomedit.ch/rdf/sphn-schema/sphn#temporal"],
        ),
        "https://example.org#Property3": Property(
            identifier="https://example.org#Property3",
            label="Property 3",
            description="Property 3",
            domain=[concepts["https://example.org#ConceptA"]],
            range=[concepts["https://example.org#CollectionConcept"]],
        ),
    }
    graph.add_properties(concepts=concepts, properties=properties)

    individuals = {
        "https://example.org#Individual1": Individual(
            identifier="https://example.org#Individual1", label="Individual 1", description="Individual 1"
        ),
        "https://example.org#Individual2": Individual(
            identifier="https://example.org#Individual2", label="Individual 2", description="Individual 2"
        ),
        "https://example.org#IndividualA": Individual(
            identifier="https://example.org#IndividualA",
            label="Individual A",
            description="Individual A",
            type=["https://example.org#CollectionConcept"],
        ),
        "https://example.org#IndividualB": Individual(
            identifier="https://example.org#IndividualB",
            label="Individual B",
            description="Individual B",
            type=["https://example.org#CollectionConcept"],
        ),
    }
    graph.add_named_individuals(individuals=individuals)

    individual1_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "Individual1")
    individualA_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "IndividualA")
    individualB_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "IndividualB")
    collection_concept_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "CollectionConcept")

    # TODO: Because the IRIs are not the default SPHN IRI, the named individuals are written to project graph
    assert (individual1_iri, RDF.type, OWL.NamedIndividual) in graph.project_graph

    assert (individual1_iri, RDFS.label, Literal("Individual 1")) in graph.project_graph
    assert (individual1_iri, SKOS.definition, Literal("Individual 1")) in graph.project_graph

    assert (individualA_iri, RDF.type, OWL.NamedIndividual) in graph.project_graph
    assert (individualA_iri, RDF.type, collection_concept_iri) in graph.project_graph

    assert (individualB_iri, RDF.type, OWL.NamedIndividual) in graph.project_graph
    assert (individualB_iri, RDF.type, collection_concept_iri) in graph.project_graph


def test_add_restriction1():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))

    concepts = {
        "https://example.org#ConceptA": Concept(
            identifier="https://example.org#ConceptA", label="Concept A", description="Concept A", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptB": Concept(
            identifier="https://example.org#ConceptB", label="Concept B", description="Concept B", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptC": Concept(
            identifier="https://example.org#ConceptC", label="Concept C", description="Concept C", parent="https://example.org#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org#Property1": Property(
            identifier="https://example.org#Property1",
            label="Property 1",
            description="Property 1",
        ),
        "https://example.org#Property2": Property(
            identifier="https://example.org#Property2",
            label="Property 2",
            description="Property 2",
        ),
    }
    graph.add_properties(concepts=concepts, properties=properties)

    concept = concepts["https://example.org#ConceptA"]
    restriction1 = Restriction(
        class_identifier="https://example.org#ConceptA",
        property_identifier="https://example.org#Property1",
        some_values_from=["https://example.org/X", URIRef("https://example.org/Y")],
    )
    concept.restrictions["ConceptA-Property1"] = restriction1
    graph.apply_restrictions(concept=concept)

    conceptA_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "ConceptA")
    property1_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "Property1")

    # TODO: Because the IRIs are not the default SPHN IRI, the restrictions are written to project graph
    print(conceptA_iri)
    bn = [x for x in graph.project_graph.objects(conceptA_iri, RDFS.subClassOf) if isinstance(x, BNode)]
    assert (bn[0], RDF.type, OWL.Restriction) in graph.project_graph
    assert (bn[0], OWL.onProperty, property1_iri) in graph.project_graph

    svf_n = [x for x in graph.project_graph.objects(bn[0], OWL.someValuesFrom) if isinstance(x, BNode)]
    assert (svf_n[0], RDF.type, OWL.Class) in graph.project_graph

    un = [x for x in graph.project_graph.objects(svf_n[0], OWL.unionOf) if isinstance(x, BNode)]
    elements = flatten_rdf_list(graph.project_graph, un[0])
    assert URIRef("https://example.org/X") in elements
    assert URIRef("https://example.org/Y") in elements


def test_add_restriction2():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))

    concepts = {
        "https://example.org#ConceptA": Concept(
            identifier="https://example.org#ConceptA", label="Concept A", description="Concept A", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptB": Concept(
            identifier="https://example.org#ConceptB", label="Concept B", description="Concept B", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptC": Concept(
            identifier="https://example.org#ConceptC", label="Concept C", description="Concept C", parent="https://example.org#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org#Property1": Property(
            identifier="https://example.org#Property1",
            label="Property 1",
            description="https://example.org#Property 1",
        ),
        "https://example.org#Property2": Property(
            identifier="https://example.org#Property2",
            label="Property 2",
            description="https://example.org#Property 2",
        ),
    }
    graph.add_properties(concepts=concepts, properties=properties)

    concept = concepts["https://example.org#ConceptA"]
    restriction1 = Restriction(
        class_identifier="https://example.org#ConceptA",
        property_identifier="https://example.org#Property1",
        some_values_from=["https://example.org/X", URIRef("https://example.org/Y")],
        min_cardinality=0,
        max_cardinality=1,
    )
    concept.restrictions["ConceptA-Property1"] = restriction1
    graph.apply_restrictions(concept=concept)

    conceptA_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "ConceptA")
    property1_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#"  + "Property1")

    bn = [x for x in graph.project_graph.objects(conceptA_iri, RDFS.subClassOf) if isinstance(x, BNode)]
    assert (bn[0], RDF.type, OWL.Class) in graph.project_graph

    io_n = [x for x in graph.project_graph.objects(bn[0], OWL.intersectionOf) if isinstance(x, BNode)]
    restrictions = flatten_rdf_list(graph.project_graph, io_n[0])

    assert len(restrictions) == 3
    for res_node in restrictions:
        assert (res_node, RDF.type, OWL.Restriction) in graph.project_graph
        assert (res_node, OWL.onProperty, property1_iri) in graph.project_graph
        if (res_node, OWL.minCardinality, None) in graph.project_graph:
            assert (
                res_node,
                OWL.minCardinality,
                Literal(0, datatype=XSD.nonNegativeInteger),
            ) in graph.project_graph
        if (res_node, OWL.maxCardinality, None) in graph.project_graph:
            assert (
                res_node,
                OWL.maxCardinality,
                Literal(1, datatype=XSD.nonNegativeInteger),
            ) in graph.project_graph
        if (res_node, OWL.someValuesFrom, None) in graph.project_graph:
            svf_n = [
                x for x in graph.project_graph.objects(res_node, OWL.someValuesFrom) if isinstance(x, BNode)
            ]
            assert (svf_n[0], RDF.type, OWL.Class) in graph.project_graph

            uo_n = [x for x in graph.project_graph.objects(svf_n[0], OWL.unionOf) if isinstance(x, BNode)]
            elements = flatten_rdf_list(graph.project_graph, uo_n[0])
            assert URIRef("https://example.org/X") in elements
            assert URIRef("https://example.org/Y") in elements


def test_add_restriction3():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))

    concepts = {
        "https://example.org#ConceptA": Concept(
            identifier="https://example.org#ConceptA", label="Concept A", description="Concept A", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptB": Concept(
            identifier="https://example.org#ConceptB", label="Concept B", description="Concept B", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptC": Concept(
            identifier="https://example.org#ConceptC", label="Concept C", description="Concept C", parent="https://example.org#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org#Property1": Property(
            identifier="https://example.org#Property1",
            label="Property 1",
            description="Property 1",
        ),
        "https://example.org#Property2": Property(
            identifier="https://example.org#Property2",
            label="Property 2",
            description="Property 2",
        ),
    }
    graph.add_properties(concepts=concepts, properties=properties)

    property_path = ["https://example.org#subProperty1", "https://example.org#subProperty2", "https://example.org#subProperty3"]
    concept = concepts["https://example.org#ConceptA"]
    restriction1 = Restriction(
        class_identifier="https://example.org#ConceptA",
        property_identifier="https://example.org#Property1",
        min_cardinality=0,
        max_cardinality=1,
        property_paths={
            "https://example.org#ConceptA" + "-" + "https://example.org#Property1" + "-" + "-".join(property_path): {
                "property_path": property_path,
                "has_value": ["https://example.org/mg", "https://example.org/g", "https://example.org/kg"]
            }
        }
    )
    concept.restrictions["ConceptA-Property1"] = restriction1
    graph.apply_restrictions(concept=concept)

    print(graph.graph.serialize())
    print(graph.project_graph.serialize())

    conceptA_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "ConceptA")
    property1_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "Property1")

    bn = [x for x in graph.project_graph.objects(conceptA_iri, RDFS.subClassOf) if isinstance(x, BNode)]
    assert (bn[0], RDF.type, OWL.Class) in graph.project_graph

    io_n = [x for x in graph.project_graph.objects(bn[0], OWL.intersectionOf) if isinstance(x, BNode)]
    restrictions = flatten_rdf_list(graph.project_graph, io_n[0])

    assert len(restrictions) == 3
    for res_node in restrictions:
        assert (res_node, RDF.type, OWL.Restriction) in graph.project_graph
        assert (res_node, OWL.onProperty, property1_iri) in graph.project_graph
        if (res_node, OWL.minCardinality, None) in graph.project_graph:
            assert (
                res_node,
                OWL.minCardinality,
                Literal(0, datatype=XSD.nonNegativeInteger),
            ) in graph.project_graph
        if (res_node, OWL.maxCardinality, None) in graph.project_graph:
            assert (
                res_node,
                OWL.maxCardinality,
                Literal(1, datatype=XSD.nonNegativeInteger),
            ) in graph.project_graph
        if (res_node, OWL.someValuesFrom, None) in graph.project_graph:
            assert (res_node, RDF.type, OWL.Restriction) in graph.project_graph
            assert (res_node, OWL.onProperty, property1_iri) in graph.project_graph

            svf1_n = [
                x for x in graph.project_graph.objects(res_node, OWL.someValuesFrom) if isinstance(x, BNode)
            ]
            assert (svf1_n[0], RDF.type, OWL.Restriction) in graph.project_graph
            assert (
                svf1_n[0],
                OWL.onProperty,
                URIRef("https://example.org#subProperty1"),
            ) in graph.project_graph

            svf2_n = [
                x
                for x in graph.project_graph.objects(svf1_n[0], OWL.someValuesFrom)
                if isinstance(x, BNode)
            ]
            assert (svf2_n[0], RDF.type, OWL.Restriction) in graph.project_graph
            assert (
                svf2_n[0],
                OWL.onProperty,
                URIRef("https://example.org#subProperty2"),
            ) in graph.project_graph

            svf3_n = [
                x
                for x in graph.project_graph.objects(svf2_n[0], OWL.someValuesFrom)
                if isinstance(x, BNode)
            ]

            # TODO: The block below needs to be redone
            # assert (svf3_n[0], RDF.type, OWL.Class) in graph.project_graph
            # uo_n = [x for x in graph.project_graph.objects(svf3_n[0], OWL.unionOf) if isinstance(x, BNode)]
            # elements = flatten_rdf_list(graph.project_graph, uo_n[0])

            # values = []
            # for inner_res_node in elements:
            #     assert (inner_res_node, RDF.type, OWL.Restriction) in graph.project_graph
            #     assert (
            #         inner_res_node,
            #         OWL.onProperty,
            #         URIRef("https://example.org#subProperty3"),
            #     ) in graph.project_graph
            #     values.extend([x for x in graph.project_graph.objects(inner_res_node, OWL.hasValue)])
            # assert URIRef("https://example.org/mg") in values
            # assert URIRef("https://example.org/g") in values
            # assert URIRef("https://example.org/kg") in values


def test_add_restriction4():
    config = {
        "prefix": "ABC",
        "title": "Test ontology",
        "description": "Test ontology for testing purposes",
        "version": "123",
        "license": "https://creativecommons.org/licenses/by/4.0/",
        "root_node": ["SPHNConcept"],
        "iri": {
            "canonical_iri": "https://example.org/",
            "versioned_iri": "https://example.org/123",
        },
    }
    graph = OntologyGraph(config=Config(**config))

    concepts = {
        "https://example.org#ConceptA": Concept(
            identifier="https://example.org#ConceptA", label="Concept A", description="Concept A", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptB": Concept(
            identifier="https://example.org#ConceptB", label="Concept B", description="Concept B", parent="https://example.org#Concept"
        ),
        "https://example.org#ConceptC": Concept(
            identifier="https://example.org#ConceptC", label="Concept C", description="Concept C", parent="https://example.org#Concept"
        ),
    }
    graph.add_classes(concepts=concepts)

    properties = {
        "https://example.org#Property1": Property(
            identifier="https://example.org#Property1",
            label="Property 1",
            description="Property 1",
        ),
        "https://example.org#Property2": Property(
            identifier="https://example.org#Property2",
            label="Property 2",
            description="Property 2",
        ),
    }
    graph.add_properties(concepts=concepts, properties=properties)

    concept = concepts["https://example.org#ConceptA"]
    restriction1 = Restriction(
        class_identifier="https://example.org#ConceptA",
        property_identifier="https://example.org#Property1",
        min_cardinality=0,
        max_cardinality=1,
        notes=["This is a note regarding the property"],
        scope_notes=["This is a note regarding the scope of the property"],
    )
    concept.restrictions["ConceptA-Property1"] = restriction1
    graph.apply_restrictions(concept=concept)

    conceptA_iri = URIRef(config["iri"]["canonical_iri"][:-1] + "#" + "ConceptA")
    assert (
        conceptA_iri,
        SKOS.note,
        Literal("This is a note regarding the property"),
    ) in graph.project_graph
    assert (
        conceptA_iri,
        SKOS.scopeNote,
        Literal("This is a note regarding the scope of the property"),
    ) in graph.project_graph


def print_triples(graph, node):
    for s, p, o in graph.triples((node, None, None)):
        print(s, p, o)


def flatten_rdf_list(graph, node):
    elements = []
    element = [x for x in graph.objects(node, RDF.first)]
    elements.extend(element)
    rest = [x for x in graph.objects(node, RDF.rest)]
    if rest:
        elements.extend(flatten_rdf_list(graph, rest[0]))
    return elements
