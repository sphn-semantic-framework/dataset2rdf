from pathlib import Path
import pytest
from dataset2rdf.config import get_config
from dataset2rdf.models import Concept, Property, Restriction
from dataset2rdf.utils import (
    NO_SUBCLASSES,
    ONLY_DIRECT_SUBCLASSES,
    REGEX_MAP,
    add_named_individuals_as_restriction,
    get_reference,
    infer_domain,
    is_curie,
    merge_restriction,
    parse_active_composed_ofs,
    parse_active_concept,
    parse_active_concepts,
    parse_extendable_valueset,
    prepare_cardinalities,
    sanitize,
    parse_element,
    format_property_name,
    format_class_name,
    format_named_individual,
    extract_code,
    parse_standard_as_restriction,
    add_code_as_restriction,
)

BASE_DIR = Path(__file__).parent.resolve()
DATA_DIR = BASE_DIR / '..' / 'data'

config_file = DATA_DIR / 'input' / 'SPHN_Dataset_2024_1_config.yaml'
config = get_config(config_file)

@pytest.mark.parametrize(
    "input_string,expected_string",
    [("S​1", "S 1"), (" S2 ", "S2"), ("S3 ", "S3"), ("  1 2 3 ​", "1 2 3")],
)
def test_sanitize(input_string, expected_string):
    sanitized_string = sanitize(input_string)
    assert sanitized_string == expected_string


@pytest.mark.parametrize(
    "input_string,expected_bool",
    [
        ("ex:123​", True),
        ("123​", False),
        ("sphn:Code", True),
        ("Code", False),
        ("sphn:hasLaterality", True),
        ("hasLaterality", False),
    ],
)
def test_is_curie(input_string, expected_bool):
    status = is_curie(input_string)
    assert expected_bool == status


@pytest.mark.parametrize(
    "input_string,expected_string",
    [
        ("https://biomedit.ch/rdf/sphn-schema/sphn#hasCode", "hasCode"),
        ("https://biomedit.ch/rdf/sphn-schema/sphn#Diagnosis", "Diagnosis"),
        ("https://biomedit.ch/rdf/sphn-schema/sphn/genotech/Concept", "Concept"),
        ("https://biomedit.ch/rdf/sphn-schema/sphn/genotech/hasConcept", "hasConcept"),
    ],
)
def test_get_reference(input_string, expected_string):
    reference = get_reference(input_string)
    assert reference == expected_string


@pytest.mark.parametrize(
    "input_string,expected_string",
    [
        ("%", "percent"),
        ("[mm]", "sblmmsbr"),
        ("(ml)", "rblmlrbr"),
        ("<", "LessThan"),
        (">=", "GreaterThanOrEqual"),
        ("=", "Equal"),
        ("#", "nb"),
        ("{#}", "cblnbcbr"),
        ("kPa.s", "kPadots"),
        ("kPa.s/L", "kPadotsperL"),
        ("mg/kg", "mgperkg"),
        ("IU/kg", "IUperkg"),
        ("mmol/min/kPa", "mmolperminperkPa"),
        ("mmol/min/kPa/L", "mmolperminperkPaperL"),
        ("umol/(8.h)", "umolperrbl8dothrbr"),
        ("u[IU]/mL", "usblIUsbrpermL"),
        ("umol/10*6{RBC}", "umolper10exp6cblRBCcbr"),
        ("ug/[sft_i]", "ugpersblsft_isbr"),
        ("ug/L{d-dimer_unit}", "ugperLcbld-dimer_unitcbr"),
        ("uN.s/(cm5.m2)", "uNdotsperrblcm5dotm2rbr"),
        ("[tb'U]", "sbltbapoUsbr"),
        ("[in_i'H2O]", "sblin_iapoH2Osbr"),
        ("[anti'Xa'U]/mL", "sblantiapoXaapoUsbrpermL"),
        ("[ft_us]/[ft_us]", "sblft_ussbrpersblft_ussbr"),
        ("mg/d/{1.73_m2}", "mgperdpercbl1dot73_m2cbr"),
        ("g/(8.h){shift}", "gperrbl8dothrbrcblshiftcbr"),
        ("10*6.[CFU]/L", "10exp6dotsblCFUsbrperL")
    ],
)
def test_parse_element(input_string, expected_string):
    parsed_string = parse_element(input_string)
    assert parsed_string == expected_string


@pytest.mark.parametrize(
    "input_string,expected_string",
    [
        ("Body Site", "BodySite"),
        ("Drug Administration Event", "DrugAdministrationEvent"),
        ("ICD-O Diagnosis", "ICDODiagnosis"),
        ("Gestational Age At Birth", "GestationalAgeAtBirth"),
    ],
)
def test_format_class_name(input_string, expected_string):
    formatted_name = format_class_name(input_string)
    assert formatted_name == expected_string


@pytest.mark.parametrize(
    "input_string,expected_string",
    [
        ("datetime", "hasDateTime"),
        ("date time", "hasDateTime"),
        ("systolic pressure", "hasSystolicPressure"),
        ("subject pseudo identifier", "hasSubjectPseudoIdentifier"),
        ("coding system and version", "hasCodingSystemAndVersion"),
    ],
)
def test_format_property_name(input_string, expected_string):
    formatted_name = format_property_name(input_string)
    assert formatted_name == expected_string


@pytest.mark.parametrize(
    "input_string,expected_string",
    [
        ("Blood (whole)", "BloodWhole"),
        ("X-ray", "X-ray"),
        (
            "Temporarily serious impact / medically important",
            "TemporarilySeriousImpactMedicallyImportant",
        ),
        ("RECIST 1.0", "RECIST1dot0"),
        (
            "Cells from nonblood specimen type (e.g. dissociated tissue), nonviable",
            "CellsFromNonBloodSpecimenTypeNonViableTissue",
        ),
        (
            "Cells from nonblood specimen type (e.g. dissociated tissue), viable",
            "CellFromNonBloodSpecimenTypeViableTissue",
        ),
        ("24 h urine", "24hUrine"),
        ("Allprotect ® tissue reagent", "AllprotectTissueReagent"),
        ("(+) Extension, (-) Flexion", "PlusExtensionMinusFlexion"),
        ("Lumbar Flat, Hip 90° Flexed", "LumbarFlatHip90DegreesFlexed"),
        ("(+) Positive", "PlusPositive"),
        ("(-) Negative", "MinusNegative"),
        ("scale (with min-max)", "ScaleWithMinMax"),
        ("multiple-choice (single answer)", "MultipleChoiceSingleAnswer"),
        ("multiple-choice (multiple answers)", "MultipleChoiceMultipleAnswers"),
        ("yes/no", "YesNo")
    ],
)
def test_format_named_individual(input_string, expected_string):
    formatted_name = format_named_individual(input_string)
    assert formatted_name == expected_string


@pytest.mark.parametrize(
    "code,pattern,expected_string",
    [
        ("child of : 105590001 |Substance (substance)|", r"child of\s*:\s*(\d+)", "105590001"),
    ],
)
def test_extract_code(code, pattern, expected_string):
    extracted_code = extract_code(code, pattern)
    assert extracted_code == expected_string


@pytest.mark.parametrize(
    "concept,prop,standard,valueset,expected_codes,notes",
    [
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            "SNOMED CT",
            "child of: 105590001 |Substance (substance)|",
            ["http://snomed.info/id/105590001"],
            [f"sphn:hasValue {ONLY_DIRECT_SUBCLASSES}"],
        ),
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            "SNOMED CT",
            "child of: 105590001 |Substance (substance)|",
            ["http://snomed.info/id/105590001"],
            [f"sphn:hasValue {ONLY_DIRECT_SUBCLASSES}"],
        ),
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            "SNOMED CT",
            "7771000 |Left (qualifier value)|; 24028007 |Right (qualifier value)|; 51440002 |Right and left (qualifier value)|; 66459002 |Unilateral (qualifier value)|; 261665006 |Unknown (qualifier value)|",
            [
                "http://snomed.info/id/7771000",
                "http://snomed.info/id/24028007",
                "http://snomed.info/id/51440002",
                "http://snomed.info/id/66459002",
                "http://snomed.info/id/261665006",
            ],
            [f"sphn:hasValue {NO_SUBCLASSES}"],
        ),
    ],
)
def test_parse_standard_as_restriction(concept, prop, standard, valueset, expected_codes, notes):
    parse_standard_as_restriction(
        concept=concept,
        prop=prop,
        property_path=None,
        prefix="sphn",
        add_info=None,
        standard=standard,
        valueset=valueset,
        config=config
    )
    assert concept.restrictions
    restriction = list(concept.restrictions.values())[0]
    assert restriction.class_identifier == concept.identifier
    assert restriction.property_identifier == prop.identifier
    assert restriction.some_values_from
    for code in expected_codes:
        assert code in restriction.some_values_from
    assert restriction.scope_notes
    for note in notes:
        assert note in restriction.scope_notes


@pytest.mark.parametrize(
    "concept,prop,prop_path,codes,notes,scope_notes",
    [
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            [],
            ["http://snomed.info/id/105590001"],
            [],
            [f"sphn:hasValue {ONLY_DIRECT_SUBCLASSES}"],
        ),
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            [],
            [
                "http://snomed.info/id/7771000",
                "http://snomed.info/id/24028007",
                "http://snomed.info/id/51440002",
                "http://snomed.info/id/66459002",
                "http://snomed.info/id/261665006",
            ],
            [],
            [f"sphn:hasValue {NO_SUBCLASSES}"],
        ),
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            ["https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit", "https://biomedit.ch/rdf/sphn-schema/sphn#hasCode"],
            [
                "http://snomed.info/id/7771000",
                "http://snomed.info/id/24028007",
                "http://snomed.info/id/51440002",
                "http://snomed.info/id/66459002",
                "http://snomed.info/id/261665006",
            ],
            [],
            [f"sphn:hasValue {NO_SUBCLASSES}"],
        ),
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            [],
            [],
            [f"sphn:hasValue allowed coding system: SNOMED CT"],
            [],
        ),
    ],
)
def test_add_code_as_restriction(concept, prop, prop_path, codes, notes, scope_notes):
    restriction = add_code_as_restriction(
        concept=concept,
        prop=prop,
        prop_path=prop_path,
        codes=codes,
        notes=notes,
        scope_notes=scope_notes,
    )
    assert restriction
    assert restriction.class_identifier == concept.identifier
    assert restriction.property_identifier == prop.identifier
    if prop_path:
        assert restriction.property_paths
        key = f"{concept.identifier}-{prop.identifier}-{'-'.join(prop_path)}"
        assert key in restriction.property_paths
        ppr = restriction.property_paths[key]
        assert ppr.some_values_from
        for code in codes:
            assert code in ppr.some_values_from
    else:
        if codes:
            assert restriction.some_values_from
            for code in codes:
                assert code in restriction.some_values_from
    if scope_notes:
        assert restriction.scope_notes
        for note in scope_notes:
            assert note in restriction.scope_notes
    if notes:
        assert restriction.notes
        for note in notes:
            assert note in restriction.notes


@pytest.mark.parametrize(
    "concept,prop,collection_concept,scope_notes",
    [
        (
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept"),
            Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue"),
            Concept(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept_hasValue"),
            [],
        ),
    ],
)
def test_add_named_individuals_as_restriction(concept, prop, collection_concept, scope_notes):
    restriction = add_named_individuals_as_restriction(
        concept=concept, prop=prop, collection_concept=collection_concept, scope_notes=scope_notes
    )
    assert restriction
    assert restriction.class_identifier == concept.identifier
    assert restriction.property_identifier == prop.identifier
    assert restriction.some_values_from
    assert (
        f"{collection_concept.identifier}"
        in restriction.some_values_from
    )
    if scope_notes:
        assert restriction.scope_notes
        for note in scope_notes:
            assert note in restriction.scope_notes


@pytest.mark.parametrize(
    "concept_name,property_name,cardinality_pattern,expected_obj",
    [
        ("Concept", "hasValue", "1:1", {"min_cardinality": "1", "max_cardinality": "1"}),
        ("Concept", "hasValue", "0:1", {"min_cardinality": "0", "max_cardinality": "1"}),
        ("Concept", "hasValue", "1:n", {"min_cardinality": "1"}),
    ],
)
def test_prepare_cardinalities(concept_name, property_name, cardinality_pattern, expected_obj):
    cardinality_representation = prepare_cardinalities(
        concept_name=concept_name,
        property_name=property_name,
        cardinality_pattern=cardinality_pattern,
        iri_header_concept="https://biomedit.ch/rdf/sphn-schema/sphn#",
        iri_header_property="https://biomedit.ch/rdf/sphn-schema/sphn#"
    )
    for k, v in expected_obj.items():
        assert k in cardinality_representation
        assert cardinality_representation[k] == v


@pytest.mark.parametrize(
    "concepts,properties,expected_domain",
    [
        (
            {
                "https://biomedit.ch/rdf/sphn-schema/sphn#Concept": Concept(
                    identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept",
                    restrictions={
                        "Concept-hasValue2": Restriction(
                            class_identifier="https://biomedit.ch/rdf/sphn-schema/sphn#Concept",
                            property_identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue3",
                            min_cardinality="1",
                            max_cardinality="1",
                        )
                    },
                )
            },
            {
                "https://biomedit.ch/rdf/sphn-schema/sphn#hasValue1": Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue1"),
                "https://biomedit.ch/rdf/sphn-schema/sphn#hasValue2": Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue2", parent="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue1"),
                "https://biomedit.ch/rdf/sphn-schema/sphn#hasValue3": Property(identifier="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue3", parent="https://biomedit.ch/rdf/sphn-schema/sphn#hasValue2")
            },
            "https://biomedit.ch/rdf/sphn-schema/sphn#Concept",
        ),
    ],
)
def test_infer_domain(concepts, properties, expected_domain):
    infer_domain(concepts=concepts, properties=properties)
    concept = concepts["https://biomedit.ch/rdf/sphn-schema/sphn#Concept"]
    prop1 = properties["https://biomedit.ch/rdf/sphn-schema/sphn#hasValue1"]
    assert prop1.domain
    assert concept in prop1.domain
    prop2 = properties["https://biomedit.ch/rdf/sphn-schema/sphn#hasValue2"]
    assert prop2.domain
    assert concept in prop2.domain
    prop3 = properties["https://biomedit.ch/rdf/sphn-schema/sphn#hasValue3"]
    assert prop3.domain
    assert concept in prop3.domain


@pytest.mark.parametrize(
    "record, expected",
    [
        (
            {
                'release': 2023.1,
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptA',
                'concept or concept compositions or inherited': 'concept',
                'general concept name': 'Concept A',
                'general description': '',
                'contextualized concept name': 'Concept A',
                'contextualized description': '',
                'parent': 'SPHNConcept',
                'type': '',
                'standard': '',
                'value set or subset': '',
                'meaning binding': 'SNOMED CT: 12345678 | Random SNOMED CT code |; LOINC: 12345-10 Some code',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA",
                "label": "Concept A",
                "mappings": [{"standard": "SNOMED CT", "identifier": "12345678"}, {"standard": "LOINC", "identifier": "12345-10"}],
            }
        ),
        (
            {
                'release': 2023.1,
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'concept',
                'general concept name': 'Concept B',
                'general description': '',
                'contextualized concept name': 'Concept B',
                'contextualized description': '',
                'parent': 'SPHNConcept',
                'type': '',
                'standard': '',
                'value set or subset': '',
                'meaning binding': 'ST:1234567', 'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                "label": "Concept B",
                "mappings": [{"standard": "ST", "identifier": "1234567"}]
            }
        ),
        (
            {
                'release': 2023.1,
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptC',
                'concept or concept compositions or inherited': 'concept',
                'general concept name': 'Concept C',
                'general description': '',
                'contextualized concept name': 'Concept C',
                'contextualized description': '',
                'parent': 'Concept B',
                'type': 'Concept B',
                'standard': '',
                'value set or subset': '',
                'meaning binding': 'SO: 0000830 chromosome part; GENO: 0000614 chromosomal region', 'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptC",
                "label": "Concept C",
                "mappings": [{"standard": "GENO", "identifier": "0000614"}, {"standard": "SO", "identifier": "0000830"}]
            }
        )
    ],
)
def test_parse_active_concept(record, expected):
    concepts = {}
    concept = parse_active_concept(
        filename="",
        dataset=None,
        record=record,
        concepts=concepts,
        config=config
    )
    assert expected["identifier"] == concept.identifier
    assert expected["label"] == concept.label
    for mapping in concept.mappings:
        mapping_obj = {"standard": mapping.standard, "identifier": mapping.identifier}
        assert mapping_obj in expected["mappings"]



@pytest.mark.parametrize(
    "record, expected_properties, expected_individuals",
    [
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptA',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p1',
                'general description': '',
                'contextualized concept name': 'p1',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP1",
                "label": "has p1",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA-https://biomedit.ch/rdf/sphn-schema/sphn#hasP1": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP1",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-resource/ucum/UCUM"]
                    }
                },
                "type": ["objectProperty"]
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptA',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p2',
                'general description': '',
                'contextualized concept name': 'p2',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'qualitative',
                'standard': '',
                'value set or subset': 'X; Y; Z',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP2",
                "label": "has p2",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA-https://biomedit.ch/rdf/sphn-schema/sphn#hasP2": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP2",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA_p2"]
                    }
                },
                "type": ["objectProperty"]
            },
            {
                "https://biomedit.ch/rdf/sphn-schema/sphn/individual#X": {
                    "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn/individual#X",
                    "label": "X",
                    "type": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA_p2"]
                },
                "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Y": {
                    "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Y",
                    "label": "Y",
                    "type": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA_p2"]
                },
                "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Z": {
                    "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Z",
                    "label": "Z",
                    "type": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA_p2"]
                },
            }
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptA',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p3',
                'general description': '',
                'contextualized concept name': 'p3',
                'contextualized description': '',
                'parent': 'SPHNAttributeDatatype',
                'type': 'string',
                'standard': '',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP3",
                "label": "has p3",
                "restrictions": {},
                "type": ["dataProperty"]
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p4',
                'general description': '',
                'contextualized concept name': 'p4',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'child of : 122869004 | measurement procedure (procedure) |',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP4",
                "label": "has p4",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP4": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP4",
                        "some_values_from": ["http://snomed.info/id/122869004"],
                        "scope_notes": {"sphn:hasP4 only direct subclasses allowed"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p5',
                'general description': '',
                'contextualized concept name': 'p5',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': '69658003 |Specific (qualifier value)|; 60132005 |Generalized (qualifier value)|; 410529002 |Not needed (qualifier value)|',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP5",
                "label": "has p5",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP5": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP5",
                        "some_values_from": ["http://snomed.info/id/69658003", "http://snomed.info/id/60132005", "http://snomed.info/id/410529002"],
                        "scope_notes": {"sphn:hasP5 no subclasses allowed"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p6',
                'general description': '',
                'contextualized concept name': 'p6',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'descendant of: 716777001 | Hemodynamic monitoring (regime/therapy) |; descendant of: 46973005 |Blood pressure taking (procedure)|',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP6",
                "label": "has p6",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP6": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP6",
                        "some_values_from": ["http://snomed.info/id/716777001", "http://snomed.info/id/46973005"],
                        "scope_notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p8',
                'general description': '',
                'contextualized concept name': 'p8',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': 'Unit: %',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP8",
                "label": "has p8",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP8": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP8",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP8-https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": ["https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit", "https://biomedit.ch/rdf/sphn-schema/sphn#hasCode"],
                                "some_values_from": ["https://biomedit.ch/rdf/sphn-resource/ucum/percent"]
                            }
                        },
                        "has_value": [],
                        "scope_notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p9',
                'general description': '',
                'contextualized concept name': 'p9',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': 'Unit: min; h; d; wk; mo; a',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP9",
                "label": "has p9",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP9": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP9",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP9-https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": ["https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit", "https://biomedit.ch/rdf/sphn-schema/sphn#hasCode"],
                                "some_values_from": ["https://biomedit.ch/rdf/sphn-resource/ucum/min", "https://biomedit.ch/rdf/sphn-resource/ucum/h", "https://biomedit.ch/rdf/sphn-resource/ucum/d", "https://biomedit.ch/rdf/sphn-resource/ucum/wk", "https://biomedit.ch/rdf/sphn-resource/ucum/mo", "https://biomedit.ch/rdf/sphn-resource/ucum/a"],
                            }
                        },
                        "has_value": [],
                        "scope_notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p10',
                'general description': '',
                'contextualized concept name': 'p10',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, ICD-10, ICD-O-3 Topography, ICD-O-3 Morphology, NANDA, ORPHA or other',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP10",
                "label": "has p10",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP10": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP10",
                        "some_values_from": [],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": {"sphn:hasP10 allowed coding system: SNOMED CT, ICD-10, ICD-O-3 Topography, ICD-O-3 Morphology, NANDA, ORPHA or other"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p11',
                'general description': '',
                'contextualized concept name': 'p11',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'NANDA',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP11",
                "label": "has p11",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP11": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP11",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-schema/sphn#Code"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": {"sphn:hasP11 allowed coding system: NANDA"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p13',
                'general description': '',
                'contextualized concept name': 'p13',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'CHOP',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP13",
                "label": "has p13",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP13": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP13",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-resource/chop/CHOP"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p14',
                'general description': '',
                'contextualized concept name': '',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'GMDN, EMDN\u200b',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP14",
                "label": "has p14",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP14": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP14",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-schema/sphn#Code"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": {"sphn:hasP14 allowed coding system: GMDN, EMDN"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p15',
                'general description': '',
                'contextualized concept name': 'p15',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'LOINC',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP15",
                "label": "has p15",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP15": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP15",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-resource/loinc/LOINC"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p16',
                'general description': '',
                'contextualized concept name': 'p16',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, EDQM',
                'value set or subset': 'for SNOMED CT : descendant of: 736542009 |Pharmaceutical dose form (dose form)|; for EDQM : descendant of: EDQMPDF',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP16",
                "label": "has p16",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP16": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP16",
                        "some_values_from": ["http://snomed.info/id/736542009", "https://biomedit.ch/rdf/sphn-schema/sphn#Code"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": {"sphn:hasP16 allowed coding system: SNOMED CT, EDQM"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p17',
                'general description': '',
                'contextualized concept name': 'p17',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': '',
                'value set or subset': 'Concept A; toxicity; cost; disease progression; end of planned therapy; no noticeable improvement; cytogenetic resistance; comorbidity; patient choice; death; lost to follow-up; other; unknown',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP17",
                "label": "has p17",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP17": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP17",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptA", "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB_p17"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {
                "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Comorbidity": {
                    "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn/individual#Comorbidity",
                    "label": "comorbidity",
                    "type": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB_p17"]
                },
                "https://biomedit.ch/rdf/sphn-schema/sphn/individual#CytogeneticResistance": {
                    "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn/individual#CytogeneticResistance",
                    "label": "cytogenetic resistance",
                    "type": ["https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB_p17"]
                }
            }
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'ICD-O-3 Topography',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "some_values_from": ["https://biomedit.ch/rdf/sphn-schema/sphn#Code"],
                        "property_path": [],
                        "has_value": [],
                        "scope_notes": set(),
                        "notes": {"sphn:hasP18 allowed coding system: ICD-O-3 Topography"}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': 'unit -> code restricted to: Gy; cGy; mCi; MBq',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasCode'
                                ],
                                "some_values_from": [
                                    'https://biomedit.ch/rdf/sphn-resource/ucum/Gy',
                                    'https://biomedit.ch/rdf/sphn-resource/ucum/cGy',
                                    'https://biomedit.ch/rdf/sphn-resource/ucum/mCi',
                                    'https://biomedit.ch/rdf/sphn-resource/ucum/MBq'
                                ],
                            }
                        },
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': 'unit -> code restricted to: ucum:h; ucum:wk',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasCode'
                                ],
                                "some_values_from": [
                                    "https://biomedit.ch/rdf/sphn-resource/ucum/h",
                                    "https://biomedit.ch/rdf/sphn-resource/ucum/wk"
                                ],
                            }
                        },
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'medical device -> type code restricted to: 1234565',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                ],
                                "some_values_from": [
                                    "http://snomed.info/id/1234565",
                                ],
                            }
                        },
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, CHOP',
                'value set or subset': 'medical device -> type code restricted to: for SNOMED CT: 12345;45353;for CHOP: Z23.1;Z43.3',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                ],
                                "some_values_from": [
                                    "http://snomed.info/id/12345",
                                    "http://snomed.info/id/45353",
                                    "https://biomedit.ch/rdf/sphn-resource/chop/23.1",
                                    "https://biomedit.ch/rdf/sphn-resource/chop/43.3"
                                ],
                            }
                        },
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                    },
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "notes": {'sphn:hasP18 allowed coding system: SNOMED CT, CHOP'}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT or other',
                'value set or subset': 'medical device -> type code restricted to: for SNOMED CT: 12345;45353;for CHOP: Z23.1;Z43.3',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                        "property_paths": {
                                "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                    "property_path": [
                                        'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                        'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                    ],
                                    "some_values_from": [
                                        "http://snomed.info/id/12345",
                                        "http://snomed.info/id/45353",
                                        "https://biomedit.ch/rdf/sphn-resource/chop/23.1",
                                        "https://biomedit.ch/rdf/sphn-resource/chop/43.3",
                                    ],
                                }
                        },
                    },
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "some_values_from": [
                            "https://biomedit.ch/rdf/sphn-schema/sphn#Code",
                            "https://biomedit.ch/rdf/sphn-schema/sphn#Terminology"
                        ],
                        "notes": {'sphn:hasP18 allowed coding system: SNOMED CT or other'}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'medical device -> type code restricted to: descendant of: 1234565',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                ],
                                "some_values_from": [
                                    "http://snomed.info/id/1234565",
                                ],
                            }
                        },
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'medical device -> type code restricted to: descendant of: 1234565; 3432423',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                ],
                                "some_values_from": [
                                    "http://snomed.info/id/1234565",
                                    "http://snomed.info/id/3432423",
                                ],
                            }
                        },
                        "scope_notes": set(),
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'medical device -> type code restricted to: child of: 895443; 364243',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice-https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode": {
                                "property_path": [
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasMedicalDevice',
                                    'https://biomedit.ch/rdf/sphn-schema/sphn#hasTypeCode'
                                ],
                                "some_values_from": [
                                    "http://snomed.info/id/895443",
                                    "http://snomed.info/id/364243",
                                ],
                            }
                        },
                        "scope_notes": {'sphn:hasP18 only direct subclasses allowed'},
                        "notes": set()
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, CHOP',
                'value set or subset': 'for SNOMED CT: 12345;45353;for CHOP: Z23.1;Z43.3',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "some_values_from": [
                            "http://snomed.info/id/12345",
                            "http://snomed.info/id/45353",
                            "https://biomedit.ch/rdf/sphn-resource/chop/23.1",
                            "https://biomedit.ch/rdf/sphn-resource/chop/43.3",
                        ],
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, CHOP or other',
                'value set or subset': 'for SNOMED CT: 12345;45353;for CHOP: Z23.1;Z43.3',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "some_values_from": [
                            "http://snomed.info/id/12345",
                            "http://snomed.info/id/45353",
                            "https://biomedit.ch/rdf/sphn-resource/chop/23.1",
                            "https://biomedit.ch/rdf/sphn-resource/chop/43.3",
                            "https://biomedit.ch/rdf/sphn-schema/sphn#Code",
                            "https://biomedit.ch/rdf/sphn-schema/sphn#Terminology"
                        ],
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT, CHOP, SPHN',
                'value set or subset': 'for SNOMED CT: 12345;45353;for CHOP: Z23.1;Z43.3;for SPHN: Death',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "some_values_from": [
                            "http://snomed.info/id/12345",
                            "http://snomed.info/id/45353",
                            "https://biomedit.ch/rdf/sphn-resource/chop/23.1",
                            "https://biomedit.ch/rdf/sphn-resource/chop/43.3",
                            "https://biomedit.ch/rdf/sphn-schema/sphn#Death"
                        ],
                        "scope_notes": {'sphn:hasP18 no subclasses allowed'},
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2023.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT',
                'value set or subset': 'code restricted to: extendable value set: 29707007 |Toe structure (body structure)|; 7569003 |Finger structure (body structure)|; 48800003 |Ear lobule structure (body structure)|',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": ["https://biomedit.ch/rdf/sphn-schema/sphn#hasCode"],
                                "some_values_from": [],
                            }
                        },
                        "notes": {'sphn:hasP18/sphn:hasCode recommended values: SNOMED CT: 29707007 |Toe structure (body structure)|, SNOMED CT: 7569003 |Finger structure (body structure)|, SNOMED CT: 48800003 |Ear lobule structure (body structure)|'},
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2024.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p18',
                'general description': '',
                'contextualized concept name': 'p18',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'UCUM',
                'value set or subset': 'unit -> code restricted to : %',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                "label": "has p18",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP18",
                        "property_paths": {
                            "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP18-https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit-https://biomedit.ch/rdf/sphn-schema/sphn#hasCode": {
                                "property_path": ["https://biomedit.ch/rdf/sphn-schema/sphn#hasUnit", "https://biomedit.ch/rdf/sphn-schema/sphn#hasCode"],
                                "some_values_from": ['https://biomedit.ch/rdf/sphn-resource/ucum/percent'],
                            }
                        },
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2024.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p19',
                'general description': '',
                'contextualized concept name': 'p19',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT or other',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP19",
                "label": "has p19",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP19": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP19",
                        "note": {'sphn:hasP19 allowed coding system: SNOMED CT or other'}
                    }
                }
            },
            {}
        ),
        (
            {
                'release': "2024.1",
                'IRI': '',
                'active status (yes/no)': 'yes',
                'deprecated in': '',
                'replaced by': '',
                'concept reference': 'ConceptB',
                'concept or concept compositions or inherited': 'composedOf',
                'general concept name': 'p19',
                'general description': '',
                'contextualized concept name': 'p19',
                'contextualized description': '',
                'parent': 'SPHNAttributeObject',
                'type': 'Code',
                'standard': 'SNOMED CT; OBI or other',
                'value set or subset': '',
                'meaning binding': '',
                'additional information': ''
            },
            {
                "identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP19",
                "label": "has p19",
                "restrictions": {
                    "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB-https://biomedit.ch/rdf/sphn-schema/sphn#hasP19": {
                        "class_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#ConceptB",
                        "property_identifier": "https://biomedit.ch/rdf/sphn-schema/sphn#hasP19",
                        "note": {'sphn:hasP19 allowed coding system: SNOMED CT, OBI or other'}
                    }
                }
            },
            {}
        ),
    ],
)
def test_parse_active_composed_ofs(record, expected_properties, expected_individuals):
    concepts = {
        "ConceptA": Concept(
            identifier="ConceptA",
            label="Concept A"
        )
    }
    properties = {}
    individuals = {}
    prop = parse_active_composed_ofs(
        filename="",
        dataset=None,
        record=record,
        concepts=concepts,
        properties=properties,
        individuals=individuals,
        config=config
    )
    assert expected_properties["identifier"] == prop.identifier
    assert expected_properties["label"] == prop.label
    assert prop.domain
    concept = prop.domain[0]
    if expected_properties["restrictions"]:
        assert concept.restrictions

    for k,v in expected_properties['restrictions'].items():
        assert k in concept.restrictions
        concept_restriction = concept.restrictions[k]
        assert v["class_identifier"] == concept_restriction.class_identifier
        assert v["property_identifier"] == concept_restriction.property_identifier
        if "some_values_from" in v:
            assert set(v["some_values_from"]) == concept_restriction.some_values_from
        # if "property_path" in v:
        #     assert v["property_path"] == concept_restriction.property_path
        if "property_paths" in v:
            assert concept_restriction.property_paths
            for ppr_key, ppr in v["property_paths"].items():
                assert ppr_key in concept_restriction.property_paths
                if "property_path" in ppr:
                    assert ppr["property_path"] == concept_restriction.property_paths[ppr_key].property_path
                if "some_values_from" in ppr:
                    assert set(ppr["some_values_from"]) == concept_restriction.property_paths[ppr_key].some_values_from
        if "has_value" in v:
            assert set(v["has_value"]) == set(concept_restriction.has_value)
        if "scope_notes" in v:
            assert v["scope_notes"] == concept_restriction.scope_notes
        if "notes" in v:
            assert v["notes"] == concept_restriction.notes

    for k,v in expected_individuals.items():
        assert k in individuals
        individual = individuals[k]
        assert v["identifier"] == individual.identifier
        assert v["label"] == individual.label
        assert v["type"] == individual.type


@pytest.mark.parametrize(
    "input_string,regex,expected_string",
    [
        ("for SNOMED CT: 123456", REGEX_MAP["for_standard"], "SNOMED CT"),
        ("for CHOP: Z44.1", REGEX_MAP["for_standard"], "CHOP"),
        ("for ICD-10-GM: C23.1", REGEX_MAP["for_standard"], "ICD-10-GM"),
        ("for icd-10-gm: C23.1", REGEX_MAP["for_standard"], "icd-10-gm"),
        ("descendant of: 123456", REGEX_MAP["descendant_of"], "123456"),
        ("descendant of: snomed:123456", REGEX_MAP["descendant_of"], "snomed:123456"),
        ("descendant of: SO:435345", REGEX_MAP["descendant_of"], "SO:435345"),
        ("extendable value set: 123123", REGEX_MAP["extendable_valueset"], "123123"),
        ("child of: 123456", REGEX_MAP["child_of"], "123456"),
        ("2312312.1", REGEX_MAP["code"], "2312312.1"),
        ("snomed:2312312", REGEX_MAP["code"], "2312312"),
        ("icd-10-gm:D32.3", REGEX_MAP["code"], "D32.3"),
        ("snomed: 342432", REGEX_MAP["code"], "342432"),
        ("L/min/m2", REGEX_MAP["code"], "L/min/m2"),
        ("L/min", REGEX_MAP["code"], "L/min"),
        ("m2", REGEX_MAP["code"], "m2"),
        ("kg/m2", REGEX_MAP["code"], "kg/m2"),
        ("SNOMED CT: 767524001 |Unit of measure (qualifier value)|", REGEX_MAP["code"], "767524001"),
        ("%", REGEX_MAP["code"], "%"),
        ("SNOMED CT: 767524001 |Unit of measure (qualifier value)|", REGEX_MAP["meaning_binding_standard"], "SNOMED CT"),
        ("LOINC: 66464-9 Medical device type [PhenX]", REGEX_MAP["meaning_binding_standard"], "LOINC"),
        ("SNOMED CT: 767524001 |Unit of measure (qualifier value)|", REGEX_MAP["meaning_binding_code"], "767524001"),
        ("LOINC: 66464-9 Medical device type [PhenX]", REGEX_MAP["meaning_binding_code"], "66464-9"),
        ("0:1", r"([\d]+:[\d]+|[\d]+:[n])", "0:1"),
        ("1:1", r"([\d]+:[\d]+|[\d]+:[n])", "1:1"),
        ("1:n", r"([\d]+:[\d]+|[\d]+:[n])", "1:n"),
        ("1:2", r"([\d]+:[\d]+|[\d]+:[n])", "1:2"),
    ],
)
def test_regex(input_string, regex, expected_string):
    extracted_string = extract_code(input_string, regex)
    assert extracted_string == expected_string



@pytest.mark.parametrize(
    "r1,r2,expected",
    [
        (
            Restriction(class_identifier="ConceptA", property_identifier="prop1"),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1)
        ),
        (
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=None),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", max_cardinality=1),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1)
        ),
        (
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=None, some_values_from=['snomed:123134', 'snomed:42543']),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", max_cardinality=1, some_values_from=['snomed:798606', 'snomed:096553']),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543', 'snomed:798606', 'snomed:096553'])
        ),
        (
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543']),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543']),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543'])
        ),
        (
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543']),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=[]),
            Restriction(class_identifier="ConceptA", property_identifier="prop1", min_cardinality=1, max_cardinality=1, some_values_from=['snomed:123134', 'snomed:42543'])
        ),
    ],
)
def test_merge_restriction(r1,r2,expected):
    r = merge_restriction(r1,r2)
    assert r == expected

@pytest.mark.parametrize(
    "standard,values,expected",
    [
        (
            'SNOMED CT',
            ['extendable value set: 29707007 |Toe structure (body structure)|', '7569003 |Finger structure (body structure)|', '48800003 |Ear lobule structure (body structure)|'],
            ['SNOMED CT: 29707007 |Toe structure (body structure)|', 'SNOMED CT: 7569003 |Finger structure (body structure)|', 'SNOMED CT: 48800003 |Ear lobule structure (body structure)|']
        ),
        (
            'SNOMED CT',
            ['extendable value set: SNOMED CT: 29707007 |Toe structure (body structure)|', '7569003 |Finger structure (body structure)|', '48800003 |Ear lobule structure (body structure)|'],
            ['SNOMED CT: 29707007 |Toe structure (body structure)|', 'SNOMED CT: 7569003 |Finger structure (body structure)|', 'SNOMED CT: 48800003 |Ear lobule structure (body structure)|']
        ),
        (
            'SNOMED CT',
            ['extendable value set: SNOMED CT: 29707007 |Toe structure (body structure)|', 'SNOMED CT: 7569003 |Finger structure (body structure)|', 'SNOMED CT: 48800003 |Ear lobule structure (body structure)|'],
            ['SNOMED CT: 29707007 |Toe structure (body structure)|', 'SNOMED CT: 7569003 |Finger structure (body structure)|', 'SNOMED CT: 48800003 |Ear lobule structure (body structure)|']
        ),
    ]
)
def test_parse_extendable_valueset(standard, values, expected):
    codes = parse_extendable_valueset(standard, values)
    assert set(codes) == set(expected)
