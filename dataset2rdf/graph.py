import logging
import rdflib
from typing import List, Dict, Set
from rdflib import RDFS, OWL, RDF, XSD, DC, DCTERMS, SKOS, Literal
from rdflib.term import URIRef
from rdflib import BNode
from rdflib.collection import Collection

from dataset2rdf.config import Config, get_config, logger
from dataset2rdf.models import Concept, Property, Individual, Restriction
from dataset2rdf.utils import REQUIRED_PREFIX_HEADERS, get_iri_for_named_individual, get_iso_datetime


class OntologyGraph:
    """
    An OntologyGraph is a wrapper class for the Ontology.

    Args:
        config: A configuration that is used to prepare the Ontology

    """

    ANNOTATION_PROPERTIES = {
        DC.description,
        DC.title,
        DCTERMS.license,
        OWL.maxCardinality,
        OWL.minCardinality,
        OWL.deprecated,
        SKOS.note,
    }

    DATA_TYPES = {XSD.gDay, XSD.gMonth, XSD.gYear, XSD.time}

    ROOT_CONCEPT = "SPHNConcept"

    def __init__(self, config: Config = get_config()):
        self.graph = rdflib.Graph()
        self.project_graph = rdflib.Graph()
        self.config = config
        if config.iri.canonical_iri.endswith("#"):
            self.namespace = config.iri.canonical_iri[:-1]
        else:
            self.namespace = config.iri.canonical_iri
        self.sphn_prefixes = { self.config.prefix }
        if self.config.deprecated_prefix:
            self.sphn_prefixes.add(self.config.deprecated_prefix)
        self.version = config.version
        if config.iri.versioned_iri:
            self.version_IRI = rdflib.term.URIRef(config.iri.versioned_iri)
        else:
            self.version_IRI = rdflib.term.URIRef(self.namespace + self.version.replace(".", "/"))
        self.namespace_IRI = rdflib.term.URIRef(f"{self.namespace}#")
        if self.config.root_node:
            self.ROOT_CONCEPT = self.config.root_node[0]
        self.root_concept_iri = self.namespace_IRI + self.ROOT_CONCEPT
        self.add_metadata()
        if self.config.imports:
            self.add_imports(self.config.imports)
        if self.config.project_metadata:
            if self.config.project_metadata.iri.canonical_iri.endswith("#"):
                self.project_namespace = self.config.project_metadata.iri.canonical_iri[:-1]
            else:
                self.project_namespace = self.config.project_metadata.iri.canonical_iri
            self.project_namespace_IRI = rdflib.term.URIRef(self.config.project_metadata.iri.canonical_iri)
            self.graph.bind(config.project_metadata.prefix.lower(), URIRef(self.config.project_metadata.iri.canonical_iri))
            self.add_project_metadata()
            self.add_project_imports(self.config.imports)
            self.add_project_imports(self.config.project_metadata.imports)

    def add_metadata(self) -> None:
        """
        Add metadata to the ontology graph.

        This method does the following:
        - Type the graph as an OWL Ontology
        - Set the ontology version
        - Bind the default ``sphn`` prefix
        """
        # Create the SPHN prefix, namespace and versioned IRI
        self.graph.bind(self.config.prefix, self.namespace_IRI)
        try:
            self.graph.bind(self.config.project_metadata.prefix.lower(), self.config.project_metadata.iri.canonical_iri)
        except:
            pass

        if self.config.deprecated_iri:
            self.graph.bind("sphn-deprecated", URIRef(self.config.deprecated_iri))
        self.graph.bind("dc", DC)
        self.graph.bind("dcterms", DCTERMS)
        self.graph.bind("skos", SKOS)
        self.graph.bind("sphn-individual", get_iri_for_named_individual(self.config.iri.canonical_iri))
        # TODO: Change here also the namespace for the version from the project specific
        self.graph.add((URIRef(self.namespace), OWL.versionIRI, self.version_IRI))
        self.graph.add((URIRef(self.namespace), RDF.type, OWL.Ontology))
        self.graph.add((URIRef(self.namespace), DCTERMS.created, Literal(get_iso_datetime())))
        if self.config.copyright:
            self.graph.add(
                (URIRef(self.namespace), DC.rights, rdflib.term.Literal(self.config.copyright))
            )
        if self.config.title:
            self.graph.add((URIRef(self.namespace), DC.title, rdflib.term.Literal(self.config.title)))
        if self.config.description:
            self.graph.add(
                (URIRef(self.namespace), DC.description, rdflib.term.Literal(self.config.description))
            )
        if self.config.citation:
            self.graph.add(
                (URIRef(self.namespace), DCTERMS.bibliographicCitation, rdflib.term.Literal(self.config.citation))
            )
        # TODO: Canonical IRI needs to be without fragment separator
        if self.config.prior_version:
            if self.config.prior_version.startswith("2023"):
                if self.config.deprecated_iri:
                    prior_version_namespace = self.config.deprecated_iri
                else:
                    prior_version_namespace = self.namespace
            else:
                prior_version_namespace = self.namespace
            if prior_version_namespace.endswith("#"):
                prior_version_iri = URIRef(
                    f"{prior_version_namespace[0:-1]}/{self.config.prior_version.replace('.', '/')}"
                )
            else:
                prior_version_iri = URIRef(prior_version_namespace) + "/" + self.config.prior_version.replace(".", "/")
            self.graph.add((URIRef(self.namespace), OWL.priorVersion, prior_version_iri))
        if self.config.license:
            self.graph.add(
                (URIRef(self.namespace), DCTERMS.license, rdflib.term.URIRef(self.config.license))
            )

        for prop in self.ANNOTATION_PROPERTIES:
            self.graph.add((prop, RDF.type, OWL.AnnotationProperty))

        for type in self.DATA_TYPES:
            self.graph.add((type, RDF.type, RDFS.Datatype))

        if self.config.project_metadata:
            self.project_root_concept_iri = URIRef(self.config.project_metadata.iri.canonical_iri + self.config.project_metadata.root_node[0])
        else:
            self.project_root_concept_iri = None

    def add_project_metadata(self) -> None:
        """
        Add project metadata to the ontology graph.
        """
        self.project_graph.bind(self.config.prefix, self.namespace_IRI)
        self.project_graph.bind(self.config.project_metadata.prefix.lower(), URIRef(self.config.project_metadata.iri.canonical_iri))
        self.project_graph.bind("dc", DC)
        self.project_graph.bind("dcterms", DCTERMS)
        self.project_graph.bind("skos", SKOS)
        self.project_graph.bind(f"{self.config.project_metadata.prefix.lower()}-individual", get_iri_for_named_individual(self.config.project_metadata.iri.canonical_iri))
        self.project_graph.add((URIRef(self.project_namespace), OWL.versionIRI, URIRef(self.config.project_metadata.iri.versioned_iri)))
        self.project_graph.add((URIRef(self.project_namespace), RDF.type, OWL.Ontology))
        self.project_graph.add((URIRef(self.project_namespace), DCTERMS.created, Literal(get_iso_datetime())))
        if self.config.project_metadata.copyright:
            self.project_graph.add(
                (URIRef(self.project_namespace), DC.rights, rdflib.term.Literal(self.config.project_metadata.copyright))
            )
        if self.config.project_metadata.title:
            self.project_graph.add((URIRef(self.project_namespace), DC.title, rdflib.term.Literal(self.config.project_metadata.title)))
        if self.config.project_metadata.description:
            self.project_graph.add(
                (URIRef(self.project_namespace), DC.description, rdflib.term.Literal(self.config.project_metadata.description))
            )
        if self.config.project_metadata.prior_version:
            if self.project_namespace.endswith("#"):
                prior_version_iri = URIRef(
                    f"{self.project_namespace[0:-1]}/{self.config.project_metadata.prior_version.replace('.', '/')}"
                )
            else:
                prior_version_iri = URIRef(self.project_namespace) + "/" + self.config.project_metadata.prior_version.replace(".", "/")
            self.project_graph.add((URIRef(self.project_namespace), OWL.priorVersion, prior_version_iri))
        if self.config.project_metadata.license:
            self.project_graph.add(
                (URIRef(self.project_namespace), DCTERMS.license, rdflib.term.URIRef(self.config.project_metadata.license))
            )

        if self.config.project_metadata.imports:
            self.project_graph.add(
                (
                    self.project_namespace_IRI + "Terminology",
                    RDF.type,
                    OWL.Class
                )
            )
            self.project_graph.add(
                (
                    self.project_namespace_IRI + "Terminology",
                    RDFS.label,
                    Literal("Terminology")
                )
            )
            # self.project_graph.add(
            #     (
            #         self.project_namespace_IRI + "Terminology",
            #         RDFS.comment,
            #         Literal(f"Terminology class for grouping external resources used in the {self.config.project_metadata.prefix} project")
            #     )
            # )
            self.project_graph.add(
                (
                    self.project_namespace_IRI + "Terminology",
                    SKOS.definition,
                    Literal(f"Terminology class for grouping external resources used in the {self.config.project_metadata.prefix} project")
                )
            )

    def add_imports(self, imports: Dict) -> None:
        """
        Add imports to the ontology graph.

        Args:
            imports: A dict of one or more imported ontologies

        """
        # Create prefixes, namespaces and versioned IRIs for the imported external resources
        for resource in imports.values():
            self.graph.bind(resource.prefix, rdflib.term.URIRef(resource.iri.canonical_iri))
            if resource.iri.versioned_iri:
                # Some resources do not have a version iri defined (the dereferenceable ones)
                self.graph.add(
                    (
                        URIRef(self.namespace),
                        OWL.imports,
                        rdflib.term.URIRef(resource.iri.versioned_iri),
                    )
                )
            if resource.create_root_node:
                # Move external terminologies (by creating their root class) under SPHN Terminology class
                for root_node in resource.root_node:
                    if f"{resource.prefix}:" in root_node.lower():
                        root_node_identifier = root_node.split(":", 1)[1]
                    elif f"{resource.prefix}_" in root_node.lower():
                        root_node_identifier = root_node.split("_", 1)[1]
                    else:
                        root_node_identifier = root_node
                    root_node_iri = rdflib.term.URIRef(resource.iri.canonical_iri + root_node_identifier)
                    if resource.prefix.startswith('sphn-'):
                        # Resource
                        self.graph.add(
                            (
                                root_node_iri,
                                RDFS.subClassOf,
                                self.namespace_IRI + "Terminology"
                            )
                        )
                    elif resource.prefix not in {'atc', 'chop', 'snomed', 'icd-10-gm', 'loinc', 'ucum', 'oncotree'}:
                        res_key = f'sphn-{resource.prefix}'.upper()
                        # link roots to general root
                        if res_key in imports:
                            resource2 = imports[res_key]
                            general_root_iri = URIRef(resource2.iri.canonical_iri + resource.prefix.upper())
                            self.graph.add(
                                (
                                    root_node_iri,
                                    RDFS.subClassOf,
                                    general_root_iri
                                )
                            )
                            self.graph.add(
                                (
                                    general_root_iri,
                                    RDFS.subClassOf,
                                    self.namespace_IRI + "Terminology"
                                )
                            )
                    else:
                        self.graph.add(
                            (
                                root_node_iri,
                                RDFS.subClassOf,
                                self.namespace_IRI + "Terminology"
                            )
                        )

    def add_project_imports(self, imports: Dict) -> None:
        """
        Add imports to the project ontology graph.

        Args:
            imports: A dict of one or more imported ontologies

        """
        self.project_graph.add(
            (
                URIRef(self.project_namespace),
                OWL.imports,
                self.version_IRI
            )
        )
        for resource in imports.values():
            self.project_graph.bind(resource.prefix, rdflib.term.URIRef(resource.iri.canonical_iri))
            if resource.iri.versioned_iri:
                self.project_graph.add(
                    (
                        URIRef(self.project_namespace),
                        OWL.imports,
                        rdflib.term.URIRef(resource.iri.versioned_iri),
                    )
                )
            if resource.provided_by:
                if resource.provided_by.lower() != self.config.prefix.lower():
                    if resource.create_root_node:
                        # Move external terminologies (by creating their root class) under SPHN Terminology class
                        for root_node in resource.root_node:
                            if root_node.startswith('http:'):
                                root_node_iri = rdflib.term.URIRef(root_node)
                            else:
                                if f"{resource.prefix}:" in root_node.lower():
                                    root_node_identifier = root_node.split(":", 1)[1]
                                elif f"{resource.prefix}:" in root_node:
                                    root_node_identifier = root_node.split(":", 1)[1]
                                else:
                                    root_node_identifier = root_node
                                root_node_iri = rdflib.term.URIRef(resource.iri.canonical_iri + root_node_identifier)
                            if resource.prefix.startswith(self.config.project_metadata.prefix):
                                self.project_graph.add(
                                    (
                                        root_node_iri,
                                        RDFS.subClassOf,
                                        self.project_namespace_IRI + "Terminology"
                                    )
                                )
                            else:
                                res_key = f"{self.config.prefix}-{resource.prefix}".upper()
                                if res_key in imports:
                                    resource2 = imports[res_key]
                                    general_root_iri = URIRef(resource2.iri.canonical_iri + resource.prefix.upper())
                                    self.project_graph.add(
                                        (
                                            root_node_iri,
                                            RDFS.subClassOf,
                                            general_root_iri
                                        )
                                    )
                                    self.project_graph.add(
                                        (
                                            general_root_iri,
                                            RDFS.subClassOf,
                                            self.project_namespace_IRI + "Terminology"
                                        )
                                    )
                                else:
                                    self.project_graph.add(
                                        (
                                            root_node_iri,
                                            RDFS.subClassOf,
                                            self.project_namespace_IRI + "Terminology"
                                        )
                                    )

                            self.project_graph.add(
                                (
                                    self.project_namespace_IRI + "Terminology",
                                    RDFS.subClassOf,
                                    self.namespace_IRI + "Terminology"
                                )
                            )

    def generate_iri(self, identifier: str, config: Config = get_config()) -> URIRef:
        """
        Generate an IRI for a given local identifier.

        Args:
            identifier: str

        Returns:
            An IRI

        """
        if isinstance(identifier, URIRef):
            iri = identifier
        else:
        #    iri = self.namespace_IRI + identifier
            iri = URIRef(identifier)
        return iri

    def add_classes(self, concepts: Dict, config: Config = get_config()) -> None:
        """
        Add a list of concepts to the ontology graph.

        Args:
            concepts: A dictionary of concepts

        """
        for identifier, concept in concepts.items():
            # Create a SPHN class for a concept
            self.add_class(concept)

    def add_class(self, concept: Concept, config: Config = get_config()) -> None:
        """
        Create a SPHN class for a given Concept

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``

        """
        subject = URIRef(concept.identifier)
        project_graph = None
        # logging.warning(str(subject))
        qnames = self.graph.compute_qname(subject)
        for key, restriction in concept.restrictions.items():
            if (str(concept.identifier).startswith(self.config.iri.canonical_iri) 
                and not str(restriction.property_identifier).startswith(self.config.iri.canonical_iri)
                ):
                if not restriction.property_identifier.startswith("http://purl.org/dc/terms/conformsTo"):
                    concept.project_expansion = True
        if qnames[0] in self.sphn_prefixes and concept.project_expansion:
            graph = self.graph
            project_graph = self.project_graph
        elif qnames[0] in self.sphn_prefixes:
            graph = self.graph
        else:
            graph = self.project_graph
        graph.add((subject, RDF.type, OWL.Class))
        if project_graph:
            project_graph.add((subject, RDF.type, OWL.Class))
        # Add label to the class
        if concept.label:
            label = rdflib.Literal(concept.label)
            graph.add((subject, RDFS.label, label))
            if project_graph:
                project_graph.add((subject, RDFS.label, label))
        # Add description to the class
        if concept.description:
            description = rdflib.Literal(concept.description)
            #graph.add((subject, RDFS.comment, description))
            graph.add((subject, SKOS.definition, description))
            if project_graph:
                #project_graph.add((subject, RDFS.comment, description))
                project_graph.add((subject, SKOS.definition, description))

        # Set subclass element
        if concept.parent:
            if concept.parent == concept.identifier:
                logger.warning(f"{concept.identifier} is referring to itself as its parent")
            parent_uri = URIRef(concept.parent)
            graph.add((subject, RDFS.subClassOf, parent_uri))
            if concept.parent == config.iri.canonical_iri + "Deprecated":
                graph.add((subject, OWL.deprecated, rdflib.Literal(True, datatype=XSD.boolean)))
            if project_graph:
                project_graph.add((subject, RDFS.subClassOf, parent_uri))
                
        # Add meaning binding
        for mapping in concept.mappings:
            if mapping.standard in self.config.imports:
                iri = self.config.imports[mapping.standard].iri.canonical_iri
            elif self.config.project_metadata and mapping.standard in self.config.project_metadata.imports:
                iri = self.config.project_metadata.imports[mapping.standard].iri.canonical_iri
            elif mapping.standard == 'NCIT':
                iri = "http://purl.obolibrary.org/obo/NCIT_"
            else:
                iri = "https://example.org/"
                logger.warning(f"Import metadata for '{mapping.standard}' not defined in Config; Using default IRI {iri}")
            mapping_iri = URIRef(iri + mapping.identifier)
            graph.add((subject, URIRef(mapping.predicate), mapping_iri))
            if project_graph:
                project_graph.add((subject, RDFS.subClassOf, parent_uri))

        if concept.sensitive:
            self.graph.add(
                (subject, URIRef(self.config.iri.canonical_iri + "subjectToDeIdentification"), rdflib.term.Literal("true", datatype=XSD.boolean))
            )
        if concept.project_sensitive:
            self.project_graph.add(
                (subject, URIRef(self.config.iri.canonical_iri + "subjectToDeIdentification"), rdflib.term.Literal("true", datatype=XSD.boolean))
            )
        if concept.replaced_by:
            for target_concept in concept.replaced_by:
                self.graph.add(
                    (URIRef(target_concept), URIRef(self.config.iri.canonical_iri + "replaces"), subject)
                )

    def add_properties(self, concepts: Dict, properties: Dict) -> None:
        """
        Add properties to the ontology graph.

        Args:
            concepts: A dictionary of concepts
            properties: A dictionary of properties

        """
        for prop_name, prop in properties.items():
            # Create property URI
            property_uri = URIRef(prop.identifier)

            qnames = self.graph.compute_qname(property_uri)
            if qnames[0] in self.sphn_prefixes:
                graph = self.graph
            else:
                graph = self.project_graph
            # Define the type of the property (datatype or object)
            if prop.type == "dataProperty":
                # Qualitative will refer to SPHN individuals (range is SPHNConcept)
                if "qualitative" in prop.range:
                    graph.add((property_uri, RDF.type, OWL.ObjectProperty))
                else:
                    graph.add((property_uri, RDF.type, OWL.DatatypeProperty))
            elif prop.type == "annotationProperty":
                graph.add((property_uri, RDF.type, OWL.AnnotationProperty))
            elif prop.type == "objectProperty":
                graph.add((property_uri, RDF.type, OWL.ObjectProperty))
            else:
                logger.warning(f"{prop.identifier} has type '{prop.type}'; Cannot determine an appropriate type")


            # Add label to the property
            if prop.label:
                label_p = rdflib.Literal(prop.label)
                graph.add((property_uri, RDFS.label, label_p))

            # Add the description to the property
            if prop.description:
                description = rdflib.Literal(prop.description)
                #graph.add((property_uri, RDFS.comment, description))
                graph.add((property_uri, SKOS.definition, description))

            # Set subPropertyOf relation between properties
            if prop.parent:
                if prop.parent == prop.identifier:
                    logger.warning(f"{prop.identifier} is referring to itself as its parent")
                parent_uri = URIRef(prop.parent)
                graph.add((property_uri, RDFS.subPropertyOf, parent_uri))

            self.add_property_domain(prop=prop, prop_uri=property_uri, concepts=concepts)
            self.add_property_range(prop=prop, prop_uri=property_uri, concepts=concepts)
            if prop.sensitive:
                self.graph.add(
                    (property_uri, URIRef(self.config.iri.canonical_iri + "subjectToDeIdentification"), rdflib.term.Literal("true", datatype=XSD.boolean))
                )
            if prop.project_sensitive:
                self.project_graph.add(
                    (property_uri, URIRef(self.config.iri.canonical_iri + "subjectToDeIdentification"), rdflib.term.Literal("true", datatype=XSD.boolean))
                )
            if prop.replaced_by:
                for target_property in prop.replaced_by:
                    self.graph.add(
                        (URIRef(target_property), URIRef(self.config.iri.canonical_iri + "replaces"), property_uri)
                    )


    def add_property_domain(self, prop: Property, prop_uri: URIRef, concepts: Dict) -> None:
        """
        Add domain(s) for a given property.

        Args:
            prop: An instance of ``dataset2rdf.models.Property``
            prop_uri: The property URI
            concepts: A dictionary of concepts

        """
        if len(prop.domain) > 1:
            prepared_domains1 = []
            prepared_domains2 = []
            for domain in prop.domain:
                if isinstance(domain, str):
                    if domain in concepts:
                        domain = concepts[domain]
                try:
                    domain_iri = self.generate_iri(domain.identifier)
                    qnames = self.graph.compute_qname(domain_iri)
                    if qnames[0] in self.sphn_prefixes:
                        if domain_iri not in prepared_domains1:
                            prepared_domains1.append(domain_iri)
                            if self.root_concept_iri not in prepared_domains1:
                                prepared_domains1.append(self.root_concept_iri)
                    else:
                        if domain_iri not in prepared_domains2:
                            prepared_domains2.append(domain_iri)
                            if self.project_root_concept_iri and self.project_root_concept_iri not in prepared_domains2:
                                prepared_domains2.append(self.project_root_concept_iri)
                except Exception as e:
                    logger.exception(
                        f"Property {prop.identifier} has a reference to a domain '{domain}' which is not a valid concept."
                    )
            if prepared_domains1:
                qnames = self.graph.compute_qname(prop_uri)
                if qnames[0] in self.sphn_prefixes:
                    graph = self.graph
                else:
                    graph = self.project_graph
                if len(prepared_domains1) > 1:
                    self.generate_domain(prop, prop_uri, graph, prepared_domains1)
                else:
                    self.generate_domain(prop, prop_uri, graph, prepared_domains1)
            if prepared_domains2:
                graph = self.project_graph
                if len(prepared_domains1 + prepared_domains2) > 1:
                    self.generate_domain(prop, prop_uri, graph, prepared_domains1 + prepared_domains2)
                else:
                    self.generate_domain(prop, prop_uri, graph, prepared_domains2)
        else:
            if len(prop.domain):
                domain = prop.domain[0]
                if isinstance(domain, str):
                    if domain in concepts:
                        domain = concepts[domain]
                domain_uri = URIRef(domain.identifier)
                qnames = self.graph.compute_qname(prop.identifier)
                if qnames[0] in self.sphn_prefixes:
                    graph = self.graph
                else:
                    graph = self.project_graph
                
                # Check that were adding the domain alone only when it is exactly the root concept, otherwise add the domain and root concept as well
                if domain_uri == self.root_concept_iri:
                    self.generate_domain(prop, prop_uri, graph, [domain_uri])
                elif domain_uri.startswith(self.namespace_IRI):
                    self.generate_domain(prop, prop_uri, graph, [domain_uri, self.root_concept_iri])
                elif self.project_root_concept_iri and domain_uri == self.project_root_concept_iri:
                    self.generate_domain(prop, prop_uri, graph, [domain_uri])
                else:
                    if self.project_root_concept_iri:
                        self.generate_domain(prop, prop_uri, graph, [domain_uri, self.project_root_concept_iri])
                    else:
                        self.generate_domain(prop, prop_uri, graph, [domain_uri])
            #elif not (prop.identifier.endswith("AttributeObject") or prop.identifier.endswith("AttributeDatatype")):
                # This will happen if the project specific concept has not assigned any cardinalities to hasSubjectPseudoIdentifier and so on. Hence they are created project specific, but not assigned a domain:
            #    project = False
            #    prefix = self.config.prefix.upper()
            #    uri_header = self.config.iri.canonical_iri
            #    topLevel = "Concept"
            #    if prop.identifier.lower().startswith(self.config.projectdefinitions["canonical_iri"].lower()):
            #        project = True
            #        prefix = self.config.projectdefinitions["prefix"]
            #        uri_header = self.config.projectdefinitions["canonical_iri"]
            #    logging.warning("identfied a property : " + str(prop.identifier) +" but we have no domains for it. Setting it to the Prefix+AttributeObject.")
            #    if project:
            #        graph = self.project_graph
            #    else:
            #        graph = self.graph
            #    graph.add((prop_uri, RDFS.domain, URIRef(uri_header+prefix+topLevel)))

    def generate_domain(self, prop: Property, prop_uri: URIRef,  graph, domain_list):
        if hasattr(self, 'project_namespace_IRI'):
            if prop.identifier.startswith(self.project_namespace_IRI) and self.project_root_concept_iri not in domain_list:
                if self.project_root_concept_iri:
                    domain_list.append(self.project_root_concept_iri)

        if len(domain_list) == 1:
            graph.add((prop_uri, RDFS.domain, domain_list[0]))
        else:
            bn1, bn2 = rdflib.BNode(), rdflib.BNode()
            Collection(graph, bn1, domain_list)
            graph.add((prop_uri, RDFS.domain, bn2))
            graph.add((bn2, RDF.type, OWL.Class))
            graph.add((bn2, OWL.unionOf, bn1))

    def add_property_range(self, prop: Property, prop_uri: URIRef, concepts: Dict) -> None:
        """
        Add range(s) for a given property.

        Args:
            prop: An instance of ``dataset2rdf.models.Property``
            prop_uri: The property URI
            concepts: A dictionary of concepts

        """
        prop_qname = self.graph.compute_qname(prop_uri)
        if len(prop.range) > 1:
            bn1 = rdflib.BNode()
            prepared_ranges1 = []
            prepared_ranges2 = []
            for range in prop.range:
                if not isinstance(range, str):
                    range = range.identifier
                range_uri = self.get_range_uri(prop, range, concepts)
                qnames = self.graph.compute_qname(range_uri)
                if qnames[0] in self.sphn_prefixes:
                    if range_uri not in prepared_ranges1:
                        prepared_ranges1.append(range_uri)
                elif self.config.project_metadata and qnames[0] == self.config.project_metadata.prefix:
                    if range_uri not in prepared_ranges2:
                        prepared_ranges2.append(range_uri)
                else:
                    if range_uri not in prepared_ranges1:
                        prepared_ranges1.append(range_uri)
            if prepared_ranges1:
                full_range_list = prepared_ranges1
                qnames = self.graph.compute_qname(prop_uri)
                if qnames[0] in self.sphn_prefixes:
                    graph = self.graph
                else:
                    graph = self.project_graph
                    full_range_list.extend(prepared_ranges2)
                    prepared_ranges2.clear()
                Collection(graph, bn1, full_range_list)
                bn2 = rdflib.BNode()
                graph.add((prop_uri, RDFS.range, bn2))
                if prop.type == "dataProperty":
                    graph.add(((bn2, RDF.type, RDFS.Datatype)))
                else:
                    graph.add((bn2, RDF.type, OWL.Class))
                graph.add((bn2, OWL.unionOf, bn1))
            if prepared_ranges2:
                graph = self.project_graph
                Collection(graph, bn1, prepared_ranges1 + prepared_ranges2)
                bn2 = rdflib.BNode()
                graph.add((prop_uri, RDFS.range, bn2))

                if prop.type == "dataProperty":
                    graph.add(((bn2, RDF.type, RDFS.Datatype)))
                else:
                    graph.add((bn2, RDF.type, OWL.Class))
                graph.add((bn2, OWL.unionOf, bn1))
        else:
            if prop.range:
                if isinstance(prop.range[0], str):
                    range = prop.range[0]
                else:
                    range = prop.range[0].identifier
                range_uri = self.get_range_uri(prop, range, concepts)
                qname = self.graph.compute_qname(range_uri)

                # we want to place it in the project graph if the property_qname or the range_qname is the project prefix.
                if prop_qname[0] == qname[0] == self.config.prefix:
                    graph = self.graph
                elif self.config.project_metadata and \
                    (prop.identifier.lower().startswith(self.config.project_metadata.iri.canonical_iri.lower()) or \
                        range_uri.lower().startswith(self.config.project_metadata.iri.canonical_iri.lower())):
                    graph = self.project_graph
                else:
                    graph = self.graph
                graph.add((prop_uri, RDFS.range, range_uri))


    def add_named_individuals(self, individuals: Dict) -> None:
        """
        Add a list of Named Individuals to the ontology graph.

        Args:
            individuals: A dictionary of named individuals

        """
        for individual in individuals.values():
            self.add_named_individual(individual)

    def add_named_individual(self, individual: Individual) -> None:
        """
        Add a Named Individual to the ontology graph

        Args:
            individual: An instance of ``dataset2rdf.models.Individual``

        """
        subject = URIRef(individual.identifier)
        qname = self.graph.compute_qname(subject)
        if str(qname[1]) in {self.config.iri.canonical_iri, get_iri_for_named_individual(self.config.iri.canonical_iri)}:
            graph = self.graph
        else:
            graph = self.project_graph
        graph.add((subject, RDF.type, OWL.NamedIndividual))
        # Add label to the NamedIndividual
        if individual.label:
            label = rdflib.Literal(individual.label)
            graph.add((subject, RDFS.label, label))
        if individual.description:
            description = rdflib.Literal(individual.description)
            #graph.add((subject, RDFS.comment, description))
            graph.add((subject, SKOS.definition, description))
        for type in individual.type:
            object_iri = URIRef(type)
            graph.add((subject, RDF.type, object_iri))

    def get_range_uri(self, prop: Property, range: str, concepts: Dict) -> URIRef:
        """
        Get the uri of the range.

        Unlike the domains (where the type is always an SPHN Class),
        the type of the range differs for each property
        and needs to be tackled specifically to generate URIs

        Args:
            prop: An instance of ``dataset2rdf.models.Property``
            range: An instance of ``dataset2rdf.models.Concept`` that is the range of the Property
            concepts: A dictionary of concepts

        Returns:
            A ``rdflib.term.URIRef`` corresponding to the range

        """
        if range in concepts:
            range_concept = concepts[range]
            range_uri = URIRef(range_concept.identifier)
        else:
            if range == self.config.iri.canonical_iri + "temporal":
                if prop.identifier == self.config.iri.canonical_iri + "hasDay":
                    range_uri = XSD.gDay
                elif prop.identifier == self.config.iri.canonical_iri + "hasMonth":
                    range_uri = XSD.gMonth
                elif prop.identifier == self.config.iri.canonical_iri + "hasYear":
                    range_uri = XSD.gYear
                elif prop.identifier == self.config.iri.canonical_iri + "hasTime":
                    range_uri = XSD.time
                else:
                    range_uri = XSD.dateTime
            elif range == self.config.iri.canonical_iri + "string":
                range_uri = XSD.string
            elif range == self.config.iri.canonical_iri + "quantitative":
                range_uri = XSD.double
            elif range == self.config.iri.canonical_iri + "anyURI":
                range_uri = XSD.anyURI
            elif range == "http://www.w3.org/TR/2001/REC-xmlschema-2-20010502/#anyURI":
                range_uri = XSD.anyURI
            else:
                logger.debug(f"Defaulting range to {self.ROOT_CONCEPT} for {prop.identifier} - in the dataset it has been defined as {range}")
                range_uri = rdflib.term.URIRef(self.root_concept_iri)
        return range_uri

    def export(self, filename: str, format: str = "turtle") -> None:
        """
        Export the ontology graph

        Args:
            filename: The output filename
            format: The serialization format. Default is ``turtle``

        """
        self._export_header(filename=filename, format=format, base_iri=self.namespace, namespace_iri=self.namespace_IRI)
        with open(filename, "a", encoding="utf-8") as file:
            file.write(self.graph.serialize(format=format))
        self.post_export_check(filename)

    def export_project(self, filename: str, format: str = "turtle") -> None:
        """
        Export the project graph.

        Args:
            filename: The output filename
            format: The serialization format. Default is ``turtle``

        """
        self._export_header(filename=filename, format=format, base_iri=self.project_namespace, namespace_iri=self.project_namespace_IRI)
        with open(filename, "a", encoding="utf-8") as file:
            file.write(self.project_graph.serialize(format=format))
        self.post_export_check(filename)

    def post_export_check(self, filename: str, format: str = "turtle") -> None:
        """
        Checks a given filename.

        Args:
            filename: The output filename
            format: The serialization format. Default is ``turtle``

        """
        with open(filename, encoding="utf-8") as file:
            content = file.read()
            if "_:" in content:
                logger.warning(f"Unexpected blank nodes found in exported file: {filename}")

    def _export_header(self, filename: str, format: str = "turtle", base_iri: str = None, namespace_iri: str = None) -> None:
        """
        Add base and default prefix declaration at the beginning of the
        output TTL

        Args:
            filename: The output filename
            format: The serialization format. Default is ``turtle``
            base_iri: The base IRI
            namespace_iri: The namespace IRI

        """
        with open(filename, "w", encoding="utf-8") as file:
            file.write(REQUIRED_PREFIX_HEADERS.format(base=base_iri, namespace=namespace_iri))

    def add_triples(self, filename: str, format: str = "turtle") -> None:
        """
        Add additional triples to the ontology graph

        Args:
            filename: The filename
            format: The input format. Default is ``turtle``

        """
        g = rdflib.Graph()
        g.parse(filename, format=format)
        self.graph += g

    def add_cardinality(
        self, concept: Concept, class_identifier: str, prop_identifier: str, predicate: URIRef, cardinality: int, config: Config = get_config(),
    ):
        """
        Add a cardinality restriction on a property for a given class.
        The nature of the cardinality restriction depends on the ``predicate`` provided.

        Args:
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            predicate: The predicate to use
            cardinality: The expected minimum cardinality
            config: The config

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        project_graph = None
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            if prop_iri.startswith("http://purl.org/dc/terms/conformsTo"):
                graph = self.graph
            else:
                graph = self.project_graph
        elif qname[0] == config.prefix:
            graph = self.graph
            if concept.project_expansion:
                project_graph = self.project_graph
        else:
            graph = self.project_graph
        res_node = BNode()
        graph.add((res_node, RDF.type, OWL.Restriction))
        graph.add((res_node, OWL.onProperty, prop_iri))
        graph.add(
            (res_node, predicate, rdflib.term.Literal(cardinality, datatype=XSD.nonNegativeInteger))
        )
        if project_graph:
            res_node_ps = BNode()
            project_graph.add((res_node_ps, RDF.type, OWL.Restriction))
            project_graph.add((res_node_ps, OWL.onProperty, prop_iri))
            project_graph.add(
                (res_node_ps, predicate, rdflib.term.Literal(cardinality, datatype=XSD.nonNegativeInteger))
            )
            return res_node, res_node_ps
        return res_node, None

    def add_min_cardinality(
        self, concept: Concept, class_identifier: str, prop_identifier: str, cardinality: int, config: Config = get_config()
    ):
        """
        Add min cardinality restriction on a property for a given class.

        Args:
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            cardinality: The expected minimum cardinality
            config: The config

        """
        return self.add_cardinality(
            concept, class_identifier, prop_identifier, OWL.minCardinality, cardinality, config
        )

    def add_max_cardinality(
        self, concept: Concept, class_identifier: str, prop_identifier: str, cardinality: int, config: Config = get_config()
    ):
        """
        Add max cardinality restriction on a property for a given class.

        Args:
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            cardinality: The expected maximum cardinality
            config: The config

        """
        return self.add_cardinality(
            concept, class_identifier, prop_identifier, OWL.maxCardinality, cardinality, config
        )

    def apply_restrictions(self, concept: Concept, config: Config = get_config()) -> None:
        """
        Apply restrictions for a given concept.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            config: The config

        """
        for key, restriction in concept.restrictions.items():
            intersection_ofs: List = []
            union_ofs: List = []
            intersection_ofs_ps: List = []
            union_ofs_ps: List = []
            if restriction.min_cardinality is not None:
                r1, pr1 = self.add_min_cardinality(
                    concept,
                    restriction.class_identifier,
                    restriction.property_identifier,
                    cardinality=restriction.min_cardinality,
                    config=config,
                )
                intersection_ofs.append(r1)
                if pr1: intersection_ofs_ps.append(pr1)

            if restriction.max_cardinality is not None:
                r2, pr2 = self.add_max_cardinality(
                    concept,
                    restriction.class_identifier,
                    restriction.property_identifier,
                    cardinality=restriction.max_cardinality,
                    config=config,
                )
                intersection_ofs.append(r2)
                if pr2: intersection_ofs_ps.append(pr2)

            if restriction.some_values_from:
                r3, pr3 = self._add_some_values_from_restriction(
                    concept,
                    restriction.class_identifier,
                    restriction.property_identifier,
                    restriction.some_values_from,
                    config=config,
                )
                intersection_ofs.append(r3)
                if pr3:
                    intersection_ofs_ps.append(pr3)

            if restriction.property_paths:
                for property_path_restriction_key, property_path_restriction in restriction.property_paths.items():
                    if property_path_restriction.some_values_from:
                        r3a, pr3a = self._add_nested_some_values_from_restriction(
                            concept,
                            restriction.class_identifier,
                            restriction.property_identifier,
                            property_path_restriction.some_values_from,
                            property_path=property_path_restriction.property_path
                        )
                        intersection_ofs.append(r3a)
                        if pr3a:
                            intersection_ofs_ps.append(pr3a)
                    if property_path_restriction.has_value:
                        r3b, pr3b = self._add_nested_some_values_from_restriction(
                            concept,
                            restriction.class_identifier,
                            restriction.property_identifier,
                            property_path_restriction.has_value,
                            property_path=property_path_restriction.property_path
                        )
                        intersection_ofs.append(r3b)
                        if pr3b:
                            intersection_ofs_ps.append(pr3b)

            if restriction.has_value:
                r4, pr4 = self._add_some_values_from_restriction(
                    concept,
                    restriction.class_identifier,
                    restriction.property_identifier,
                    restriction.has_value,
                    config=config,
                )
                intersection_ofs.append(r4)
                if pr4: intersection_ofs_ps.append(pr4)

            if intersection_ofs:
                if len(intersection_ofs) > 1:
                    self._apply_intersection(
                        concept,
                        restriction.class_identifier,
                        restriction.property_identifier,
                        restrictions=intersection_ofs,
                        config=config,
                    )
                else:
                    concept_uri = self.generate_iri(restriction.class_identifier)
                    qname = self.graph.compute_qname(concept_uri)
                    prop_iri = self.generate_iri(restriction.property_identifier)
                    if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
                        if prop_iri.startswith("http://purl.org/dc/terms/conformsTo"):
                            graph = self.graph
                        else:
                            graph = self.project_graph
                    elif qname[0] == self.config.prefix:
                        graph = self.graph
                    else:
                        graph = self.project_graph
                    graph.add((concept_uri, RDFS.subClassOf, intersection_ofs[0]))

            if intersection_ofs_ps:
                if len(intersection_ofs_ps) > 1:
                    self._apply_intersection_ps(
                        concept,
                        restriction.class_identifier,
                        restriction.property_identifier,
                        restrictions=intersection_ofs_ps,
                        config=config,
                    )
                else:
                    concept_uri = self.generate_iri(restriction.class_identifier)
                    graph = self.project_graph
                    graph.add((concept_uri, RDFS.subClassOf, intersection_ofs_ps[0]))

            if union_ofs:
                if len(union_ofs) > 1:
                    self._apply_union(
                        concept,
                        restriction.class_identifier,
                        restriction.property_identifier,
                        restrictions=union_ofs,
                        config=config,
                    )
                else:
                    concept_uri = self.generate_iri(restriction.class_identifier)
                    qname = self.graph.compute_qname(concept_uri)
                    prop_iri = self.generate_iri(restriction.property_identifier)
                    if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
                        if prop_iri.startswith("http://purl.org/dc/terms/conformsTo"):
                            graph = self.graph
                        else:
                            graph = self.project_graph
                    elif qname[0] == self.config.prefix:
                        graph = self.graph
                    else:
                        graph = self.project_graph
                    graph.add((concept_uri, RDFS.subClassOf, union_ofs[0]))
            
            if union_ofs_ps:
                if len(union_ofs_ps) > 1:
                    self._apply_union_ps(
                        concept,
                        restriction.class_identifier,
                        restriction.property_identifier,
                        restrictions=union_ofs_ps,
                        config=config,
                    )
                else:
                    concept_uri = self.generate_iri(restriction.class_identifier)
                    graph = self.project_graph
                    graph.add((concept_uri, RDFS.subClassOf, union_ofs_ps[0]))

            if restriction.notes:
                self._add_notes(
                    concept, restriction.class_identifier, restriction.property_identifier, restriction.notes,
                )

            if restriction.scope_notes:
                self._add_scope_notes(
                    concept,
                    restriction.class_identifier,
                    restriction.property_identifier,
                    restriction.scope_notes,
                )

    def _add_notes(self, concept: Concept, class_identifier: str, prop_identifier: str, values: Set, config: Config = get_config()) -> None:
        """
        Add a ``skos:note`` about a property and its values in the context
        of a given class.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            values: A set of notes
            config: The config

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        project_graph = None
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
            if concept.project_expansion:
                project_graph = self.project_graph
        else:
            graph = self.project_graph
        prepared_values = [rdflib.Literal(x) for x in values]
        predicate = SKOS.note
        for value in prepared_values:
            graph.add((class_iri, predicate, value))
            if project_graph:
                project_graph.add((class_iri, predicate, value))


    def _add_scope_notes(self, concept: Concept, class_identifier: str, prop_identifier: str, values: Set, config: Config = get_config()) -> None:
        """
        Add a ``skos:scopeNote`` about a property and its values in the context
        of a given class.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            values: A set of notes
            config: The config

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        project_graph = None
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
            if concept.project_expansion:
                project_graph = self.project_graph
        else:
            graph = self.project_graph
        prepared_values = [rdflib.Literal(x) for x in values]
        predicate = SKOS.scopeNote
        for value in prepared_values:
            graph.add((class_iri, predicate, value))
            if project_graph:
                project_graph.add((class_iri, predicate, value))


    def _add_some_values_from_restriction(
        self, concept: Concept, class_identifier: str, prop_identifier: str, values: List, config: Config = get_config()
    ):
        """
        Add owl:someValuesFrom restriction on a property for a given class.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            values: A list of values that participate in the restriction
            config: The config

        Returns:
            The BNode that represents the someValuesFrom restriction

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        project_graph = None
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
            if concept.project_expansion:
                project_graph = self.project_graph
        else:
            graph = self.project_graph
        prepared_values = [x if isinstance(x, (URIRef, BNode)) else URIRef(x) for x in values]
        if len(prepared_values) == 1:
            res_node = BNode()
            graph.add((res_node, RDF.type, OWL.Restriction))
            graph.add((res_node, OWL.someValuesFrom, prepared_values[0]))
            graph.add((res_node, OWL.onProperty, prop_iri))
        else:
            coll_node = BNode()
            Collection(graph, coll_node, prepared_values)
            u_node = BNode()
            graph.add((u_node, RDF.type, OWL.Class))
            graph.add((u_node, OWL.unionOf, coll_node))
            res_node = BNode()
            graph.add((res_node, RDF.type, OWL.Restriction))
            graph.add((res_node, OWL.someValuesFrom, u_node))
            graph.add((res_node, OWL.onProperty, prop_iri))
        if project_graph:
            if len(prepared_values) == 1:
                res_node_ps = BNode()
                project_graph.add((res_node_ps, RDF.type, OWL.Restriction))
                project_graph.add((res_node_ps, OWL.someValuesFrom, prepared_values[0]))
                project_graph.add((res_node_ps, OWL.onProperty, prop_iri))
            else:
                coll_node_ps = BNode()
                Collection(project_graph, coll_node_ps, prepared_values)
                u_node_ps = BNode()
                project_graph.add((u_node_ps, RDF.type, OWL.Class))
                project_graph.add((u_node_ps, OWL.unionOf, coll_node_ps))
                res_node_ps = BNode()
                project_graph.add((res_node_ps, RDF.type, OWL.Restriction))
                project_graph.add((res_node_ps, OWL.someValuesFrom, u_node_ps))
                project_graph.add((res_node_ps, OWL.onProperty, prop_iri))
            return res_node, res_node_ps
        return res_node, None

    def _add_has_value_restriction(
        self, class_identifier: str, prop_identifier: str, values: List, config: Config = get_config()
    ) -> BNode:
        """
        Add owl:hasValue restriction on a property for a given class.

        Args:
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            values: A list of values that participate in the restriction
            config: The config

        Returns:
            The BNode that represents the hasValue restriction

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
        else:
            graph = self.project_graph
        prepared_values = [x if isinstance(x, URIRef) else URIRef(x) for x in values]
        if len(prepared_values) == 1:
            res_node = BNode()
            graph.add((res_node, RDF.type, OWL.Restriction))
            graph.add((res_node, OWL.someValuesFrom, prepared_values[0]))
            graph.add((res_node, OWL.onProperty, prop_iri))
        else:
            restrictions = []
            for value in prepared_values:
                res_node = BNode()
                graph.add((res_node, RDF.type, OWL.Restriction))
                graph.add((res_node, OWL.hasValue, value))
                graph.add((res_node, OWL.onProperty, prop_iri))
                restrictions.append(res_node)
            coll_node = BNode()
            Collection(graph, coll_node, restrictions)
            u_node = BNode()
            graph.add((u_node, RDF.type, OWL.Class))
            graph.add((u_node, OWL.unionOf, coll_node))
            res_node = BNode()
            graph.add((res_node, RDF.type, OWL.Restriction))
            graph.add((res_node, OWL.someValuesFrom, u_node))
        return res_node

    def _apply_intersection(
        self, concept: Concept, class_identifier: str, prop_identifier: str, restrictions: List[BNode], config: Config = get_config()
    ) -> BNode:
        """
        Apply an owl:intersectionOf on a given list of restrictions.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            restrictions: A list of restrictions (as BNode) that participate in the intersection
            config: The config

        Returns:
            The BNode that represents the intersection

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            if prop_iri.startswith("http://purl.org/dc/terms/conformsTo"):
                graph = self.graph
            else:
                graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
        else:
            graph = self.project_graph
        coll_node = BNode()
        Collection(graph, coll_node, restrictions)
        i_node = BNode()
        graph.add((i_node, RDF.type, OWL.Class))
        graph.add((i_node, OWL.intersectionOf, coll_node))
        graph.add((class_iri, RDFS.subClassOf, i_node))
        return i_node
    
    def _apply_intersection_ps(
        self, concept: Concept, class_identifier: str, prop_identifier: str, restrictions: List[BNode], config: Config = get_config()
    ) -> BNode:
        """
        Apply an owl:intersectionOf on a given list of restrictions.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            restrictions: A list of restrictions (as BNode) that participate in the intersection
            config: The config

        Returns:
            The BNode that represents the intersection

        """
        class_iri = self.generate_iri(class_identifier)
        graph = self.project_graph
        coll_node = BNode()
        Collection(graph, coll_node, restrictions)
        i_node = BNode()
        graph.add((i_node, RDF.type, OWL.Class))
        graph.add((i_node, OWL.intersectionOf, coll_node))
        graph.add((class_iri, RDFS.subClassOf, i_node))

    def _apply_union(
        self, concept: Concept, class_identifier: str, prop_identifier: str, restrictions: List[BNode], config: Config = get_config()
    ) -> BNode:
        """
        Apply an owl:unionOf on a given list of restrictions.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            restrictions: A list of restrictions (as BNode) that participate in the union
            config: The config

        Returns:
            The BNode that represents the union

        """
        class_iri = self.generate_iri(class_identifier)
        qname = self.graph.compute_qname(class_iri)
        prop_iri = self.generate_iri(prop_identifier)
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            if prop_iri.startswith("http://purl.org/dc/terms/conformsTo"):
                graph = self.graph
            else:
                graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
        else:
            graph = self.project_graph
        coll_node = BNode()
        Collection(graph, coll_node, restrictions)
        u_node = BNode()
        graph.add((u_node, RDF.type, OWL.Class))
        graph.add((u_node, OWL.unionOf, coll_node))
        graph.add((class_iri, RDFS.subClassOf, u_node))
        return u_node

    def _apply_union_ps(
        self, concept: Concept, class_identifier: str, prop_identifier: str, restrictions: List[BNode], config: Config = get_config()
    ) -> BNode:
        """
        Apply an owl:unionOf on a given list of restrictions.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            restrictions: A list of restrictions (as BNode) that participate in the union
            config: The config

        Returns:
            The BNode that represents the union

        """
        class_iri = self.generate_iri(class_identifier)
        graph = self.project_graph
        coll_node = BNode()
        Collection(graph, coll_node, restrictions)
        u_node = BNode()
        graph.add((u_node, RDF.type, OWL.Class))
        graph.add((u_node, OWL.unionOf, coll_node))
        graph.add((class_iri, RDFS.subClassOf, u_node))
        
    def _add_nested_some_values_from_restriction(
        self, concept: Concept, class_identifier: str, prop_identifier: str, values: List, property_path: List = [], config: Config = get_config()
    ):
        """
        Add nested owl:someValuesFrom restriction on a property for a given class.

        Args:
            concept: An instance of ``dataset2rdf.models.Concept``
            class_identifier: The class or concept identifier
            prop_identifier: The property identifier
            values: A list of values that participate in the restriction
            property_path: A list of properties that represents a path such
            that the value restriction is applied to the last property in
            the property path
            config: The config

        Returns:
            The BNode that represents the nested someValuesFrom restriction

        """
        prepared_values = [x if isinstance(x, (URIRef, BNode)) else URIRef(x) for x in values]
        property_path.reverse()
        property_path.append(prop_identifier)
        prop_iri = self.generate_iri(prop_identifier)
        qname = self.graph.compute_qname(prop_iri)
        project_graph = None
        if qname[0] == config.prefix and not prop_iri.startswith(config.iri.canonical_iri):
            graph = self.project_graph
        elif qname[0] == self.config.prefix:
            graph = self.graph
            if concept.project_expansion:
                project_graph = self.project_graph
                restrictions_ps = []
                prepared_values_ps = [x if isinstance(x, (URIRef, BNode)) else URIRef(x) for x in values]
        else:
            graph = self.project_graph

        concept_prefix = [self.graph.compute_qname(concept.identifier)[0]]
        property_prefix = [self.graph.compute_qname(prop_iri)[0]]
        property_path_prefixes = []
        if property_path:
            for p in property_path:
                p_iri = self.generate_iri(p)
                p_prefix = self.graph.compute_qname(p_iri)[0]
                property_path_prefixes.append(p_prefix)
        all_prefixes = property_path_prefixes + property_prefix + concept_prefix
        if any(x != config.prefix.lower() for x in all_prefixes):
            graph = self.project_graph
        else:
            graph = self.graph

        restrictions = []
        for property in property_path:
            property_iri = self.generate_iri(property)
            if len(prepared_values) == 1:
                if isinstance(prepared_values[0], URIRef):
                    res_node = BNode()
                    graph.add((res_node, RDF.type, OWL.Restriction))
                    graph.add((res_node, OWL.onProperty, property_iri))
                    graph.add((res_node, OWL.someValuesFrom, prepared_values[0]))
                    if project_graph:
                        res_node_ps = BNode()
                        project_graph.add((res_node_ps, RDF.type, OWL.Restriction))
                        project_graph.add((res_node_ps, OWL.onProperty, property_iri))
                        project_graph.add((res_node_ps, OWL.someValuesFrom, prepared_values_ps[0]))
                else:
                    res_node = BNode()
                    graph.add((res_node, RDF.type, OWL.Restriction))
                    graph.add((res_node, OWL.someValuesFrom, prepared_values[0]))
                    graph.add((res_node, OWL.onProperty, property_iri))
                    if project_graph:
                        res_node_ps = BNode()
                        project_graph.add((res_node_ps, RDF.type, OWL.Restriction))
                        project_graph.add((res_node_ps, OWL.someValuesFrom, prepared_values_ps[0]))
                        project_graph.add((res_node_ps, OWL.onProperty, property_iri))
                prepared_values = [res_node]
                restrictions.append(res_node)
                if project_graph:
                    restrictions_ps.append(res_node_ps)
                    prepared_values_ps = [res_node_ps]
            else:
                restrictions = []
                bnodeList = BNode()
                col = Collection(graph, bnodeList, prepared_values)
                second_level_restriction = BNode()

                graph.add((second_level_restriction, RDF.type, OWL.Class))
                graph.add((second_level_restriction, OWL.unionOf, bnodeList))
                restrictions.append(second_level_restriction)
                u_node = BNode()
                graph.add((u_node, RDF.type, OWL.Restriction))
                graph.add((u_node, OWL.someValuesFrom, second_level_restriction))
                graph.add((u_node, OWL.onProperty, property_iri))

                if project_graph:
                    bnodeList_ps = BNode()
                    col = Collection(project_graph, bnodeList_ps, prepared_values_ps)
                    second_level_restriction = BNode()

                    project_graph.add((second_level_restriction, RDF.type, OWL.Class))
                    project_graph.add((second_level_restriction, OWL.unionOf, bnodeList_ps))
                    restrictions.append(second_level_restriction)
                    u_node_ps = BNode()
                    project_graph.add((u_node_ps, RDF.type, OWL.Restriction))
                    project_graph.add((u_node_ps, OWL.someValuesFrom, second_level_restriction))
                    project_graph.add((u_node_ps, OWL.onProperty, property_iri))

                    prepared_values_ps = [u_node_ps]

                prepared_values = [u_node]
                    
                restrictions.append(u_node)
                if project_graph:
                    restrictions_ps.append(u_node_ps)

        if project_graph:
            return restrictions[-1], restrictions_ps[-1]
        return restrictions[-1], None