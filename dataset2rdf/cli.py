import click
import logging
import os
from typing import List, Optional
from pathlib import Path
import pandas as pd
from time import time
from datetime import timedelta
from dataset2rdf.utils import (
    DEFAULT_PREFIX,
    add_metadata_to_config,
    apply_inherited_restrictions,
    apply_type_restrictions,
    load_coding_system_information,
    load_dataset,
    load_metadata,
    parse_coding_system_information,
    parse_dataset,
    load_cardinalities,
    parse_cardinalities,
    apply_cardinalities,
    add_project_metadata_to_config,
    infer_domain,
    parse_metadata
)
from dataset2rdf.graph import OntologyGraph
from dataset2rdf.config import get_config, CONFIG, logger


@click.command()
@click.option("--dataset", required=True, help="Path to the SPHN Dataset (Excel spreadsheet)")
@click.option("--output", required=True, help="Output filename")
@click.option("--project-output", required=False, help="Output filename for project specific TTL")
@click.option("--config", help="Path to config.yaml")
@click.option("--logfile", required=False, help="The file where the logs go to")
@click.option("--debug", required=False, help="Debugging output enabled, boolean variable")
@click.option("--extras", required=False, multiple=True, help="Any extra triples (as TTL files) to add to the output")
def cli(dataset: Path, output: Path, project_output: Path, config: Path, debug: str, logfile: Path, extras: List) -> None:
    main(dataset=dataset, output=output, project_output=project_output, config=config, debug=debug, logfile=logfile, extras=extras)


def main(dataset: Path, output: Path, project_output: Path, config: Path, debug: str, logfile: Path, extras: Optional[List] = []) -> None:
    """
    Convert SPHN Dataset to SPHN RDF Schema.

    Args:
        dataset: Path to the SPHN Dataset (Excel spreadsheet)
        output: Output filename
        project_output: Output filename for project specific TTL
        config: Path to ``config.yaml``
        extras: Any extra triples (as TTL files) to add to the output

    """
    startTime = time()
    if debug == 'true':
        logger.setLevel(logging.DEBUG)
    if logfile:
        logger.handlers.clear()
        handler = logging.FileHandler(filename=logfile)
        fmt = logging.Formatter("[%(name)s][%(levelname)s][%(filename)s:%(lineno)s] %(message)s")
        handler.setFormatter(fmt)
        logger.addHandler(handler)

    config_obj = get_config(config)

    metadata_obj = {}
    try:
        metadata = load_metadata(filename=dataset, worksheet=config_obj.worksheet['metadata'])
        metadata_obj = parse_metadata(metadata)
    except:
        logger.info(f"'{config_obj.worksheet['metadata']}' sheet not found; Trying to find 'Project Metadata' sheet")
        try:
            metadata = load_metadata(filename=dataset, worksheet="Project Metadata")
            metadata_obj = parse_metadata(metadata)
        except:
            logger.info(f"No sheet with Metadata found; Using default config from config.yaml")

    if metadata_obj:
        if DEFAULT_PREFIX in metadata_obj:
            logger.info("Generating RDF Schema with metadata from the 'Metadata' sheet")
            add_metadata_to_config(DEFAULT_PREFIX, metadata_obj, config_obj)
            del metadata_obj[DEFAULT_PREFIX]
        else:
            logger.info("Generating RDF Schema using default config from config.yaml")

        if not metadata_obj and project_output:
            raise ValueError(f"No metadata found for project in 'Metadata' sheet")

        if len(metadata_obj) > 1:
            raise ValueError(f"Found more than one project in 'Metadata' sheet")

        if metadata_obj:
            logger.info("Generating project-specific RDF schema with metadata from the 'Metadata' sheet")
            add_project_metadata_to_config(metadata_obj, config_obj)

    release_notes = pd.read_excel(dataset, sheet_name="Release Notes", keep_default_na=False)
    release_versions = []
    for row in release_notes.to_dict(orient="records"):
        release_versions.append(str(row['Release']).strip())
    if project_output:
        if config_obj.template_version not in release_versions:
            logger.warning(f"The version of the template you are using is does not match with the latest version '{config_obj.template_version}'")

    try:
        dataset_df = load_dataset(filename=dataset, worksheet=config_obj.worksheet['concepts'], config=config_obj)
    except ValueError as e:
        raise e

    #if os.path.exists("input/SPHN_Dataset_2023-2.xlsx"):
        #compare_dataset_with_sphn_dataset(dataset_df)

    coding_system_info_df = load_coding_system_information(dataset)
    coding_system_info = parse_coding_system_information(coding_system_info_df, config=config_obj)
    for k, v in coding_system_info.items():
        if v.provided_by.lower() == config_obj.prefix:
            config_obj.imports[k] = v
        else:
            if config_obj.project_metadata:
                config_obj.project_metadata.imports[k] = v

    cardinalities_df = load_cardinalities(filename=dataset, worksheet=config_obj.worksheet['concepts'], config=config_obj)

    # Extract concepts and attributes from the Dataset
    logger.info("Reading the dataset.. ")
    concepts, properties, individuals = parse_dataset(
        filename=dataset, dataset=dataset_df, config=config_obj
    )
    # Extract cardinalities from the Dataset
    cardinalities = parse_cardinalities(cardinalities=cardinalities_df, properties=properties, config=config_obj)
    apply_cardinalities(concepts=concepts, properties=properties, cardinalities=cardinalities)
    infer_domain(concepts=concepts, properties=properties)
    apply_type_restrictions(concepts=concepts, properties=properties, config=config_obj)
    apply_inherited_restrictions(concepts=concepts, properties=properties)
    # Build the RDF schema
    g = OntologyGraph(config_obj)
    logger.info("Generating the RDF graph...")
    g.add_classes(concepts=concepts, config=config_obj)
    g.add_properties(concepts=concepts, properties=properties)
    g.add_named_individuals(individuals=individuals)
    for concept in concepts.values():
        g.apply_restrictions(concept, config=config_obj)
    for extra in extras:
        g.add_triples(extra)
    g.export(filename=output, format="turtle")
    if project_output:
        g.export_project(filename=project_output, format="turtle")

    logger.info("Job finished. Graph exported.")
    stopTime = time()
    logger.info("Execution took: " + str(timedelta(seconds=(stopTime-startTime))))

    for handler in logger.handlers:
        if isinstance(handler, logging.FileHandler):
            logger.removeHandler(handler)


if __name__ == "__main__":
    cli()
