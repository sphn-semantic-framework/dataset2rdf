import sys
import yaml
import logging
from pathlib import Path
from typing import List, Dict, Optional
from pydantic import BaseModel
from functools import lru_cache

from dataset2rdf.models import Concept, Property


class ResourceIri(BaseModel):
    """
    Class that represents IRIs for a particular resource.
    """

    canonical_iri: str
    versioned_iri: Optional[str] = None


class BaseConfig(BaseModel):
    """
    Class that represents a base config.
    """

    prefix: str
    iri: ResourceIri
    create_root_node: Optional[bool] = None
    root_node: Optional[List] = None


class ImportConfig(BaseConfig):
    """
    Class that represents an import-specific config.
    """
    provided_by: Optional[str] = None


class CoreConfig(BaseConfig):
    """
    Class that represents core configuration for the dataset2rdf.
    """

    title: Optional[str] = None
    description: Optional[str] = None
    license: Optional[str] = None
    version: str
    prior_version: Optional[str] = None
    template_version: Optional[str] = None
    copyright: Optional[str] = None
    citation: Optional[str] = None
    deprecated_prefix: Optional[str] = None
    deprecated_iri: Optional[str] = None


class ProjectConfig(CoreConfig):
    """
    Class that represents project specific configuration for the dataset2rdf.
    """

    imports: Optional[Dict[str, ImportConfig]] = {}
    properties: Dict[str, Property] = {}
    concepts: Dict[str, Concept] = {}

class Config(CoreConfig):
    """
    Class that represents configuration for the dataset2rdf.
    """

    imports: Dict[str, ImportConfig] = {}
    concepts: Dict[str, Concept] = {}
    properties: Dict[str, Property] = {}
    worksheet: Dict[str, str] = {
        "concepts": "Concepts",
        "cardinalities": "Cardinalities",
        "metadata": "Metadata"
    }
    project_metadata: Optional[ProjectConfig] = None


CONFIG = Path(__file__).parent.resolve() / "config.yaml"


@lru_cache()
def load_config(filename: str) -> Config:
    """
    Load the config YAML.

    Args:
        filename: The config YAML

    Returns:
        A dictionary with the config

    """
    logger.info(f"Loading config from {Path(filename).resolve()}")
    with open(filename, encoding="utf-8") as stream:
        config_data = yaml.load(stream, Loader=yaml.FullLoader)
        if "worksheet" in config_data:
            if "project_metadata" in config_data["worksheet"]:
                config_data["worksheet"]["metadata"] = config_data["worksheet"]["project_metadata"]
                del config_data["worksheet"]["project_metadata"]
            elif "projectdefinition" in config_data["worksheet"]:
                config_data["worksheet"]["metadata"] = config_data["worksheet"]["projectdefinition"]
                del config_data["worksheet"]["projectdefinition"]
            if "version" in config_data:
                config_data["version"] = str(config_data["version"])
            if "prior_version" in config_data:
                config_data["prior_version"] = str(config_data["prior_version"])
            if "template_version" in config_data:
                config_data["template_version"] = str(config_data["template_version"])
        config = Config(**config_data)
        return config


def get_config(filename: str = CONFIG) -> Config:
    """
    Get a Config object corresponding to the given config YAML.

    Args:
        filename: The config YAML
    """
    config = load_config(filename)
    return config


@lru_cache()
def get_logger(name: str = 'dataset2rdf', filename: str = None, level: int = logging.INFO) -> logging.Logger:
    """
    Get the logger.

    Args:
        name: The name of the logger
        filename: The filename to write logs to
        level: The logging level

    Returns:
        An instance of ``logger.Logger``

    """
    logger = logging.getLogger(name)
    if filename:
        handler = logging.FileHandler(filename=filename)
    else:
        handler = logging.StreamHandler(stream=sys.stdout)
    fmt = logging.Formatter("[%(name)s][%(levelname)s][%(filename)s:%(lineno)s] %(message)s")
    handler.setFormatter(fmt)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger

logger = get_logger()