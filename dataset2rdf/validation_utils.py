import re
import copy
import pandas as pd
from typing import Dict, List

from dataset2rdf.config import Config, get_config, logger
from dataset2rdf.models import Property


COLUMN_MAP = {
    "release": "release",
    "active_status": "active status (yes/no)",
    "category": "concept or concept compositions or inherited",
    "name": "general concept name",
    "description": "general description",
    "meaning_binding": "meaning binding",
    "parent": "parent",
    "type": "type",
    "excluded_type": "excluded type descendants",
    "reference": "concept reference",
    "standard": "standard",
    "valueset": "value set or subset",
    "additional_information": "additional information",
    "deprecated_in": "deprecated in",
    "replaced_by": "replaced by",
    "cardinality": "cardinality for composedOf",
    "hasAdministrativeCase": "cardinality for concept to Administrative Case",
    "hasDataProvider": "cardinality for concept to Data Provider",
    "hasDataProviderInstitute": "cardinality for concept to Data Provider Institute",
    "hasSubjectPseudoIdentifier": "cardinality for concept to Subject Pseudo Identifier",
    "hasSourceSystem": "cardinality for concept to Source System",
    "sensitive": "sensitive (yes/no)"
}

CARDINALITIES_COLUMN_MAP = {
    "name": "general concept name",
    "category": "concept or concept compositions or inherited",
    "active_status": "active status (yes/no)",
    "reference": "concept reference",
    "name": "general concept name",
    "cardinality": "cardinality for composedOf",
    "hasAdministrativeCase": "cardinality for concept to Administrative Case",
    "hasDataProvider": "cardinality for concept to Data Provider",
    "hasDataProviderInstitute": "cardinality for concept to Data Provider Institute",
    "hasSubjectPseudoIdentifier": "cardinality for concept to Subject Pseudo Identifier",
    "hasSourceSystem": "cardinality for concept to Source System"
}

CODING_SYSTEM_INFO_COLUMN_MAP = {
    "short_name": "short name",
    "full_name": "full name",
    "version_pattern": "coding system and version",
    "example_version_pattern": "example",
    "provided_in_rdf": "provided in RDF (yes/no)",
    "downloadable_in_df": "Downloadable in RDF (yes/no)",
    "provided_by": "provided by",
    "prefix": "prefix",
    "root_node": "root node",
    "canonical_iri": "canonical iri",
    "resource_prefix": "resource prefix",
    "resource_iri": "resource iri",
    "versioned_iri": "versioned iri"
}

DEFAULT_PREFIX = "sphn"
IDX_OFFSET = 2


def check_composed_of_general_description(data: pd.DataFrame) -> List:
    """
    Find composedOfs (and inherited composedOfs) that are identical but don't have the same general description.

    Args:
        data: Dataset as a Pandas DataFrame

    Returns:
        A list of errors (if any)

    """
    errors = []
    data_test = data.loc[((data['active status (yes/no)'] == "yes") & (data['concept or concept compositions or inherited'] != "concept")), :]
    data_test = data_test.loc[(data_test.duplicated(subset=['general concept name'], keep=False)), :]
    data_test['general description'] = data_test['general description'].str.strip()
    data_test = data_test.loc[(~data_test.duplicated(subset=['general concept name','general description'], keep=False))]
    if not data_test.empty:
        for index, row in data_test.iterrows():
            logger.warning(f"[Row {row['row_number']+IDX_OFFSET}] composedOf '{row[COLUMN_MAP['name']]}' occurs more than once with a different 'general description'. Only the first description is taken.")
    return errors


def get_project_start_row(data: pd.DataFrame) -> int:
    """
    Get the starting row number for project concepts.

    Args:
        data: Dataset as a Pandas DataFrame

    Returns:
        The row number

    """
    project_start_row = None
    data_ps = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes")
                        & (data[COLUMN_MAP["name"]].str.contains(":"))) , :]    
    if not data_ps.empty:
        project_start_row = data_ps.iloc[0]["row_number"]
    return project_start_row


def check_concept_general_description(data: pd.DataFrame) -> List:
    """
    Check if the general description of an extended SPHN concept is consistent
    with its original general description.

    Args:
        data: Dataset as a Pandas DataFrame

    Returns:
        A list of errors (if any)

    """
    errors = []
    data_ps = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes")
                        & (data[COLUMN_MAP["name"]].str.contains(":"))) , :]
    if not data_ps.empty:
        if not data_ps.empty:
            data_ps_concepts = data_ps.loc[(data[COLUMN_MAP["category"]] == "concept"), :]
            data_ps_concepts = data_ps_concepts.loc[(data_ps_concepts.duplicated(subset=[COLUMN_MAP["name"]], keep=False)), :]
            if not data_ps_concepts.empty:
                grouped = data_ps_concepts.groupby(COLUMN_MAP["name"])
                for name, group in grouped:
                    first_row = None
                    duplicate_rows = []
                    for index, row in group.iterrows():
                        if not first_row:
                            first_row = row["row_number"]
                        duplicate_rows.append(row["row_number"])
                    errors.append(f"[Row {first_row+IDX_OFFSET}] Project concept '{name}' occurs more than once ({', '.join([f'Row {x+IDX_OFFSET}' for x in duplicate_rows])})")
    return errors


def check_concept_multiple_occurrence(data: pd.DataFrame) -> List:
    """
    Check if a concept occurs more than once in the dataset.

    Args:
        data: Dataset as a Pandas DataFrame

    Returns:
        A list of errors (if any)

    """
    errors = []
    data_test = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes") & (data[COLUMN_MAP["category"]] == "concept")), :]
    data_test = data_test.loc[(data_test.duplicated(subset=COLUMN_MAP["name"], keep=False)), :]
    if not data_test.empty:
        grouped = data_test.groupby(COLUMN_MAP["name"])
        for name, group in grouped:
            first_row = None
            duplicate_rows = []
            for index, row in group.iterrows():
                if not first_row:
                    first_row = row["row_number"]
                duplicate_rows.append(row["row_number"])
            errors.insert(0, f"[Row {first_row+IDX_OFFSET}] SPHN concept '{name}' occurs more than once ({', '.join([f'Row {x+IDX_OFFSET}' for x in duplicate_rows])})")
    return errors

def check_concept_extension(data: pd.DataFrame) -> List:
    """
    Check if a project-specific extension to a concept is consistent
    where all the inherited composedOfs are repeated and not altered

    Args:
        data: Dataset as a Pandas DataFrame

    Returns:
        A list of errors (if any)

    """
    error_set = set()
    data_test = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes") & (data[COLUMN_MAP["category"]] == "concept")), :]
    data_test = data_test.loc[(data_test.duplicated(subset=COLUMN_MAP["name"], keep=False)), :]
    if not data_test.empty:
        grouped = data_test.groupby(COLUMN_MAP["name"])
        for name, group in grouped:
            representative_concept_row = None
            for concept_row in group.to_dict('records'):
                if not representative_concept_row:
                    representative_concept_row = concept_row
                    filtered_representative_concept_row = filter_record(representative_concept_row, COLUMN_MAP.values())
                else:
                    filtered_row = filter_record(concept_row, COLUMN_MAP.values())
                    if filtered_representative_concept_row != filtered_row:
                        mismatched_keys = compare_record(filtered_representative_concept_row, filtered_row, filtered_representative_concept_row.keys())
                        mismatched_keys_str = ', '.join(f"'{x}'" for x in mismatched_keys)
                        error_set.add(f"[Row {concept_row['row_number']+IDX_OFFSET}] Concept '{name}' is repeated but with different values for column {mismatched_keys_str}")
                    # for each group fetch composedOfs
                    data_test2 = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes") & (data[COLUMN_MAP["category"]] != "concept") & (data[COLUMN_MAP["reference"]] == representative_concept_row[COLUMN_MAP["reference"]])), :]
                    data_test2 = data_test2.loc[(data_test2.duplicated(subset=COLUMN_MAP["name"], keep=False)), :]
                    if not data_test2.empty:
                        grouped2 = data_test2.groupby(COLUMN_MAP["name"])
                        for name2, group2 in grouped2:
                            representative_composed_of_row = None
                            for composed_of_row in group2.to_dict('records'):
                                if not representative_composed_of_row:
                                    representative_composed_of_row = composed_of_row
                                    filtered_representative_composed_of_row = filter_record(representative_composed_of_row, COLUMN_MAP.values())
                                else:
                                    filtered_composed_of_row = filter_record(composed_of_row, COLUMN_MAP.values())
                                    if filtered_representative_composed_of_row != filtered_composed_of_row:
                                        mismatched_keys = compare_record(filtered_representative_composed_of_row, filtered_composed_of_row, filtered_representative_composed_of_row.keys())
                                        mismatched_keys_str = ', '.join(f"'{x}'" for x in mismatched_keys)
                                        error_set.add(f"[Row {composed_of_row['row_number']+IDX_OFFSET}] Repeated concept '{name}' has a composedOf '{name2}' with different values for column {mismatched_keys_str}")
    return list(error_set)


def check_inherited_project_concepts(data: pd.DataFrame, data_ps: pd.DataFrame, config: Config = get_config()) -> List:
    """
    Check if project concept inherits an SPHN concept and yet does not list all the inherited composedOfs.

    Args:
        data: Dataset as a Pandas DataFrame
        data_ps: Project Dataset as a Pandas DataFrame
        config: The config

    Returns:
        A list of errors (if any)

    """
    errors = []
    grouped = data_ps.groupby(COLUMN_MAP["reference"])
    for name, group in grouped:
        group_dict = group.to_dict('records')
        concept_row = group_dict[0]
        if concept_row[COLUMN_MAP["category"]] != "concept":
            logger.warning(f"[Row {concept_row['row_number'] + IDX_OFFSET}] Concept '{name}' used as a '{COLUMN_MAP['reference']}' but does not have a corresponding concept row")
            continue
        concept_parent = concept_row[COLUMN_MAP["parent"]]
        if not concept_parent.endswith("Concept"):
            composed_of_set = set()
            inherited_composed_of_set = set()
            inherited_composed_of_parent = {}
            for x in group_dict[1:]:
                if x[COLUMN_MAP["category"]] == "inherited":
                    inherited_composed_of_set.add(x[COLUMN_MAP["name"]])
                    inherited_composed_of_parent[x[COLUMN_MAP['name']]] = x[COLUMN_MAP['parent']]
                else:
                    composed_of_set.add(x[COLUMN_MAP["name"]])
            all_composed_of_set = composed_of_set.union(inherited_composed_of_set)
            parent_concept_row = get_record_from_iri(data, concept_parent, config)
            if parent_concept_row:
                parent_concept_block = data.loc[(data[COLUMN_MAP["reference"]] == parent_concept_row[COLUMN_MAP["name"]])]
                parent_composed_of_set = set()
                for index, record in parent_concept_block.iterrows():
                    if record[COLUMN_MAP["category"]] in {"composedOf", "inherited"}:
                        parent_composed_of_set.add(record[COLUMN_MAP["name"]])
                if not parent_composed_of_set.issubset(inherited_composed_of_set):
                    warn = f"[Row {concept_row['row_number'] + IDX_OFFSET}] Concept '{concept_row[COLUMN_MAP['name']]}' inherits from {concept_parent} but all inherited composedOfs are not listed."
                    common = parent_composed_of_set.intersection(composed_of_set)
                    missing = parent_composed_of_set.difference(all_composed_of_set)
                    checked_missing = set()
                    for m in missing:
                        if is_curie(m):
                            m_prefix, m_name = m.split(":")
                            formatted_m = f"{m_prefix}:{format_property_name(m_name)}"
                        else:
                            formatted_m = format_property_name(m)
                        for k,v in inherited_composed_of_parent.items():
                            if v == formatted_m:
                                error = f"[Row {concept_row['row_number'] + IDX_OFFSET}] inherited composedOf '{m}' renamed as '{k}' in concept '{concept_row[COLUMN_MAP['name']]}'."
                                logger.warning(error)
                                k_record = get_record(data, COLUMN_MAP['name'], k)
                                if is_curie(k):
                                    if not is_curie(m):
                                        error += " Renaming an inherited SPHN composedOf to a project composedOf is not permitted."
                                        errors.append(error)
                                else:
                                    if k_record and k_record[COLUMN_MAP['reference']] != concept_row[COLUMN_MAP['name']]:
                                        checked_missing.add(m)
                                    else:
                                        error += f" But '{k}' does not exist."
                                        errors.append(error)
                                warn = None
                                break
                    missing = missing.difference(checked_missing)
                    if common:
                        if warn:
                            warn += f" This is due to the following composedOfs not being marked as 'inherited': {', '.join(quotify(sorted(common)))}."
                    if missing:
                        if warn:
                            warn += f" The following parent composedOfs are missing: {', '.join(quotify(sorted(missing)))}"
                    if (common or missing) and warn:
                        logger.warning(warn)
            else:
                logger.warning(f"[Row {concept_row['row_number'] + IDX_OFFSET}] Cannot find parent '{concept_parent}' for concept '{name}'")
    return errors


def check_column_names(data: pd.DataFrame, column_map: Dict, config: Config = get_config()) -> List:
    """
    Check if expected columns exist in the given DataFrame.

    Args:
        data: Dataset as a Pandas DataFrame
        column_map: The columns that are expected.
        config: The config

    Returns:
        A list of errors (if any)

    """
    errors = []
    # find missing columns
    invalid_column_names = set()
    for column_name in column_map.values():
        if column_name not in data.columns:
            invalid_column_names.add(column_name)
    if config.version.startswith("2025") and CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"] in invalid_column_names:
        invalid_column_names.remove(CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"])
    if config.version.startswith("2025") and CARDINALITIES_COLUMN_MAP["hasDataProvider"] in invalid_column_names:
        invalid_column_names.remove(CARDINALITIES_COLUMN_MAP["hasDataProvider"])
    if config.version.startswith("2024") and CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"] in invalid_column_names:
        invalid_column_names.remove(CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"])
    if config.version.startswith("2023") and CARDINALITIES_COLUMN_MAP["hasDataProvider"] in invalid_column_names:
        invalid_column_names.remove(CARDINALITIES_COLUMN_MAP["hasDataProvider"])
    if config.version.startswith("2023") and CARDINALITIES_COLUMN_MAP["hasSourceSystem"] in invalid_column_names:
        invalid_column_names.remove(CARDINALITIES_COLUMN_MAP["hasSourceSystem"])
    if config.version.startswith("2023") and COLUMN_MAP["sensitive"] in invalid_column_names:
        invalid_column_names.remove(COLUMN_MAP["sensitive"])
    if config.version.startswith("2023") and COLUMN_MAP["excluded_type"] in invalid_column_names:
        invalid_column_names.remove(COLUMN_MAP["excluded_type"])

    if invalid_column_names:
        e = f"Found missing column name(s):{invalid_column_names}."
        errors.append(e)
    return errors


def check_project_concepts(data: pd.DataFrame, data_ps: pd.DataFrame, start_row: int, config: Config = get_config()) -> List:
    """
    Check project concepts for consistency.

    Args:
        data: A pandas DataFrame corresponding to the Dataset
        start_row: The starting row for project concepts
        config: The config object

    Returns:
        A list of errors, if any

    """
    errors = []
    # get all active records
    for record in data.to_dict('records'):
        if record['row_number'] >= start_row:
            # Check only project records
            category = sanitize(record[COLUMN_MAP["category"]])
            if category in {'concept'}:
                # parsing project concept block
                errors.extend(check_project_concept(data, data_ps, start_row, record, config))
            elif category in {'composedOf'}:
                # parsing project concept's composedOf
                errors.extend(check_project_composed_of(data, data_ps, start_row, record, config))
            elif category in {'inherited'}:
                # parsing project concept's inherited composedOf
                errors.extend(check_project_inherited_composed_of(data, data_ps, start_row, record, config))
    return errors


def check_project_concept(dataset: pd.DataFrame, data_ps: pd.DataFrame, start_row: int, record: Dict, config: Config = get_config()) -> List:
    """
    Check project concepts for consistency.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        start_row: The starting row for project concepts
        record: A dictionary corresponding to the concept record
        config: The config object

    Returns:
        A list of errors, if any

    """
    errors = []
    name = sanitize(record[COLUMN_MAP['name']])
    check_project_concept_name(record, name)
    description = sanitize(record[COLUMN_MAP['description']])
    if not description:
        logger.warning(f"[Row {record['row_number']+IDX_OFFSET}] Project concept '{name}' does not have a '{COLUMN_MAP['description']}'")
    concept_iri = get_iri_from_record(record)
    errors.extend(check_iri(record, iri=concept_iri))
    concept_prefix = None
    if is_curie(name):
        concept_prefix, concept_name = [x.strip() for x in name.split(":", 1)]
    if concept_prefix and concept_prefix != DEFAULT_PREFIX:
        # project concept
        if not compare_iri(concept_iri, get_namespace_iri(config.project_metadata.iri.canonical_iri), strategy="startswith"):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project concept '{name}' has an invalid IRI '{concept_iri}'. Project concept should have project IRI")
        concept_parent = sanitize(record['parent'])
        logger.debug(f"[check_project_concept] Project concept '{name}' with parent '{concept_parent}'")
        if concept_parent.lower().endswith('concept'):
            expected_parent = f"{concept_prefix}:{concept_prefix.upper()}Concept"
            if concept_parent != expected_parent:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project concept '{name}' should have '{expected_parent}' as its parent instead of '{concept_parent}'")
        else:
            # concept has a parent
            concept_type = sanitize(record['type'])
            concept_parent_record = get_record(dataset, COLUMN_MAP['name'], concept_type)
            if concept_parent_record:
                errors.extend(compare_concept_cardinality(dataset, concept_parent_record, record))
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Cannot find parent '{concept_type}' for concept '{concept_name}'")
        errors.extend(check_concept_parent(dataset, data_ps, record))
    else:
        # SPHN concept
        if not compare_iri(concept_iri, get_namespace_iri(config.iri.canonical_iri), strategy="startswith"):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN concept '{name}' has an invalid IRI '{concept_iri}'. SPHN concept should have SPHN IRI")
        concept_parent = sanitize(record['parent'])
        expected_parent = "SPHNConcept"
        if concept_parent.lower().endswith('concept'):
            if concept_parent != expected_parent:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN concept '{name}' should have '{expected_parent}' as its parent instead of '{concept_parent}'")
        else:
            # concept has a parent
            concept_type = sanitize(record['type'])
            concept_parent_record = get_record(dataset, COLUMN_MAP['name'], concept_type)
            if concept_parent_record:
                errors.extend(compare_concept_cardinality(dataset, concept_parent_record, record))
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Cannot find parent '{concept_type}' for concept '{concept_name}'")
        errors.extend(check_concept_parent(dataset, data_ps, record))
    return errors


def check_project_valueset(record: Dict) -> List:
    errors = []
    name = sanitize(record[COLUMN_MAP['name']])
    reference = sanitize(record[COLUMN_MAP['reference']])
    standard = sanitize(record[COLUMN_MAP['standard']])
    valueset = sanitize(record[COLUMN_MAP['valueset']])
    prop_type = sanitize(record[COLUMN_MAP['type']])
    if valueset:
        if not standard:
            if prop_type != 'qualitative':
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' has a custom valueset '{valueset}' but the property type is '{prop_type}' instead of 'qualitative'.")
        if 'restricted to:' in valueset.lower() or 'descendant of:' in valueset.lower():
            if not standard:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' has values restricted to a valueset '{valueset}' but no 'standard' is provided.")
    return errors


def check_project_concept_name(record: Dict, name: str):
    if is_curie(name):
        prefix, reference = name.split(":", 1)
        if not reference.istitle():
            logger.warning(f"[Row {record['row_number']+IDX_OFFSET}] Project concept name '{name}' contains an acronym and this is highly discouraged.")


def check_iri(record: Dict, iri: str) -> List:
    """
    Check IRI of a concept or composedOf to see if it is correctly formatted
    and corresponds to the name.

    Args:
        record: A dictionary corresponding to the concept or composedOf record
        iri: The IRI of a concept or composedOf

    Returns:
        A list of errors, if any

    """
    errors = []
    name = sanitize(record[COLUMN_MAP['name']])
    if ':' in name:
        name = name.split(':', 1)[1]
    category = sanitize(record[COLUMN_MAP['category']])
    record_type = 'concept' if category == 'concept' else 'composedOf'
    elements = iri.split("/")
    last_element = elements[-1]
    base_iri = '/'.join(elements[0:-1])
    if "#" in last_element:
        e, reference = last_element.split("#", 1)
        base_iri += f'/{e}#'
    else:
        reference = last_element
        base_iri += '/'
    if category == 'concept':
        expected_reference = format_class_name(sanitize(name))
    else:
        expected_reference = format_property_name(sanitize(name))

    if reference != expected_reference:
        errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {record_type} '{name}' is expected to have an IRI '{base_iri}{expected_reference}' instead of '{base_iri}{reference}'.")
    return errors


def check_concept_parent(dataset: pd.DataFrame, data_ps: pd.DataFrame, record: Dict) -> List:
    errors = []
    name = record[COLUMN_MAP['name']]
    if is_curie(name):
        prefix = name.split(":", 1)[0]
        namespace = "Project"
    else:
        prefix = "SPHN"
        namespace = "SPHN"

    concept_parent = sanitize(record[COLUMN_MAP['parent']])
    concept_type = sanitize(record[COLUMN_MAP['type']])
    if concept_parent.lower().endswith('concept'):
        if concept_type:
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} concept '{name}' has parent '{concept_parent}' with type '{concept_type}'; If parent is a root concept then type should remain empty")
    else:
        if concept_parent != concept_type.replace(' ', ''):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} concept '{name}' has parent '{concept_parent}' with type '{concept_type}'; Type and parent should always refer to the same concept")
    return errors


def check_project_composed_of(dataset: pd.DataFrame, data_ps: pd.DataFrame, start_row: int, record: Dict, config: Config = get_config()) -> List:
    """
    Check project composedOfs for consistency.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        start_row: The starting row for project concepts
        record: A dictionary corresponding to the composedOf record
        config: The config object

    Returns:
        A list of errors, if any

    """
    errors = []
    composed_of_prefix = None
    composed_of_iri = get_iri_from_record(record)
    name = sanitize(record[COLUMN_MAP['name']])
    reference = sanitize(record[COLUMN_MAP['reference']])
    description = sanitize(record[COLUMN_MAP['description']])
    errors.extend(check_iri(record, iri=composed_of_iri))
    if not description:
        logger.warning(f"[Row {record['row_number']+IDX_OFFSET}] project composedOf '{name}' does not have a '{COLUMN_MAP['description']}'")
    if is_curie(name):
        composed_of_prefix, composed_of_name = [x.strip() for x in name.split(":", 1)]
    errors.extend(check_project_valueset(record))
    if composed_of_prefix and composed_of_prefix != DEFAULT_PREFIX:
        # project composedOf in project concept
        logger.debug(f"[check_project_composed_of] Project composedOf '{name}' used in project concept '{reference}'")
        if not compare_iri(composed_of_iri, get_namespace_iri(config.project_metadata.iri.canonical_iri), strategy="startswith"):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' has an invalid IRI '{composed_of_iri}'. Project composedOf should have project IRI")
        errors.extend(check_composed_of_parent(dataset, data_ps, record, config))
        composed_of_parent = sanitize(record['parent'])
        if composed_of_parent.lower().endswith('attributeobject') or composed_of_parent.lower().endswith('attributedatatype'):
            expected_parent = f"{composed_of_prefix}:{composed_of_prefix.upper()}{'AttributeObject' if composed_of_parent.lower().endswith('attributeobject') else 'AttributeDatatype'}"
            if composed_of_parent != expected_parent:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' should have '{expected_parent}' as its parent instead of '{composed_of_parent}'")
        else:
            # if composedOf has a parent then check type
            logger.debug(f"[check_project_concepts] project composedOf '{name}' has parent '{composed_of_parent}'")
            composed_of_parent_record = get_record_from_iri(dataset, composed_of_parent, config)
            if composed_of_parent_record:
                errors.extend(compare_composed_of_type(dataset, upstream_record=composed_of_parent_record, record=record, config=config))
            else:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' has a parent '{composed_of_parent}' that does not exist")
    else:
        reference_prefix = None
        if is_curie(reference):
            reference_prefix, reference_name = [x.strip() for x in reference.split(":", 1)]
        if reference_prefix and reference_prefix != DEFAULT_PREFIX:
            # sphn composedOf in project concept
            logger.debug(f"[check_project_concepts] SPHN composedOf '{name}' used in project concept '{reference}'")
            errors.extend(check_composed_of_parent(dataset, data_ps, record, config))
            if not compare_iri(composed_of_iri, get_namespace_iri(config.iri.canonical_iri), strategy="startswith"):
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' has an invalid IRI '{composed_of_iri}'. SPHN composedOf should have SPHN IRI")
            # check if composedOf exists in SPHN
            subset = dataset.loc[(dataset['row_number'] < start_row)]
            composed_of_record = get_record(subset, COLUMN_MAP['name'], name)
            if not composed_of_record:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' does not exist. If this is a project composedOf then the name should have a project prefix")
            # if composedOf has a parent then check type
            composed_of_parent = sanitize(record['parent'])
            if composed_of_parent.lower().endswith('attributeobject') or composed_of_parent.lower().endswith('attributedatatype'):
                expected_parent = f"{'SPHNAttributeObject' if composed_of_parent.lower().endswith('attributeobject') else 'SPHNAttributeDatatype'}"
                if composed_of_parent != expected_parent:
                        errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' should have '{expected_parent}' as its parent instead of '{composed_of_parent}'. If this is a project composedOf then the name should have a project prefix")
            else:
                # if composedOf has a parent then check type
                logger.debug(f"[check_project_concepts] SPHN composedOf '{name}' has parent '{composed_of_parent}'")
                composed_of_parent_record = get_record_from_iri(dataset, composed_of_parent, config)
                if composed_of_parent_record:
                    errors.extend(compare_composed_of_type(dataset, upstream_record=composed_of_parent_record, record=record, config=config))
                else:
                        errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' has a parent '{composed_of_parent}' that does not exist")
        else:
            # sphn composedOf in SPHN concept (project extension)
            logger.debug(f"[check_project_concepts] SPHN composedOf '{name}' used in SPHN concept '{reference}'")
            # check if composedOf exists in SPHN
            subset = dataset.loc[(dataset['row_number'] < start_row)]
            composed_of_record = get_record(subset, COLUMN_MAP['name'], name)
            if not composed_of_record:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' does not exist. If this is a project composedOf then the name should have a project prefix")
            if not compare_iri(composed_of_iri, get_namespace_iri(config.iri.canonical_iri), strategy="startswith"):
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' has an invalid IRI '{composed_of_iri}'. SPHN composedOf should have SPHN IRI")
            errors.extend(check_composed_of_parent(dataset, data_ps, record, config))

    return errors


def check_composed_of_parent(dataset: pd.DataFrame, data_ps: pd.DataFrame, record: Dict, config: Config = get_config()):
    errors = []
    name = record[COLUMN_MAP['name']]
    if is_curie(name):
        prefix = name.split(":", 1)[0]
        namespace = "Project"
    else:
        prefix = "SPHN"
        namespace = "SPHN"
    expected_ao_parent = "Attribute Object"
    expected_ad_parent = "Attribute Datatype"
    composed_of_type = record[COLUMN_MAP['type']]
    composed_of_parent = record[COLUMN_MAP['parent']]
    if composed_of_type in {'datetime', 'string', 'double', 'qualitative', 'quantitative', 'temporal'}:
        if composed_of_parent.lower().endswith('attributeobject'):
            category = sanitize(record[COLUMN_MAP['category']])
            valueset = sanitize(record[COLUMN_MAP['valueset']])
            if category == 'composedOf':
                if not valueset:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{name}' has type '{composed_of_type}'. Either '{COLUMN_MAP['valueset']}' should be defined OR the parent should be an {expected_ad_parent}")
            elif category == 'inherited':
                ...
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{name}' has type '{composed_of_type}'. Thus its parent should be an {expected_ad_parent}")
        else:
            if not composed_of_parent.lower().endswith('attributedatatype'):
                parent_record = get_record_from_iri(dataset, composed_of_parent, config)
                if parent_record:
                    errors.extend(check_composed_of_parent(dataset, data_ps, parent_record, config))
                    # if not parent_record[COLUMN_MAP['parent']].lower().endswith('attributedatatype'):
                    #     errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{name}' has type '{composed_of_type}'. Thus its parent should be an {expected_ad_parent}")
                else:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{name}' has parent '{composed_of_parent}' that does not exist")
    else:
        composed_of_type_list = composed_of_type.split(";")
        for type in composed_of_type_list:
            composed_of_type_record = get_record(dataset, COLUMN_MAP['name'], type)
            if composed_of_type_record:
                if composed_of_parent.lower().endswith('attributedatatype'):
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{name}' has type '{type}'. Thus its parent should be an {expected_ao_parent}")
                else:
                    if not composed_of_parent.lower().endswith('attributeobject'):
                        errors.extend(_check_composed_of_parent(dataset, record, namespace, config))
    return errors


def _check_composed_of_parent(dataset: pd.DataFrame, record: Dict, namespace: str, config: Config = get_config()) -> List:
    """
    Check the parent of a composedOf

    Args:
        dataset: The dataset
        record: The record for the composedOf
        namespace: The namespace of the composedOf
        config: The config

    Returns:
        A list of errors, if any

    """

    errors = []
    parent_record = get_record_from_iri(dataset, record[COLUMN_MAP['parent']], config)
    if parent_record:
        if not parent_record[COLUMN_MAP['parent']].lower().endswith('attributeobject'):
            super_parent_record = get_record_from_iri(dataset, parent_record[COLUMN_MAP['parent']], config)
            if super_parent_record:
                errors.extend(_check_composed_of_parent(dataset, parent_record, namespace, config))
            else:
                errors.append(f"#[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{record[COLUMN_MAP['name']]}' has type '{record[COLUMN_MAP['type']]}'. Thus its parent should be an Attribute Object")
    else:
        errors.append(f"[Row {record['row_number']+IDX_OFFSET}] {namespace} composedOf '{record[COLUMN_MAP['name']]}' has parent '{record[COLUMN_MAP['parent']]}' that does not exist")
    return errors


def check_project_inherited_composed_of(dataset: pd.DataFrame, data_ps: pd.DataFrame, start_row: int, record: Dict, config: Config = get_config()) -> List:
    """
    Check inherited composedOfs in project concepts for consistency.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        start_row: The starting row for project concepts
        record: A dictionary corresponding to the composedOf record
        config: The config object

    Returns:
        A list of errors, if any

    """
    errors = []
    composed_of_prefix = None
    composed_of_iri = get_iri_from_record(record)
    name = sanitize(record[COLUMN_MAP['name']])
    reference = sanitize(record[COLUMN_MAP['reference']])
    description = sanitize(record[COLUMN_MAP['description']])
    if not description:
        logger.warning(f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' does not have a '{COLUMN_MAP['description']}'")
    if is_curie(name):
        composed_of_prefix, composed_of_name = [x.strip() for x in name.split(":", 1)]
    errors.extend(check_project_valueset(record))
    if composed_of_prefix and composed_of_prefix != DEFAULT_PREFIX:
        # project composedOf inherited and reused in a project concept
        logger.debug(f"[check_project_concepts] Project composedOf '{name}' inherited and reused in project concept '{reference}'")
        if not compare_iri(composed_of_iri, get_namespace_iri(config.project_metadata.iri.canonical_iri), strategy="startswith"):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Project composedOf '{name}' has an invalid IRI '{composed_of_iri}'. Project composedOf should have project IRI")
        composed_of_parent = sanitize(record[COLUMN_MAP['parent']])
        errors.extend(check_composed_of_parent(dataset, data_ps, record, config))
        if composed_of_parent.lower().endswith('attributeobject') or composed_of_parent.lower().endswith('attributedatatype'):
            expected_parent = f"{composed_of_prefix}:{composed_of_prefix.upper()}{'AttributeObject' if composed_of_parent.lower().endswith('attributeobject') else 'AttributeDatatype'}"
            if composed_of_parent != expected_parent:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' should have '{expected_parent}' as its parent instead of '{composed_of_parent}'")
        else:
            composed_of_parent_record = get_record_from_iri(dataset, composed_of_parent, config)
            if not composed_of_parent_record:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' has a parent '{composed_of_parent}' that does not exist")
        reference_record = get_record(dataset, COLUMN_MAP['name'], reference)
        if reference_record:
            reference_parent = reference_record[COLUMN_MAP['type']]
            if reference_parent:
                reference_parent_record = get_record(dataset, COLUMN_MAP['name'], reference_parent)
                if reference_parent_record:
                    condition = (dataset[COLUMN_MAP['name']].str.strip() == name) & (dataset[COLUMN_MAP['reference']].str.strip() == reference_parent.strip())
                    filtered_data = dataset[condition]
                    if filtered_data.empty:
                        subset = dataset[(dataset[COLUMN_MAP['reference']].str.strip() == reference_parent)]
                        composed_of_parent_record = get_record_from_iri(subset, composed_of_parent, config)
                        if composed_of_parent_record:
                            composed_of_parent_record_name = composed_of_parent_record[COLUMN_MAP['name']]
                            condition = (data_ps[COLUMN_MAP['name']].str.strip() == composed_of_parent_record_name) & (data_ps[COLUMN_MAP['reference']].str.strip() == reference_parent.strip())
                            filtered_data2 = data_ps[condition]
                            if filtered_data2.empty:
                                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' does not exist in parent concept '{reference_parent}'")
                        else:
                            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' has a parent '{composed_of_parent}' that does not exist")
                    else:
                        # check cardinality and type of the composedOf against its occurrence in the parent concept
                        upstream_record = filtered_data.iloc[0].to_dict()
                        if upstream_record:
                            errors.extend(compare_composed_of_cardinality(dataset, upstream_record=upstream_record, record=record))
                            errors.extend(compare_composed_of_type(dataset, upstream_record=upstream_record, record=record, config=config))
                            errors.extend(compare_composed_of_standard(dataset, upstream_record=upstream_record, record=record))
                        else:
                            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' does not exist in the parent concept '{reference_parent}'")
                else:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' has a reference '{reference}'. The reference concept inherits from concept '{reference_parent}' that does not exist")
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' has a reference '{reference}'. The reference concept does not have a 'type' defined")
        else:
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited project composedOf '{name}' has a reference '{reference}' that does not exist")
    else:
        # sphn composedOf inherited and reused in a project concept
        logger.debug(f"[check_project_concepts] SPHN composedOf '{name}' inherited and reused in project concept '{reference}'")
        if not compare_iri(composed_of_iri, get_namespace_iri(config.iri.canonical_iri), strategy="startswith"):
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] SPHN composedOf '{name}' has an invalid IRI '{composed_of_iri}'. SPHN composedOf should have SPHN IRI")
        composed_of_parent = sanitize(record['parent'])
        if composed_of_parent.lower().endswith('attributeobject') or composed_of_parent.lower().endswith('attributedatatype'):
            composed_of_types = record[COLUMN_MAP['type']].split(";")
            expected_parent = f"{'SPHNAttributeObject' if composed_of_parent.lower().endswith('attributeobject') else 'SPHNAttributeDatatype'}"
            for x in composed_of_types:
                x_record = get_record(dataset, COLUMN_MAP['name'], x)
                if x_record:
                    expected_parent = "SPHNAttributeObject"
            if composed_of_parent != expected_parent:
                #composed_of_types = [x.strip() for x in sanitize(record[COLUMN_MAP['type']]).split(';')]
                # if not any(is_curie(x) for x in composed_of_types):
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' should have '{expected_parent}' as its parent instead of '{composed_of_parent}'")
        else:
            composed_of_parent_record = get_record_from_iri(dataset, composed_of_parent, config)
            if composed_of_parent_record:
                errors.extend(check_composed_of_parent(dataset, data_ps, record, config))
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' has a parent '{composed_of_parent}' that does not exist")
        reference_record = get_record(dataset, COLUMN_MAP['name'], reference)
        if reference_record:
            reference_parent = reference_record[COLUMN_MAP['type']]
            if reference_parent:
                reference_parent_record = get_record(dataset, COLUMN_MAP['name'], reference_parent)
                if reference_parent_record:
                    condition = (dataset[COLUMN_MAP['name']].str.strip() == name) & (dataset[COLUMN_MAP['reference']].str.strip() == reference_parent.strip())
                    filtered_data = dataset[condition]
                    if filtered_data.empty:
                        composed_of_parent_record = get_record_from_iri(dataset, composed_of_parent, config)
                        if composed_of_parent_record:
                            composed_of_parent_record_name = composed_of_parent_record[COLUMN_MAP['name']]
                            condition = (dataset[COLUMN_MAP['name']].str.strip() == composed_of_parent_record_name) & (dataset[COLUMN_MAP['reference']].str.strip() == reference_parent.strip())
                            filtered_data2 = dataset[condition]
                            if filtered_data2.empty:
                                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' does not exist in parent concept '{reference_parent}'")
                        else:
                             errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' does not exist in parent concept '{reference_parent}'")
                    else:
                        # check cardinality and type of the composedOf against its occurrence in the parent concept
                        upstream_record = filtered_data.iloc[0].to_dict()
                        if upstream_record:
                            errors.extend(compare_composed_of_cardinality(dataset, upstream_record=upstream_record, record=record))
                            errors.extend(compare_composed_of_type(dataset, upstream_record=upstream_record, record=record, config=config))
                            errors.extend(compare_composed_of_standard(dataset, upstream_record=upstream_record, record=record))
                        else:
                            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' does not exist in the parent concept '{reference_parent}'")
                else:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' has a reference '{reference}'. The reference inherits from concept '{reference_parent}' that does not exist")
            else:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' has a reference '{reference}'. The reference concept does not have a 'type' defined")
        else:
            errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited SPHN composedOf '{name}' has a reference '{reference}' that does not exist")
    return errors


def compare_concept_cardinality(data: pd.DataFrame, parent_record: Dict, record: Dict) -> List:
    """
    Compare cardinalities associated with a concept to that of its parent.

    Args:
        data:  A pandas DataFrame corresponding to the Dataset
        parent_record: A dictionary corresponding to the row of the parent concept
        record: A dictionary corresponding to the row of the concept

    Returns:
        A list of errors, if any

    """
    errors = []
    keys = [
        COLUMN_MAP['hasAdministrativeCase'],
        COLUMN_MAP['hasSubjectPseudoIdentifier'],
    ]
    if COLUMN_MAP['hasDataProviderInstitute'] in record:
        keys.append(COLUMN_MAP['hasDataProviderInstitute'])
    if COLUMN_MAP['hasDataProvider'] in record:
        keys.append(COLUMN_MAP['hasDataProvider'])
    if COLUMN_MAP['hasSourceSystem'] in record:
        keys.append(COLUMN_MAP['hasSourceSystem'])

    name = sanitize(record[COLUMN_MAP['name']])
    parent_cardinalities = {k:parent_record[k] for k in keys}
    cardinalities = {k:record[k] for k in keys}
    for k in keys:
        cardinality_pattern1 = parent_cardinalities[k]
        cardinality_pattern2 = cardinalities[k]
        if cardinality_pattern1 and cardinality_pattern2:
            if cardinality_pattern1 == '0:1':
                if cardinality_pattern2 not in {'0:1', '1:1'}:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Concept '{name}' has '{k}' which is broader ({cardinality_pattern2}) than its parent ({cardinality_pattern1})")
            elif cardinality_pattern1 == '1:1':
                if cardinality_pattern2 not in {'1:1'}:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Concept '{name}' has '{k}' which is broader ({cardinality_pattern2}) than its parent ({cardinality_pattern1})")
            elif cardinality_pattern1 == '0:n':
                if cardinality_pattern2 not in {'0:1', '1:1', '0:n', '1:n'}:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Concept '{name}' has '{k}' which is unrecognized ({cardinality_pattern2}) when compared its parent ({cardinality_pattern1})")
            elif cardinality_pattern1 == '1:n':
                if cardinality_pattern2 not in {'1:n', '1:1'}:
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] Concept '{name}' has '{k}' which is broader ({cardinality_pattern2}) than its parent ({cardinality_pattern1})")
    return errors


def compare_composed_of_cardinality(data: pd.DataFrame, upstream_record: Dict, record: Dict) -> List:
    """
    Compare cardinalities associated with a composedOf to that of an upstream composedOf.

    Args:
        data:  A pandas DataFrame corresponding to the Dataset
        parent_record: A dictionary corresponding to the row of the upstream composedOf
        record: A dictionary corresponding to the row of the composedOf

    Returns:
        A list of errors, if any

    """
    errors = []
    name = sanitize(record[COLUMN_MAP['name']])
    upstream_reference = sanitize(upstream_record[COLUMN_MAP["reference"]])
    cardinality = record[COLUMN_MAP['cardinality']]
    upstream_cardinality = upstream_record[COLUMN_MAP['cardinality']]
    if upstream_cardinality and cardinality:
        if upstream_cardinality == '0:1':
            if cardinality not in {'0:1', '1:1'}:
                if not (upstream_reference in {'Variant Descriptor', 'Sample Processing'} and name in {'notation', 'output'}):
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' has cardinality which is broader ({cardinality}) than what is expected ({upstream_cardinality})")
        elif upstream_cardinality == '1:1':
            if cardinality not in {'1:1'}:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' has cardinality which is broader ({cardinality}) than what is expected ({upstream_cardinality})")
        elif upstream_cardinality == '0:n':
            if cardinality not in {'0:1', '1:1', '0:n', '1:n'}:
                errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' has cardinality which is unrecognized ({cardinality}) when compared to what is expected ({upstream_cardinality})")
        elif upstream_cardinality == '1:n':
            if cardinality not in {'1:n', '1:1'}:
                if not re.findall(r'[1-9]+[0-9]*:[n\d]+', record[COLUMN_MAP["cardinality"]]):
                    errors.append(f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' has cardinality which is broader ({cardinality}) than what is expected ({upstream_cardinality})")
    return errors



def validate_property_path(dataset: pd.DataFrame, record: Dict, property_path: List) -> bool:
    """
    Validate a given property path to ensure that all properties in the path exist.

    Args:
        dataset: The dataset
        record: The record for the property
        property_path: The property path

    Returns:
        A boolean on whether the property path is valid or not

    """
    if not isinstance(record, Dict):
        record = record.to_dict()
    prop_type = record[COLUMN_MAP["type"]]
    if ';' in prop_type:
        prop_types = [sanitize(x) for x in prop_type.split(";")]
    else:
        prop_types = [sanitize(prop_type)]
    valid_status = []
    for prop_type in prop_types:
        updated_record = copy.deepcopy(record)
        updated_record[COLUMN_MAP["type"]] = prop_type
        valid_status.append(_validate_property_path(dataset, updated_record, property_path.copy()))
    if any(not x for x in valid_status):
        valid_p = False
    else:
        valid_p = True
    return valid_p

def _validate_property_path(dataset: pd.DataFrame, record: Dict, property_path: List) -> bool:
    """
    Validate a given property path to ensure that all properties in the path exist.

    Args:
        dataset: The dataset
        record: The record for the property
        property_path: The property path

    Returns:
        A boolean on whether the property path is valid or not
    """
    property_path.reverse()
    p = sanitize(property_path.pop())
    prop_type = record[COLUMN_MAP["type"]]
    block = dataset.loc[((dataset[COLUMN_MAP["active_status"]].str.lower() == "yes") & (dataset[COLUMN_MAP["reference"]] == prop_type))]
    p_record = None
    valid_p = False
    for row in block.to_dict(orient="records"):
        if row[COLUMN_MAP["category"]] in {"composedOf", "inherited"}:
            if row[COLUMN_MAP["name"]] == p:
                valid_p = True
                p_record = row
    if valid_p:
        if property_path:
            property_path.reverse()
            return validate_property_path(dataset, p_record, property_path.copy())
    else:
        logger.error(f"Cannot find property '{p}' in concept '{prop_type}'")
    return valid_p



def compare_iri(iri1: str, iri2: str, strategy: str = "equal") -> bool:
    iri1 = iri1.replace('://www.', '://')
    iri2 = iri2.replace('://www.', '://')
    if not iri2.endswith('/'):
        iri2 += "/"
    if strategy == "startswith":
        return iri1.startswith(iri2)
    elif strategy == "endswith":
        return iri1.endswith(iri2)
    elif strategy == "equal":
        return iri1 == iri2
    else:
        return iri1 == iri2

def compare_composed_of_standard(dataset: pd.DataFrame, upstream_record: Dict, record: Dict) -> None:
    """
    Compare standard of a composedOf with its parent composedOf.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        upstream_record: Record corresponding to the parent composedOf
        record: Record corresponding to the current composedOf

    """
    errors = []
    name = record[COLUMN_MAP['name']]
    upstream_standard_list = [x.strip() for x in upstream_record[COLUMN_MAP['standard']].split(',')]
    standard_list = [x.strip() for x in record[COLUMN_MAP['standard']].split(',')]
    for standard in standard_list:
        if standard and standard not in upstream_standard_list:
            if not upstream_record[COLUMN_MAP['standard']]:
                continue
            elif 'other' in upstream_record[COLUMN_MAP['standard']].lower():
                continue
            else:
                e = f"[Row {record['row_number']+IDX_OFFSET}] inherited composedOf '{name}' has a standard '{standard}' that is not allowed since parent composedOf '{upstream_record[COLUMN_MAP['name']]}' allows only {upstream_record[COLUMN_MAP['standard']]}"
                errors.append(e)
    return errors


def compare_composed_of_valueset(dataset: pd.DataFrame, upstream_record: Dict, record: Dict) -> None:
    """
    Compare valueset of a composedOf with its parent composedOf.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        upstream_record: Record corresponding to the parent composedOf
        record: Record corresponding to the current composedOf

    """
    errors = []
    name = record[COLUMN_MAP['name']]
    upstream_valueset = [x.strip() for x in upstream_record[COLUMN_MAP['valueset']].split(';')]
    valueset = [x.strip() for x in record[COLUMN_MAP['valueset']].split(';')]
    # TODO: To be implemented.
    return errors


def compare_composed_of_type(dataset: pd.DataFrame, upstream_record: Dict, record: Dict, config: Config = get_config()) -> List:
    """
    Compare type of a composedOf with its upstream composedOf.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        upstream_record: Record corresponding to the upstream composedOf
        record: Record corresponding to the current composedOf

    Returns:
        A list of errors, if any

    """
    errors = []
    name = record[COLUMN_MAP['name']]
    upstream_composed_of_type = sanitize(upstream_record[COLUMN_MAP['type']])
    composed_of_type = sanitize(record[COLUMN_MAP['type']])
    upstream_composed_of_type_list = [x.strip() for x in upstream_composed_of_type.split(';')]
    composed_of_type_list = [x.strip() for x in composed_of_type.split(';')]
    invalid_types = set(composed_of_type_list)
    for upstream_type in upstream_composed_of_type_list:
        if upstream_type.lower().endswith('sphn concept'):
            invalid_types.clear()
            break
        elif upstream_type.lower().endswith('concept'):
            filtered_invalid_types = [x for x in invalid_types if not is_curie(x)]
            invalid_types = filtered_invalid_types
        for type in composed_of_type_list:
            if type in {'string', 'temporal', 'qualitative', 'quantitative'}:
                if type == upstream_type:
                    invalid_types.remove(type)
            else:
                if type == upstream_type:
                    invalid_types.remove(type)
                else:
                    # type not same as upstream type;
                    # check if the parent of type is same as the upstream type
                    type_record = get_record(dataset, COLUMN_MAP['name'], type)
                    check_type_hierarchy(dataset, type_record, type, upstream_type, invalid_types, config)

    if invalid_types:
        errors.append(f"[Row {record['row_number']+IDX_OFFSET}] composedOf '{name}' has type ({', '.join(invalid_types)}) which is not in the same scope when comparing with composedOf '{upstream_record[COLUMN_MAP['name']]}' with type ({upstream_composed_of_type}) in parent concept '{upstream_record[COLUMN_MAP['reference']]}'")
    return errors


def check_type_hierarchy(dataset, type_record, type, upstream_type, invalid_types, config = get_config()) -> None:
    """
    Recursively check if the parent of the given type is the same as the upstream type.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        type_record: Record corresponding to the type
        type: The starting type
        upstream_type: The upstream type
        invalid_types: A set of invalid types

    """
    if type_record:
        type_record_parent = type_record[COLUMN_MAP['parent']]
        if not type_record_parent.lower().endswith('concept'):
            type_record_parent_record = get_record_from_iri(dataset, type_record_parent, config)
            if type_record_parent_record:
                if type_record_parent_record[COLUMN_MAP['name']] == upstream_type:
                    invalid_types.remove(type)
                else:
                    check_type_hierarchy(dataset, type_record_parent_record, type, upstream_type, invalid_types, config)


def get_iri_from_record(record: Dict) -> str:
    if ' IRI' in record:
        iri = sanitize(record[' IRI'])
    else:
        iri = sanitize(record['IRI'])
    return iri


def get_namespace_iri(iri: str) -> str:
    if iri.endswith('#'):
        namespace_iri = iri[:-1]
    else:
        namespace_iri = iri
    return namespace_iri


def filter_record(record: Dict, keys: List) -> Dict:
    """
    Filter a record based on a given list of keys.

    Args:
        record: A dictionary corresponding to a record
        keys: A list of keys

    Returns:
        The filtered record

    """
    filtered_record = {}
    for key in keys:
        if key in record:
            filtered_record[key] = record[key]
    return filtered_record


def compare_record(record1: Dict, record2: Dict, keys: List) -> List:
    """
    Compare two records based on a given list of keys.

    Args:
        record1: A dictionary corresponding to a record
        record2: A dictionary corresponding to a record
        keys: A list of keys

    Returns:
        The list of keys that do not match between record1 and record2

    """
    mismatched_keys = []
    for key in keys:
        if record1[key] != record2[key]:
            mismatched_keys.append(key)
    return mismatched_keys


def get_record(dataset: pd.DataFrame, column: str, s: str) -> Dict:
    """
    From the dataset, get a record that matches a given string
    from a specified column.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        column: The column to look up
        s: The string to match

    Returns:
        A dictionary corresponding to the matched record

    """
    record = None
    filtered_data = dataset[dataset[column].str.strip() == s]
    if not filtered_data.empty:
        record = filtered_data.iloc[0].to_dict()
    return record


def get_record_from_iri(dataset: pd.DataFrame, name: str, config: Config = get_config()) -> Dict:
    """
    From the dataset, get a record that matches a given string
    from the IRI column.

    Args:
        dataset: A pandas DataFrame corresponding to the Dataset
        name: The name to look up
        config: The config

    Returns:
        A dictionary corresponding to the matched record

    """
    record = None
    if is_curie(name):
        fragment = name.split(":", 1)[1].strip()
    else:
        fragment = name
    if ' IRI' in dataset:
        filtered_data = dataset[dataset[' IRI'].str.endswith(f"#{fragment}")]
    else:
        filtered_data = dataset[dataset['IRI'].str.endswith(f"#{fragment}")]
    if not filtered_data.empty:
        record = filtered_data.iloc[0].to_dict()
    if not record:
        if name in config.properties:
            record = get_record_for_special_property(config.properties[name])

    return record



def get_record_for_special_property(property: Property) -> Dict:
    """
    Get record corresponding to special properties defined in the config
    but not in the Dataset itself.

    Args:
        property: A Property instance for the special property

    Returns:
        A record corresponding to the property

    """
    record = {
        'release': '2024.2',
        'unique ID': '0000000000',
        ' IRI': property.identifier,
        'active status (yes/no)': 'yes',
        'deprecated in': '',
        'replaced by': '',
        'concept reference': '',
        'concept or concept compositions or inherited': 'composedOf',
        'general concept name': property.label,
        'general description': property.description,
        'contextualized concept name': property.label,
        'contextualized description': property.description,
        'parent': property.parent.split('#')[1] if property.parent else '',
        'type': property.range[0].split('#')[1] if property.range else '',
        'excluded type descendants': ''
    }
    return record


def is_curie(s: str) -> bool:
    """
    Check if a given string is a CURIE.

    Args:
        s: A string

    Returns:
        A boolean

    """
    parts = s.split(":", 1)
    return len(parts) > 1

def format_class_name(class_name: str) -> str:
    """
    Format the name for a class/concept.

    Args:
        class_name: The class/concept name from the SPHN Dataset

    Returns:
        The formatted class name that is appropriate for RDF

    """
    if "(-)" in class_name:
        formatted_class_name = class_name.replace(" ", "")
    else:
        formatted_class_name = class_name.replace(" ", "").replace("-", "")
    return formatted_class_name


def format_property_name(property_name: str) -> str:
    """
    Format the name for a property.

    Args:
        property_name: The property name from the SPHN Dataset

    Returns:
        The formatted property name that is appropriate for RDF

    """
    formatted_property_name = "has" + sanitize(property_name).title().replace(" ", "").replace(
        "Datetime", "DateTime"
    )
    return formatted_property_name


def quotify(values: List[str]) -> List:
    """
    Given a list of values, quotify each value in the given list.

    Args:
        values: A list of values

    Returns:
        The quotified list of values

    """

    return [f"'{x}'" for x in values]


def sanitize(s: str) -> str:
    """
    Sanitize string by replacing unusual/unexpected characters.

    Args:
        s: The string to sanitize

    Returns:
        The sanitized string

    """
    # replace 'Zero width space' (0x200B)
    s = str(s).replace("​", " ")
    # replace 'Zero width space' (\xa0)
    s = s.replace(" ", " ")
    # replace newline chars with spaces
    s = s.replace("\n", " ")
    # replace
    s = s.replace("⁠", "")
    # replace leading and trailing spaces
    s = s.strip()
    return s