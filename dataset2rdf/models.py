from typing import List, Optional, Set, Dict
from pydantic import BaseModel, Field


class Property(BaseModel):
    """
    A Property class extracts information about a property
    from the SPHN Dataset.
    """

    identifier: str
    label: Optional[str] = None
    description: Optional[str] = None
    parent: Optional[str] = None
    domain: List = []
    range: List = []
    type: Optional[str] = None
    sensitive: Optional[bool] = False
    project_sensitive: Optional[bool] = False
    replaced_by: Optional[List[str]] = []


class PropertyPathRestriction(BaseModel):
    property_path: List = []
    some_values_from: Set = set()
    has_value: Set = set()


class Restriction(BaseModel):
    """
    A Restriction class that captures restrictions that are
    to be applied for a given class.
    """

    class_identifier: str
    property_identifier: str
    property_paths: Dict[str, PropertyPathRestriction] = {}
    min_cardinality: Optional[int] = None
    max_cardinality: Optional[int] = None
    some_values_from: Set = set()
    all_values_from: Set = set()
    has_value: Set = set()
    notes: Set = set()
    scope_notes: Set = set()


class Mapping(BaseModel):
    standard: str
    identifier: str
    predicate: Optional[str] = None


class Concept(BaseModel):
    """
    A Concept class extracts information about a concept
    from the SPHN Dataset.
    """

    identifier: str
    label: Optional[str] = None
    description: Optional[str] = None
    parent: Optional[str] = None
    mappings: List[Mapping] = []
    restrictions: Dict[str, Restriction] = {}
    project_expansion: bool = False
    sensitive: Optional[bool] = False
    project_sensitive: Optional[bool] = False
    replaced_by: Optional[List[str]] = []
    type_restrictions: Dict[str, Restriction] = {}


class Individual(BaseModel):
    """
    An Individual class that represents a NamedIndividual
    from the SPHN Dataset.
    """

    identifier: str
    label: Optional[str] = None
    description: Optional[str] = None
    type: List = []
