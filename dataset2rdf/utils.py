import datetime
from functools import lru_cache
import sys
from typing import Dict, Set, Tuple, List
import logging
import re
import copy
import stringcase
import pandas as pd
import numpy as np
from rdflib import SKOS, OWL, XSD

from dataset2rdf.validation_utils import CARDINALITIES_COLUMN_MAP, CODING_SYSTEM_INFO_COLUMN_MAP, COLUMN_MAP, IDX_OFFSET, check_column_names, check_composed_of_general_description, check_concept_extension, check_concept_general_description, check_concept_multiple_occurrence, check_inherited_project_concepts, check_project_concepts, format_class_name, format_property_name, get_project_start_row, get_record, get_record_from_iri, is_curie, sanitize, validate_property_path
from dataset2rdf.models import Concept, Mapping, Property, Individual, PropertyPathRestriction, Restriction
from dataset2rdf.config import Config, ImportConfig, ProjectConfig, get_config, logger


pd.set_option('mode.chained_assignment', None)

ONLY_DIRECT_SUBCLASSES = "only direct subclasses allowed"
NO_SUBCLASSES = "no subclasses allowed"

OBO_ONTOLOGIES = {"SO", "GENO"}

REGEX_MAP = {
    "code": r"(?:.+:)?\s?([\[\]{}()/\w\d.%_-]+)",
    "meaning_binding_standard": r"(^[A-Za-z0-9-\s]+):",
    "meaning_binding_code": r":[\s]*([A-Za-z0-9-_]+)",
    "extendable_valueset": r"extendable value set\s*:\s*([\w:]*[\w+\d+])",
    "descendant_of": r"descendant of\s*:\s*([\w:]*[\w+\d+])",
    "child_of": r"child of\s*:\s*([\w:]*[\w+\d+])",
    "for_standard": r"for\s*([\w\s\d-]*)\s*:"
}

REQUIRED_PREFIX_HEADERS = """@prefix : <{namespace}> .
"""
DEFAULT_PREFIX = "sphn"
MINIMAL_METADATA_FIELDS = {'prefix', 'canonical_iri', 'versioned_iri'}
METADATA_FIELDS = {'prefix', 'canonical_iri', 'versioned_iri', 'title', 'description', 'version', 'copyright'}
INTERNAL_DATATYPES = {'string', 'temporal', 'quantitative', 'anyURI'}


def load_metadata(filename: str, worksheet: str = "Metadata") -> pd.DataFrame:
    """
    Load the SPHN Dataset into a Pandas DataFrame.

    Args:
        filename: the excel file of the SPHN Dataset
        worksheet: The name of the sheet where the metadata is defined

    Returns:
        A Pandas DataFrame

    """
    df = pd.read_excel(filename, worksheet, keep_default_na=False)
    data = pd.DataFrame(df)
    data = data.applymap(sanitize)
    return data


def load_dataset(filename: str, worksheet: str = "Concepts", config: Config = get_config()) -> pd.DataFrame:
    """
    Load the SPHN Dataset into a Pandas DataFrame.

    Args:
        filename: the excel file of the SPHN Dataset
        worksheet: The name of the sheet were all the concepts are defined
        config: the config

    Returns:
        A Pandas DataFrame

    """
    df = pd.read_excel(filename, worksheet, keep_default_na=False)
    data = pd.DataFrame(df)
    data = data.applymap(sanitize)
    data['row_number'] = data.reset_index().index
    data = data.drop_duplicates()
    errors = []
    errors.extend(check_concept_general_description(data))
    errors.extend(check_composed_of_general_description(data))
    project_start_row = get_project_start_row(data)
    if project_start_row:
        core_dataset = data.iloc[0:project_start_row - 1]
        if not config.project_metadata:
            raise Exception(f"Project concepts detected from Row {project_start_row+IDX_OFFSET} onwards but project metadata is not defined.")
    else:
        core_dataset = data

    # find concepts that occur multiple times in SPHN
    errors.extend(check_concept_multiple_occurrence(core_dataset))
    # find SPHN concept extensions where repeated rows have changed
    errors.extend(check_concept_extension(data))
    # find extensions to SPHN concepts defined in project space
    # check project concepts
    if project_start_row:
        data_test = data.loc[(data[COLUMN_MAP["active_status"]].str.lower() == "yes"), :]
        data_ps = data.loc[((data[COLUMN_MAP["active_status"]].str.lower() == "yes") & (data["row_number"] >= project_start_row )), :]
        errors.extend(check_project_concepts(data_test, data_ps, project_start_row, config))
        # check if project concept inherits an SPHN concept and yet does not list all the inherited composedOfs
        errors.extend(check_inherited_project_concepts(data_test, data_ps, config))
    
    # find missing columns
    errors.extend(check_column_names(data, COLUMN_MAP, config))

    if errors:
        logger.error(f"{len(errors)} errors found while parsing {filename}:\n - " + '\n - '.join(sorted(errors)))
        raise ValueError(errors)

    return data


def load_cardinalities(filename: str, worksheet: str = "Cardinalities", config: Config = get_config()) -> pd.DataFrame:
    """
    Load cardinalities from the SPHN Dataset into a Pandas DataFrame.

    Args:
        filename: the excel file of the SPHN Dataset
        worksheet: The name of the sheet were all the cardinalities are defined
        config: the config

    Returns:
        A Pandas DataFrame

    """
    errors = []
    df = pd.read_excel(filename, worksheet, keep_default_na=False)
    data = pd.DataFrame(df)
    data = data.applymap(sanitize)
    data = data.drop_duplicates()

    data_columns = [
        "concept or concept compositions or inherited",
        "active status (yes/no)",
        "concept reference",
        "general concept name",
        "cardinality for composedOf",
        "cardinality for concept to Administrative Case",
        "cardinality for concept to Subject Pseudo Identifier",
    ]

    if config.version.startswith("2023"):
        data_columns.append("cardinality for concept to Data Provider Institute")

    if config.version.startswith("2024"):
        data_columns.append("cardinality for concept to Data Provider")
        data_columns.append("cardinality for concept to Source System")

    if config.version.startswith("2025"):
        data_columns.append("cardinality for concept to Source System")

    errors.extend(check_column_names(data, CARDINALITIES_COLUMN_MAP, config))
    if errors:
        raise ValueError(errors)

    valid_columns = [col for col in data_columns if col in data.columns]
    data = data.loc[:,valid_columns]
    return data


def load_coding_system_information(filename: str, worksheet: str = "Coding System and Version") -> pd.DataFrame:
    """
    Load information about coding system from the SPHN Dataset into a Pandas DataFrame.

    Args:
        filename: the excel file of the SPHN Dataset
        worksheet: The name of the sheet were the information about coding systems are defined

    Returns:
        A Pandas DataFrame

    """
    df = pd.read_excel(filename, worksheet, keep_default_na=False)
    data = pd.DataFrame(df)
    data = data.applymap(sanitize)
    return data


def parse_metadata(metadata: pd.DataFrame) -> Dict:
    metadata_obj = {}
    for row in metadata.to_dict('records'):
        metadata_obj[row['prefix']] = row
    return metadata_obj


def add_metadata_to_config(default_prefix: str, metadata: Dict, config: Config = get_config()) -> None:
    if not default_prefix:
        default_prefix = DEFAULT_PREFIX
    core_metadata = metadata[default_prefix]
    if any(x not in core_metadata for x in METADATA_FIELDS):
        raise KeyError(f"The following fields are mandatory for {DEFAULT_PREFIX} in the Metadata sheet: {METADATA_FIELDS}")

    config.prefix = sanitize(core_metadata['prefix'])
    canonical_iri = sanitize(core_metadata['canonical_iri'])
    if not canonical_iri.endswith("#"):
        raise ValueError(f"canonical_iri for {config.prefix} must end with '#'")
    config.iri.canonical_iri = canonical_iri
    config.iri.versioned_iri = sanitize(core_metadata['versioned_iri'])
    config.title = sanitize(core_metadata['title'])
    config.description = sanitize(core_metadata['description'])
    config.version = str(sanitize(core_metadata['version']))
    config.prior_version = str(sanitize(core_metadata['prior version']))
    config.copyright = sanitize(core_metadata['copyright'])
    config.license = sanitize(core_metadata['license'])
    return config


def add_project_metadata_to_config(metadata: Dict, config: Config = get_config()) -> None:
    """
    Parse metadata and add project-specific metadata to the config.

    Args:
        metadata: a dictionary containing the metadata
        config: the config

    """
    metadata_values = list(metadata.values())
    if metadata_values:
        project_metadata = metadata_values[0]
        if any(x not in project_metadata for x in MINIMAL_METADATA_FIELDS):
            raise KeyError(f"The following fields are mandatory for a project in the Metadata sheet: {MINIMAL_METADATA_FIELDS}")

        for k in project_metadata.keys():
            project_metadata[k] = sanitize(project_metadata[k])

        canonical_iri = sanitize(project_metadata['canonical_iri'])
        versioned_iri = sanitize(project_metadata["versioned_iri"])
        if not canonical_iri.endswith("/") and not canonical_iri.endswith("#"):
            raise ValueError(f"canonical_iri for {project_metadata['prefix']} must end with either a '/' or a '#'")

        project_metadata["iri"] = {
            "canonical_iri": canonical_iri,
            "versioned_iri": versioned_iri
        }
        del project_metadata["canonical_iri"]
        del project_metadata["versioned_iri"]

        project_metadata["concepts"] = {}
        project_root_concept_name = project_metadata['prefix'].upper() + 'Concept'
        project_root_concept = Concept(
            identifier=project_metadata['iri']['canonical_iri'] + project_root_concept_name,
            label=f"{project_metadata['prefix'].upper()} Concept",
            description=f"{project_metadata['prefix'].upper()} Concept defined by the {project_metadata['prefix']} project",
            parent=config.iri.canonical_iri + config.root_node[0]
        )
        project_metadata["concepts"][project_metadata['prefix'].upper() + 'Concept'] = project_root_concept
        project_metadata["root_node"] = [project_metadata['prefix'].upper() + 'Concept']
        project_metadata["create_root_node"] = True
        config.project_metadata = ProjectConfig(**project_metadata)


def parse_dataset(
    filename: str, dataset: pd.DataFrame, config: Config = get_config()
) -> Tuple[Dict, Dict, Dict]:
    """
    Get the list of concepts and properties from the SPHN Dataset.

    Args:
        filename: the Dataset filename
        dataset: a DataFrame containing the content of the SPHN Dataset
        config: the config

    Returns:
        A 3 tuple of dictionaries that represents concepts, properties and individuals
        extracted from the Dataset.

    """
    concepts: Dict = {}
    properties: Dict = {}
    individuals: Dict = {}
    # Concepts not in Dataset
    for sphn_concept in config.concepts.values():
        concepts[sphn_concept.identifier] = sphn_concept
    if config.project_metadata and config.project_metadata.concepts:
        for project_concept in config.project_metadata.concepts.values():
            concepts[project_concept.identifier] = project_concept
    # if config.project_metadata.root_node and config.project_metadata.create_root_node: # more in tune with the class definition but less in tune with coding

    # Properties not in Dataset
    for sphn_property in config.properties.values():
        properties[sphn_property.identifier] = sphn_property
    # add project properties if they exist
    if config.project_metadata:
        for project_property in config.project_metadata.properties.values():
            properties[project_property.identifier] = project_property
    # find invalid values in active_status column
    dataset['row_number'] = dataset.reset_index().index
    for index, row in dataset.iterrows():
        record = row.to_dict()
        value = sanitize(record[COLUMN_MAP["active_status"]].lower())
        concept_ref = sanitize(record[COLUMN_MAP["reference"]])
        concept_name = sanitize(record[COLUMN_MAP["name"]])
        parent_name = sanitize(record[COLUMN_MAP["parent"]])
        if value not in ["", "no", "yes"]:
            raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value in column "active status (yes/no)". Should be "yes" or "no", found: "{value}" for {concept_ref}.{concept_name}')

    parse_active_concepts(
        filename=filename,
        dataset=dataset,
        concepts=concepts,
        properties=properties,
        individuals=individuals,
        config=config
    )
    parse_inactive_concepts(
        filename=filename, dataset=dataset, concepts=concepts, properties=properties, config=config
    )
    set_property_range(dataset=dataset, concepts=concepts, properties=properties, config=config)
    return (concepts, properties, individuals)


def parse_active_concepts(
    filename: str,
    dataset: pd.DataFrame,
    concepts: Dict,
    properties: Dict,
    individuals: Dict,
    config: Config = get_config(),
) -> Tuple[Dict, Dict, Dict]:
    """
    Parse active concepts and properties from the Dataset.

    Args:
        filename: the Dataset filename
        dataset: a DataFrame containing the content of the SPHN Dataset
        concepts: a dictionary of concepts
        properties: a dictionary of properties
        individuals: a dictionary of individuals
        config: the config

    Returns:
        A 3 tuple of dictionaries that represents concepts, properties and individuals
        extracted from the Dataset.

    """
    concept_list = []
    for row in dataset.to_dict('records'):
        if sanitize(row[COLUMN_MAP["active_status"]].lower()) == "yes":
            concept_name = sanitize(row[COLUMN_MAP["name"]]).replace(" ","")
            if is_curie(concept_name): 
                concept_prefix, concept_name = [x.strip() for x in concept_name.split(":", 1)]
            concept_list.append(concept_name)

    for index, row in dataset.iterrows():
        record = row.to_dict()
        if sanitize(row[COLUMN_MAP["active_status"]].lower()) == "yes":
            category = sanitize(record[COLUMN_MAP["category"]])
            concept_ref = sanitize(record[COLUMN_MAP["reference"]])
            concept_name = sanitize(record[COLUMN_MAP["name"]])
            parent_name = sanitize(record[COLUMN_MAP["parent"]])
            description = sanitize(record[COLUMN_MAP["description"]])
            # check for missing values and invalid values
            if concept_ref == "" and concept_name == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in the columns "general concept name" and "concept reference". Please check for rows with missing values.')
            elif concept_name == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in column "general concept name" with concept reference "{concept_ref}". Please enter a concept name.')
            elif concept_ref == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in column "concept reference" for concept "{concept_name}". Please enter a concept.')
            elif parent_name == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in column "parent" for concept "{concept_name}" with concept reference "{concept_ref}". Please enter a parent concept.')
            elif description == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in column "general description" for concept "{concept_name}" with concept reference "{concept_ref}". Please enter a description.')
            elif category == "":
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] No value found in column "concept or concept compositions or inherited" for "{concept_ref}.{concept_name}". Should be "inherited", "concept" or "composedOf".')
            elif category not in ["concept", "composedOf", "inherited"]:
                raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value in column "concept or concept compositions or inherited" for "{concept_ref}.{concept_name}". Should be "inherited", "concept" or "composedOf", found: "{category}"')
            else:
                if is_curie(concept_ref):
                    concept_ref_prefix, concept_ref = [x.strip() for x in concept_ref.split(":", 1)]
                if not concept_ref == ' '.join([w.title() if w.islower() else w for w in concept_ref.split()]):
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Formatting error found in column "concept reference" for concept "{concept_ref}". Please use title case.')
                elif concept_ref.replace(" ", "") not in concept_list:
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "concept reference": {concept_ref}. Concept is not defined.')

                # if not is_curie(concept_name) and is_curie(parent_name):
                #     raise ValueError(f'Logic error found in columns "general concept name" and "parent" for concept "{concept_name}". SPHN concepts, composedOfs and inherited must have a SPHN parent.')

            # parse data
            if category == "concept":
                # check for invalid values
                if not concept_name.split(":")[-1] == concept_ref.split(":")[-1]:
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Logic error found in columns "general concept name" and "concept reference" for concept "{concept_name}". Concept name and concept reference must be the same.')
                if is_curie(concept_name):
                    concept_prefix, concept_name = [x.strip() for x in concept_name.split(":", 1)]
                if not concept_name == ' '.join([w.title() if w.islower() else w for w in concept_name.split()]):
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Formatting error found in column "general concept name" for concept "{concept_name}". Please use title case.')
                if " " in parent_name:
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Formatting error found in column "parent": {parent_name}. Please remove space(s).')
                elif is_curie(parent_name):
                    parent_prefix, parent_name = [x.strip() for x in parent_name.split(":", 1)]
                    if config.project_metadata:
                        if not parent_prefix.lower() == config.project_metadata.prefix.lower():
                            raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid prefix found in column "parent" for concept {concept_name}. Found "{parent_prefix}", but prefix has to be "sphn" or "{config.project_metadata.prefix}".')
                    if parent_name not in concept_list and parent_name not in [parent_prefix.upper() + "Concept"]:
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent": {parent_name}. Concept is not defined.')
                elif parent_name not in concept_list and parent_name not in config.concepts.keys():
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent": {parent_name}. Concept is not defined.')
                
                parse_active_concept(filename=filename, dataset=dataset, record=row, concepts=concepts, config=config)

            elif (
                category == "composedOf" or category == "inherited"
            ):
                # check for invalid values
                if is_curie(concept_name):
                    concept_prefix, concept_name = [x.strip() for x in concept_name.split(":", 1)]
                if not concept_name == ' '.join([w.lower() if len(w) > 1 else w for w in concept_name.split()]):
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Formatting error found in column "general concept name" for composedOf "{concept_name}". Please use lower case.')
                if is_curie(concept_ref) and not is_curie(concept_name):
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Logic error found in columns "general concept name" and "concept reference" for composedOf "{concept_name}". SPHN composedOfs and inherited must have a SPHN concept reference.')
                
                if " " in parent_name:
                    raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Formatting error found in column "parent": {parent_name}. Please remove space(s).')
                elif is_curie(parent_name):
                    parent_prefix, parent_name = [x.strip() for x in parent_name.split(":", 1)]
                    if config.project_metadata:
                        if not parent_prefix.lower() == config.project_metadata.prefix.lower():
                            raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid prefix found in column "parent" for composedOf {concept_name}. Found "{parent_prefix}", but prefix has to be "sphn" or "{config.project_metadata.prefix}".')
                    if not (parent_name.startswith("has") or parent_name in [parent_prefix.upper() + "AttributeObject", parent_prefix.upper() + "AttributeDatatype"]):
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent" for composedOf "{concept_name}". Found "{parent_name}", but value has to start with "has" or "{parent_prefix}".')
                    elif parent_name.split("has")[-1].lower() not in concept_list and parent_name not in [parent_prefix.upper() + "AttributeObject", parent_prefix.upper() + "AttributeDatatype"]:
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent": {parent_name}. Concept is not defined.')
                else:
                    if not (parent_name.startswith("has") or parent_name in ["SPHNAttributeObject", "SPHNAttributeDatatype"]):
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent" for composedOf "{concept_name}". Found "{parent_name}", but value has to be SPHNAttributeObject, SPHNAttributeDatatype or start with "has".')
                    elif parent_name.split("has")[-1].lower() not in concept_list and parent_name not in config.properties:
                        raise ValueError(f'[Row {record["row_number"]+IDX_OFFSET}] Invalid value found in column "parent": {parent_name}. composedOf is not defined.')

                parse_active_composed_ofs(
                    dataset=dataset,
                    filename=filename,
                    record=row,
                    concepts=concepts,
                    properties=properties,
                    individuals=individuals,
                    config=config
                )
    return (concepts, properties, individuals)


def parse_inactive_concepts(
    filename, dataset: pd.DataFrame, concepts: Dict, properties: Dict, config: Config = get_config()
) -> Tuple[Dict, Dict]:
    """
    Parse inactive concepts from the Dataset.

    Args:
        filename: the Dataset filename
        dataset: a DataFrame containing the content of the SPHN Dataset
        concepts: a dictionary of concepts
        properties: a dictionary of properties
        config: the config

    Returns:
        A 2 tuple of dictionaries that represents concepts and properties
        extracted from the Dataset.

    """
    for row in dataset.to_dict('records'):
        # inactive concept
        if sanitize(row[COLUMN_MAP["active_status"]].lower()) == "no":
            category = sanitize(row[COLUMN_MAP["category"]])
            if category == "concept":
                if ":" in sanitize(row[COLUMN_MAP["name"]]):
                    prefix = sanitize(row[COLUMN_MAP["name"]]).split(":")[0].lower()
                    if prefix not in config.project_metadata:
                        logger.info(f"Ignoring inactive concept '{row[COLUMN_MAP['name']]}'")
                        continue
                else:
                    prefix = "sphn"
                iri_header = prefixer(prefix, config)
                deprecated_in = sanitize(row[COLUMN_MAP["deprecated_in"]])
                if deprecated_in:
                    if config.deprecated_iri:
                        iri_header = config.deprecated_iri
                    identifier = format_class_name(sanitize(row[COLUMN_MAP["name"]]))
                    logger.debug(f"Concept {identifier} deprecated in {deprecated_in}")
                    if iri_header+identifier in concepts:
                        key = f"{iri_header+identifier}_Deprecated_{deprecated_in}"
                    else:
                        key = iri_header+identifier
                    concept = Concept(identifier=iri_header+identifier)
                    concept.label = sanitize(row[COLUMN_MAP["name"]].strip())
                    concept.description = concept.label
                    concept.parent = config.iri.canonical_iri+"Deprecated"
                    concepts[key] = concept

                replaced_by = sanitize(row[COLUMN_MAP["replaced_by"]])
                if replaced_by:
                    identifier = format_class_name(sanitize(row[COLUMN_MAP["name"]]))
                    release = sanitize(row[COLUMN_MAP["release"]])
                    if release == config.version:
                        concept_iri = config.iri.canonical_iri
                    else:
                        if config.deprecated_iri:
                            concept_iri = config.deprecated_iri
                        else:
                            concept_iri = config.iri.canonical_iri
                    if ";" in replaced_by:
                        replaced_by_concepts = [sanitize(x) for x in replaced_by.split(";")]
                    else:
                        replaced_by_concepts = [sanitize(x) for x in replaced_by.split(",")]
                    key = concept_iri + identifier
                    if key in concepts:
                        concept = concepts[key]
                    else:
                        concept = Concept(identifier=key)
                        concept.label = sanitize(row[COLUMN_MAP["name"]]).strip()
                        concept.description = concept.label
                        concept.parent = iri_header+"Deprecated"
                        concepts[key] = concept
                    for replaced_by_concept in replaced_by_concepts:
                        concept_key = config.iri.canonical_iri + format_class_name(replaced_by_concept)
                        if concept_key not in concepts:
                            raise KeyError(f"Cannot find {concept_key} in concepts")
                        concept.replaced_by.append(concept_key)
    return (concepts, properties)


def prefixer(prefix: str, config: Config = get_config()) -> str:
    """
    Return an IRI for a given prefix.

    Args:
        prefix: the prefix
        config: the config

    Returns:
        The IRI corresponding to the given prefix

    """
    iri_header = ""
    prefix = prefix.strip()
    if prefix.lower() == config.prefix:
        iri_header = config.iri.canonical_iri
    elif config.project_metadata:
        if prefix.lower() == config.project_metadata.prefix.lower() :
            iri_header = config.project_metadata.iri.canonical_iri
        else:
            raise ValueError(f"PREFIX : " + str(prefix) +" not found in sphn or in project metadata. looking for: " + str(prefix) + " but only have sphn and " + str(config.project_metadata.prefix.lower()))
    else:
        raise ValueError(f"PREFIX : " + str(prefix) +" not found in sphn or in project metadata. looking for: " + str(prefix) + " but only have sphn")
    return iri_header


def get_iri_for_named_individual(iri: str) -> str:
    """
    Given an IRI, prepare the base IRI for named individual.

    The incoming IRI is typically the canonical IRI.

    Args:
        iri: str

    Returns:
        The IRI for named individual

    """
    if str(iri).endswith('#'):
        named_individual_iri = re.sub(r'#$', '/individual#', iri)
    elif str(iri).endswith('/'):
        named_individual_iri = re.sub(r'/$', '/individual/', iri)
    else:
        named_individual_iri = f"{iri}/individual/"
    return named_individual_iri


def parse_active_concept(filename: str, dataset: pd.DataFrame, record: Dict, concepts: Dict, config: Config = get_config()) -> Concept:
    """
    Parse an active concept from a record.

    Args:
        filename: the Dataset filename
        record: a record that represents the concept
        concepts: a dictionary of concepts
        config: the config

    Returns:
        An instance of ``dataset2rdf.models.Concept``

    """
    if ":" in sanitize(record[COLUMN_MAP["name"]]):
        concept_name = sanitize(record[COLUMN_MAP["name"]]).split(":")[1]
        prefix = sanitize(record[COLUMN_MAP["name"]]).split(":")[0].lower()
    else:
        concept_name = sanitize(record[COLUMN_MAP["name"]])
        prefix = "sphn"
    iri_header = prefixer(prefix, config)
    identifier = format_class_name(sanitize(concept_name))
    if iri_header+identifier in concepts:
        concept = concepts[iri_header+identifier]
    else:
        concept = Concept(identifier=iri_header+identifier)
    concept.label = concept_name
    if sanitize(record[COLUMN_MAP["description"]]):
        concept.description = sanitize(record[COLUMN_MAP["description"]])
    if sanitize(COLUMN_MAP["meaning_binding"]) in record:
        mappings = sanitize(record[COLUMN_MAP["meaning_binding"]])
        if mappings:
            add_concept_mappings(concept, mappings.split(";"))
            if ((";" not in mappings) and (mappings.count(":") > 1)):
                raise ValueError(f'Formatting error found in meaning binding "{mappings}" for {prefix}:{concept_name}. PLease check, if semicolon is used in case of more than one specified code.')
    parent_concept_name = sanitize(record[COLUMN_MAP["parent"]])
    if is_curie(parent_concept_name):
        parent_prefix, parent_concept_name = [x.strip() for x in parent_concept_name.split(":", 1)]
    else:
        parent_prefix = config.prefix
    parent_iri_header = prefixer(parent_prefix, config)
    if parent_concept_name:
        if parent_iri_header + parent_concept_name in concepts:
            parent_concept_identifier = parent_concept_name
            concept.parent = parent_iri_header+parent_concept_identifier
        elif parent_concept_name.lower().startswith(prefix) and prefix.lower() != config.prefix:
            # this is the case for a project specific addition which has a root node not present in the config.
            createConceptInModel(parent_concept_name, prefix, concepts, config)
            parent_concept_identifier = parent_concept_name
            concept.parent = config.project_metadata.iri.canonical_iri + parent_concept_identifier
        else:
            parent_concept_identifier = format_class_name(parent_concept_name)
            concept.parent = parent_iri_header+parent_concept_identifier
        if concept.identifier == concept.parent:
            raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] {concept_name} is referring to itself as its parent")
    if COLUMN_MAP["sensitive"] in record:
        if record[COLUMN_MAP["sensitive"]] and sanitize(record[COLUMN_MAP["sensitive"]]).lower() == "yes":
            if prefix == config.prefix:
                concept.sensitive = True
            else:
                concept.project_sensitive = True
    concepts[iri_header+identifier] = concept
    return concept


def createConceptInModel(name: str, prefix: str, concepts: Dict, config: Config = get_config()) -> None:
    """
    Create a reserved concept.

    Args:
        name: the concept name
        prefix: the concept prefix
        concepts: a dictionary of concepts
        config: the config

    """
    canonical_iri = config.project_metadata.iri.canonical_iri
    label = " ".join(name.split(prefix))
    description = f"{label} defined by the {config.project_metadata.prefix} project"
    element = Concept(identifier=canonical_iri+name, label=label, description=description)
    if name.endswith("Concept"):
        element.parent = concepts[config.iri.canonical_iri + "SPHNConcept"].identifier
    concepts[name] = element


def createPropertyInModel(name: str, prefix: str, properties: Dict, config: Config = get_config()) -> None:
    """
    Create a reserved property.

    Args:
        name: the concept name
        prefix: the concept prefix
        properties: a dictionary of properties
        config: the config

    """
    canonical_iri = config.project_metadata.iri.canonical_iri
    label = " ".join(name.split(prefix))
    description = f"{label} defined by the {config.project_metadata.prefix} project"
    element = Property(identifier=canonical_iri+name, label=label, description=description)
    if name.endswith("AttributeDatatype"):
        element.parent = properties[config.iri.canonical_iri + "SPHNAttributeDatatype"].identifier
        element.type = "dataProperty"
    elif name.endswith("AttributeObject"):
        element.parent = properties[config.iri.canonical_iri + "SPHNAttributeObject"].identifier
        element.type = "objectProperty"
    properties[name] = element

def parse_active_composed_ofs(
    filename: str,
    dataset: pd.DataFrame,
    record: Dict,
    concepts: Dict,
    properties: Dict,
    individuals: Dict,
    config: Config = get_config()
) -> Property:
    """
    Parse an active composedOf from a record.

    Args:
        filename: the Dataset filename
        record: a record that represents the composedOf
        concepts: a dictionary of concepts
        properties: a dictionary of properties
        individuals: a dictionary of individuals
        config: the config

    Returns:
        An instance of ``dataset2rdf.models.Property``

    """
    if ":" in sanitize(record[COLUMN_MAP["name"]]):
        property_name = sanitize(record[COLUMN_MAP["name"]]).split(":")[1]
        prefix = sanitize(record[COLUMN_MAP["name"]]).split(":")[0].lower()
    else:
        property_name = sanitize(record[COLUMN_MAP["name"]])
        prefix = "sphn"
    if ":" in sanitize(record[COLUMN_MAP["reference"]]):
        reference_prefix = sanitize(record[COLUMN_MAP["reference"]]).split(":")[0].lower()
    else:
        reference_prefix = "sphn"

    property_identifier = format_property_name(property_name)
    prop = None

    imports = copy.deepcopy(config.imports)
    if prefix != config.prefix or reference_prefix != config.prefix:
        imports.update(config.project_metadata.imports)

    iri_header = prefixer(prefix, config)

    prop_type_restrictions = set()
    if iri_header+property_identifier in properties:
        prop = properties[iri_header+property_identifier]
    else:
        prop = Property(identifier=iri_header+property_identifier)
        prop.label = f"has {property_name}"
        properties[iri_header+property_identifier] = prop

    if sanitize(record[COLUMN_MAP["description"]]):
        prop.description = sanitize(record[COLUMN_MAP["description"]])
    prop_types = [sanitize(x) for x in record[COLUMN_MAP["type"]].split(";")]
    for prop_type in prop_types:
        if is_curie(prop_type):
            prop_type_prefix, prop_type = [x.strip() for x in prop_type.split(":", 1)]
        else:
            prop_type_prefix = config.prefix

        if property_identifier in {'hasCodingSystemAndVersion', 'hasName', 'hasIdentifier'}:
            prop.type = 'dataProperty'
        else:
            if prop_type in INTERNAL_DATATYPES:
                prop.type = 'dataProperty'
            else:
                prop.type = 'objectProperty'
                prop_type_restrictions.add(f"{prop_type_prefix}:{format_class_name(prop_type)}")

    parent = sanitize(record[COLUMN_MAP["parent"]])
    if is_curie(parent):
        parent_prefix, parent_identifier = [x.strip() for x in parent.split(":", 1)]
    else:
        parent_prefix = config.prefix
        parent_identifier = parent
    parent_iri_header = prefixer(parent_prefix, config)
    prop.parent = parent_iri_header+parent_identifier
    if prop.identifier == prop.parent:
        raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] {property_name} is referring to itself as its parent")
    if parent.lower().startswith(parent_prefix.lower()) and parent_prefix != config.prefix and len(parent_prefix) > 0:
        if parent.endswith("AttributeObject") or parent.endswith("AttributeDatatype"):
            createPropertyInModel(parent_identifier, parent_iri_header, properties, config)

    # Add domain to existing property
    concept_reference = sanitize(record[COLUMN_MAP["reference"]])
    concept_identifier = format_class_name(concept_reference)
    # parse prefix of domain:
    if is_curie(concept_identifier):
        domain_prefix, concept_identifier = [x.strip() for x in concept_identifier.split(":", 1)]
    else:
        domain_prefix = config.prefix
    domain_iri_header = prefixer(domain_prefix, config)
    if domain_iri_header+concept_identifier in concepts:
        concept = concepts[domain_iri_header+concept_identifier]
    else:
        concept = Concept(identifier=domain_iri_header+concept_identifier)
        concepts[domain_iri_header+concept_identifier] = concept
    if concept not in prop.domain:
        prop.domain.append(concept)

    for prop_type in prop_type_restrictions:
        if is_curie(prop_type):
            prop_type_prefix, prop_type = [x.strip() for x in prop_type.split(":", 1)]
        else:
            prop_type_prefix = config.prefix
        prop_type_iri_header = prefixer(prop_type_prefix, config)
        r_key = f"{concept.identifier}-{prop.identifier}"
        if r_key in concept.type_restrictions:
            type_res = concept.type_restrictions[r_key]
        else:
            type_res = Restriction(
                class_identifier=concept.identifier,
                property_identifier=prop.identifier
            )
            concept.type_restrictions[r_key] = type_res
        type_res.some_values_from.add(prop_type_iri_header+prop_type)

    # Parse standard and valueset
    standard = sanitize(record[COLUMN_MAP["standard"]])
    standard = standard.replace(";",",")
    valueset = sanitize(record[COLUMN_MAP["valueset"]])
    object_type = sanitize(record[COLUMN_MAP["type"]])
    add_info = sanitize(record[COLUMN_MAP["additional_information"]])
    if standard:
        # standard defined
        if valueset:
            # standard defined and valueset defined
            if "link to value set:" in valueset:
                match = re.findall(r":[\s]*'([A-Za-z0-9]+)'", valueset)
                if not match:
                    raise Exception(f"Cannot find a reference to a sheet for valueset: {valueset}")
                sheet_name = match[0]
                codes = parse_code_from_sheet(
                    filename=filename,
                    sheet_name=sheet_name,
                    standard=standard,
                    imports=imports,
                    config=config
                )
                add_code_as_restriction(
                    concept=concept, prop=prop, codes=codes, config=config
                )
            elif 'restricted to' in valueset:
                #valueset = valueset.replace(",", ";")
                if 'restricted to :' in valueset:
                    valueset = valueset.replace('restricted to :', 'restricted to:')
                restricted_to_expressions = parse_restricted_to_expression(valueset)
                for expression in restricted_to_expressions:
                    path_str, elements_str = expression.split("restricted to:")
                    property_path_list = path_str.strip().split("->")
                    if dataset is not None:
                        valid_property_path = validate_property_path(dataset, record, property_path_list.copy())
                        if not valid_property_path:
                            raise ValueError(f"[Row {record['row_number'] + IDX_OFFSET}] {concept_reference}.{property_name} has an invalid property path '{path_str}'")
                    property_path = []
                    for p in property_path_list:
                        if is_curie(p):
                            p_prefix, p_identifier = p.split(":")
                            p_identifier = format_property_name(p_identifier)
                            p_iri = f"{prefixer(p_prefix, config)}{p_identifier}"
                            property_path.append(p_iri)
                        else:
                            p_prefix = config.prefix
                            p_identifier = p
                            p_identifier = format_property_name(p_identifier)
                            p_iri = f"{prefixer(p_prefix, config)}{p_identifier}"
                            property_path.append(p_iri)
                    elements = [x.strip() for x in elements_str.split(";")]
                    parsed_elements = []
                    for_standard = None
                    scope = None
                    for element in elements:
                        if standard in {'UCUM'}:
                            element = parse_element(element)
                        if element.startswith("for "):
                            try:
                                for_standard = extract_code(element, REGEX_MAP["for_standard"])
                            except ValueError as ve:
                                logger.error(f"[Row {record['row_number']}] Could not extract code from '{valueset}'. This is usually due to the codes values in the {COLUMN_MAP['valueset']} not conforming to the guidelines.")
                                raise ve
                        if "descendant of:" in element:
                            scope = "descendant of: "
                        elif "child of:" in element:
                            scope = "child of: "
                        if for_standard and for_standard not in element:
                            element = f"for {for_standard}: {scope if scope and scope not in element else ''}{element}"
                            parsed_elements.append(element)
                        else:
                            element = f"{scope if scope and scope not in element else ''}{element}"
                            parsed_elements.append(element)

                    parse_standard_as_restriction(
                        concept=concept,
                        prop=prop,
                        standard=standard,
                        valueset=";".join(parsed_elements),
                        property_path=property_path,
                        prefix=prefix.lower(),
                        add_info=add_info,
                        config=config
                    )

            elif valueset.lower().startswith("unit:"):
                # UCUM code restriction on prop -> hasUnit -> hasCode
                standard_iri = imports["UCUM"].iri.canonical_iri
                valueset = valueset.replace(",",";")
                elements = [
                    parse_element(x.strip())
                    for x in valueset.split(":")[1].split(";")
                ]
                codes = [f"{standard_iri}{x}" for x in elements]
                add_code_as_restriction(
                    concept=concept,
                    prop=prop,
                    prop_path=[iri_header+"hasUnit", iri_header+"hasCode"],
                    codes=codes,
                    config=config,
                )
            # Check that if the type is set to a concept which is note Code or Terminology, then it is intended to add it to the "hasCode" property of stated concept in "type" e.g. BodySite or MeasurementMethod for the BloodPressure
            elif object_type and object_type != "Code":
                logger.debug("for: " + concept_identifier + " on the property: " + property_name + " found a objecttype : " + format_class_name(object_type) + " but also there we have a valueset : " + str(valueset))
                # we need to have preferred properties that we need to find: "hasCode" "hasTypeCode" "hasMethodCode"  (hasTypeCode is for the MedicalDevice; hasMethodCode is for the DataDetermination)
                # then we also need to add these as property paths

                # if object_type is DataDetermination then we do use hasMethodCode
                # if object_type is MedicalDevice then we do use hasTypeCode
                # else we use hasCode
                secondary_property = "hasCode"
                if format_class_name(object_type) == "DataDetermination" : secondary_property = "hasMethodCode"
                elif format_class_name(object_type) == "MedicalDevice" : secondary_property = "hasTypeCode"

                if is_curie(object_type):
                    typeprefix = object_type.split(":", 1)[0].strip()
                else:
                    typeprefix = config.prefix
                type_iri_header = prefixer(typeprefix, config)

                parse_standard_as_restriction(
                    concept=concept,
                    prop=prop,
                    property_path=[type_iri_header+secondary_property],
                    standard=standard,
                    valueset=valueset,
                    prefix=parent_prefix.lower(),
                    add_info=add_info,
                    config=config,
                )

            else:
                # valueset is one (or more) codes from the given standard
                parse_standard_as_restriction(
                    concept=concept,
                    prop=prop,
                    property_path=None,
                    standard=standard,
                    valueset=valueset,
                    prefix=parent_prefix.lower(),
                    add_info=add_info,
                    config=config,
                )
        else:
            # standard defined but no valueset defined; i.e. bind to root node of standard
            # if standard is defined in config and standard is single-valued. else skos:note
            original_standard_list = [x.strip() for x in standard.split(",")]
            # check if we find the "or other" as a element in the split, if so we add a Node, otherwise we continue
            if standard.lower().endswith("or other"):
                new_last_element = original_standard_list[-1][:-len("or other")].strip()
                if len(original_standard_list) == 1:
                    valid_standard_list = [new_last_element]
                else:
                    valid_standard_list = original_standard_list[:-1]+[new_last_element]

            else:
                valid_standard_list = original_standard_list
            note = None
            Code = False
            if not standard.lower().endswith("or other"):
                for stand in valid_standard_list:
                    stand = stand.upper()
                    # add Code restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                    if not stand in imports and not stand == "Terminology":
                        if not "Terminology" in valid_standard_list:
                            code_iri = config.iri.canonical_iri + "Code"
                            add_code_as_restriction(
                                concept=concept,
                                prop=prop,
                                codes=[code_iri],
                                config=config
                            )
                        else:
                            Code = True
                for stand in valid_standard_list:
                    # add other restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                    stand = stand.upper()
                    if (stand in imports and
                        not ("Terminology" in valid_standard_list and Code)
                        ):
                        if imports[stand].create_root_node:
                            if f'SPHN-{stand}' in imports:
                                standard_concept = f"{imports[f'SPHN-{stand}'].iri.canonical_iri}{stand}"
                            else:
                                root_node = imports[stand].root_node[0]
                                if root_node.startswith('http'):
                                    standard_concept = root_node
                                else:
                                    if f"{imports[stand].prefix}:".lower() in root_node.lower():
                                        root_node = root_node.split(":", 1)[1]
                                    if len(imports[stand].root_node) > 1:
                                        logger.warning(f"Terminology {stand} has more than one root node but only considering the first: {stand} -> {imports[stand].root_node}")
                                    standard_concept = f"{imports[stand].iri.canonical_iri}{root_node}"
                        else:
                            standard_iri = imports[f"SPHN-{stand}"].iri.canonical_iri
                            root_node = imports[f"SPHN-{stand}"].root_node[0]
                            if root_node.startswith('http'):
                                standard_concept = root_node
                            else:
                                if len(imports[f"SPHN-{stand}"].root_node) > 1:
                                    logger.warning(f"Terminology {stand} has more than one root node but only considering the first: {stand} -> {imports[stand].root_node}")
                                if f"{imports[f'SPHN-{stand}'].prefix}:".lower() in root_node.lower():
                                    root_node = root_node.split(":", 1)[1]
                                standard_concept = standard_iri + root_node

                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[standard_concept],
                            config=config,
                        )
                # add Terminology restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                for stand in valid_standard_list:
                    stand = stand.upper()
                    if stand == "TERMINOLOGY" and not Code:
                        terminology_iri = config.concepts["Terminology"].identifier
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[terminology_iri],
                            config=config
                        )
            if len(valid_standard_list) == 1:
                # logger.warning("found : " + str(standard_list) + " as allowed coding system")
                if valid_standard_list[0].upper() not in imports:
                    # EDAM specific code in "additional information"
                    if "descendant of" in sanitize(record[COLUMN_MAP["additional_information"]]).lower():
                        value = sanitize(record[COLUMN_MAP["additional_information"]])
                        try:
                            code = extract_code(value, REGEX_MAP["descendant_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{value}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {sanitize(', '.join(original_standard_list))}:{code}"
                    else:
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {sanitize(', '.join(original_standard_list))}"
                    add_code_as_restriction(
                        concept=concept, prop=prop, notes={note}, config=config
                    )
                else:
                    if standard.lower().endswith('or other'):
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {sanitize(', '.join(original_standard_list))}"
                        add_code_as_restriction(
                            concept=concept, prop=prop, notes={note}, config=config
                        )
            else:
                # logger.warning("found : " + str(standard_list) + " as allowed coding system")
                if re.match(REGEX_MAP["for_standard"], sanitize(record[COLUMN_MAP["additional_information"]])):
                    value = sanitize(record[COLUMN_MAP["additional_information"]])
                    try:
                        code_standard = extract_code(value, REGEX_MAP["for_standard"]).strip()
                    except ValueError as ve:
                        logger.error(f"Could not extract standard name from '{value}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                        raise ve
                    code_string = value.split(":", 1)[1].strip()
                    if "descendant of" in value.lower():
                        try:
                            code = extract_code(code_string, REGEX_MAP["descendant_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{code_string}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {code_standard}:{code}"
                else:
                    note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {sanitize(', '.join(original_standard_list))}"
                add_code_as_restriction(
                    concept=concept, prop=prop, notes={note}, config=config
                )

    else:
        # standard not defined
        if valueset:
            # standard not defined but valueset defined
            if 'as defined in the coding system' in valueset:
                pass
            elif "link to value set and specification" in valueset:
                pass
            elif 'link to value set:' in valueset:
                logger.warning(
                    f"Link to value set provided even though no standard defined: {valueset}"
                )
            else:
                if not object_type == 'qualitative':
                    logger.warning(f"{property_name} has a custom valueset defined but type is '{object_type}' instead of 'qualitative'")

                #print(f"valueset: {valueset}")
                if ":" in sanitize(record[COLUMN_MAP["reference"]]):
                    collection_name_concept = sanitize(record[COLUMN_MAP["reference"]]).split(":")[1]
                    collection_name_concept_prefix = sanitize(record[COLUMN_MAP["reference"]]).split(":")[0].lower()
                else:
                    collection_name_concept = sanitize(record[COLUMN_MAP["reference"]])
                    collection_name_concept_prefix = "sphn"
                collection_name_concept_identifier = format_class_name(collection_name_concept)

                if ":" in sanitize(record[COLUMN_MAP["name"]]):
                    collection_name_property = sanitize(record[COLUMN_MAP["name"]]).split(":")[1]
                    collection_name_property_prefix = sanitize(record[COLUMN_MAP["name"]]).split(":")[0].lower()
                else:
                    collection_name_property = sanitize(record[COLUMN_MAP["name"]])
                    collection_name_property_prefix = "sphn"
                collection_name_property_identifier = stringcase.camelcase(stringcase.snakecase(collection_name_property))

                if collection_name_property == "comparator":
                    collection_name = "Comparator"
                else:
                    collection_name = f"{collection_name_concept_identifier}_{collection_name_property_identifier}"

                collection_concept_iri_header = prefixer(collection_name_property_prefix, config)
                collection_concept_identifier = collection_concept_iri_header + collection_name
                if collection_concept_identifier in concepts:
                    collection_concept = concepts[collection_concept_identifier]
                else:
                    collection_concept_parent_iri = iri_header + "ValueSet"
                    if collection_concept_parent_iri in concepts:
                        collection_concept_parent = concepts[collection_concept_parent_iri]
                    else:
                        collection_concept_parent = Concept(
                            identifier=collection_concept_parent_iri,
                            label=f"Value Set",
                            description=f"List of value sets provided by {collection_name_property_prefix} project",
                            parent=config.iri.canonical_iri + "ValueSet"
                        )
                        concepts[collection_concept_parent_iri] = collection_concept_parent
                    if collection_name_property_prefix == config.prefix:
                        collection_concept_label = f"{sanitize(record[COLUMN_MAP['reference']])} {sanitize(record[COLUMN_MAP['name']])}"
                    else:
                        if ':' in record[COLUMN_MAP["reference"]]:
                            ref = sanitize(record[COLUMN_MAP["reference"]]).split(":", 1)[1]
                        else:
                            ref = sanitize(record[COLUMN_MAP["reference"]])
                        if ':' in record[COLUMN_MAP["name"]]:
                            n = sanitize(record[COLUMN_MAP["name"]]).split(":", 1)[1]
                        else:
                            n = sanitize(record[COLUMN_MAP["name"]])
                        collection_concept_label = f"{ref} {n}"
                    collection_concept = Concept(
                        identifier=collection_concept_identifier,
                        label=collection_concept_label,
                        description=sanitize(record[COLUMN_MAP["description"]]),
                        parent=collection_concept_parent.identifier,
                    )
                    concepts[collection_concept_identifier] = collection_concept

                elements = set([x.strip() for x in valueset.split(";")])
                for element in elements:
                    formatted_element = format_class_name(element)
                    if formatted_element in concepts:
                        logger.info(f"Treating {formatted_element} from valueset as a Concept")
                        element_iri = config.iri.canonical_iri + formatted_element
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[element_iri],
                            config=config
                        )
                    else:
                        element_identifier = format_named_individual(element)
                        named_individual_iri = get_iri_for_named_individual(collection_concept_iri_header)
                        if named_individual_iri+element_identifier in individuals:
                            named_individual = individuals[named_individual_iri+element_identifier]
                        else:
                            named_individual = Individual(
                                identifier=named_individual_iri+element_identifier,
                                label=element
                            )
                            individuals[named_individual_iri+element_identifier] = named_individual
                        named_individual.type.append(collection_concept_identifier)
                add_named_individuals_as_restriction(
                    concept=concept,
                    prop=prop,
                    collection_concept=collection_concept,
                    config=config,
                )
        else:
            # standard and valueset not defined
            pass

    if COLUMN_MAP["excluded_type"] in record and record[COLUMN_MAP["excluded_type"]]:
        excluded_types = sanitize(record[COLUMN_MAP["excluded_type"]]).split(";")
        add_excluded_type_as_note(concept=concept, property=prop, excluded_types=excluded_types, config=config)

    if COLUMN_MAP["sensitive"] in record:
        if record[COLUMN_MAP["sensitive"]] and sanitize(record[COLUMN_MAP["sensitive"]]).lower() == "yes":
            if domain_prefix == config.prefix:
                prop.sensitive = True
            else:
                prop.project_sensitive = True

    return prop


def parse_standard_as_restriction(
    concept: Concept, prop: Property, property_path : List[Property], standard: str, valueset: str, prefix: str, add_info: str, config: Config = get_config()
) -> None:
    """
    Parse a standard and valueset as a restriction
    on a given property of a given concept.

    Args:
        concept: An instance of concept
        prop: An instance of a property
        property_path: a list of Property instances that represents a property path
        standard: the standard to which the codes or valueset belongs to
        valueset: A valueset from a standard
        prefix: the prefix
        add_info: additional information from the dataset
        config: the config

    """
    list_of_values = []
    if not valueset.startswith("link to value set and specification"): list_of_values = [x.strip() for x in valueset.split(";")]
    logger.debug(f"standard: '{standard}' valueset: '{valueset}'")
    logger.debug("extracting path: " + str(property_path) + " for " +str(concept))

    if list_of_values:
        new_list_of_values = []
        if list_of_values[0].strip().startswith('for '):
            value_standard = None
            for value in list_of_values:
                if 'for ' in value:
                    new_list_of_values.append(value)
                    value_standard, value = value.split(':', 1)
                else:
                    value = f"{value_standard}: {value}"
                    new_list_of_values.append(value)
        if new_list_of_values:
            list_of_values = new_list_of_values

    imports = copy.deepcopy(config.imports)
    if config.project_metadata:
        imports.update(config.project_metadata.imports)

    if valueset.startswith("extendable value set:"):
        recommended_codes = parse_extendable_valueset(standard, list_of_values)
        if property_path:
            path = prop.identifier.split('#')[1]
            for x in property_path:
                path = path + "/" + prefix + ":" + x.split('#')[1]
            note = f"{prefix}:{path} recommended values: {', '.join(map(str,recommended_codes))}"
        else:
            note = f"{prefix}:{get_reference(prop.identifier)} recommended values: {', '.join(map(str,recommended_codes))}"
        if standard.upper() in imports:
            standard = standard.upper()
        if standard in imports:
            # standard_iri = imports[standard].iri.canonical_iri
            # if imports[standard].create_root_node:
            #     root_node = imports[standard].root_node[0]
            #     if f"{imports[standard].prefix}:" in root_node:
            #         root_node = root_node.split(":", 1)[1]
            # else:
            #     standard_iri = imports[f"SPHN-{standard}"].iri.canonical_iri
            #     root_node = imports[f"SPHN-{standard}"].root_node[0]
            #     if f"{imports[f'SPHN-{standard}'].prefix}:" in root_node:
            #         root_node = root_node.split(":", 1)[1]
            #standard_concept = standard_iri + root_node
            add_code_as_restriction(
                concept=concept,
                prop=prop,
                # commenting out code since we do not want a restriction on the root node for this scenario
                #codes=[standard_concept],
                prop_path=property_path,
                notes={note},
                config=config
            )
    else:
        standards_in_valueset = []
        descendant_of = False
        for value in list_of_values:
            if value:
                # check if the value is of the format 'for XYZ: descendant of'
                if re.match(REGEX_MAP["for_standard"], value):
                    descendant_of = False
                    try:
                        code_standard = extract_code(value, REGEX_MAP["for_standard"]).strip()
                    except ValueError as ve:
                        logger.error(f"Could not extract standard name from '{value}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                        raise ve
                    code_string = value.split(":", 1)[1].strip()
                    if "child of" in code_string.lower():
                        try:
                            code = extract_code(code_string, REGEX_MAP["child_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{code_string}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = f"{prefix}:{get_reference(prop.identifier)} {ONLY_DIRECT_SUBCLASSES}"
                    elif "descendant of" in code_string.lower():
                        descendant_of = True
                        try:
                            code = extract_code(code_string, REGEX_MAP["descendant_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{code_string}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = None
                    elif descendant_of:
                        try:
                            code = extract_code(code_string, REGEX_MAP["code"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{code_string}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = None
                    else:
                        try:
                            code = extract_code(code_string, REGEX_MAP["code"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{code_string}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        if standard not in {'UCUM'}:
                            scope_note = f"{prefix}:{get_reference(prop.identifier)} {NO_SUBCLASSES}"
                        else:
                            scope_note = None
                    logger.debug(f"Extracting standard: '{code_standard}' code: '{code}' from '{value}'")
                    if code_standard.upper() in imports:
                        code_standard = code_standard.upper()
                    if code_standard == "SNOMED":
                        code_standard = "SNOMED CT"
                    if code_standard == "CHOP":
                        if code.startswith("Z"):
                            code = code[1:]
                    standards_in_valueset.append(code_standard)
                    if code_standard in imports:
                        code_iri = prepare_standard_code_iri(
                            standard=code_standard,
                            code=code,
                            imports=imports,
                            config=config
                        )
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[code_iri],
                            prop_path=property_path,
                            scope_notes={scope_note} if scope_note else set(),
                            config=config
                        )
                    elif code_standard.lower() == "sphn":
                        code_iri = prepare_standard_code_iri(
                            standard=code_standard.lower(),
                            code=code,
                            imports=imports,
                            config=config
                        )
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[code_iri],
                            prop_path=property_path,
                            scope_notes={scope_note} if scope_note else set(),
                            config=config
                        )
                    else:
                        logger.warning(f"{code_standard} not in imports; Defaulting to Code")
                        code_iri = config.iri.canonical_iri + "Code"
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {standard}"
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[code_iri],
                            prop_path=property_path,
                            notes={note},
                            config=config
                        )
                else:
                    if "child of" in value.lower():
                        try:
                            code = extract_code(value, REGEX_MAP["child_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{valueset}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = f"{prefix}:{get_reference(prop.identifier)} {ONLY_DIRECT_SUBCLASSES}"
                    elif "descendant of" in value.lower():
                        descendant_of = True
                        try:
                            code = extract_code(value, REGEX_MAP["descendant_of"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{valueset}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = None
                    elif descendant_of:
                        try:
                            code = extract_code(value, REGEX_MAP["code"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{valueset}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        scope_note = None
                    else:
                        try:
                            code = extract_code(value, REGEX_MAP["code"])
                        except ValueError as ve:
                            logger.error(f"Could not extract code from '{valueset}'. This is usually due to the values in the '{COLUMN_MAP['valueset']}' column not conforming to the guidelines.")
                            raise ve
                        if standard not in {'UCUM'}:
                            scope_note = f"{prefix}:{get_reference(prop.identifier)} {NO_SUBCLASSES}"
                        else:
                            scope_note = None
                    logger.debug(f"Extracting code: '{code}' from '{value}'")
                    if standard.upper() in imports:
                        standard = standard.upper()
                    if standard == "SNOMED":
                        standard = "SNOMED CT"
                    if standard == "CHOP":
                        if code.startswith("Z"):
                            code = code[1:]
                    if standard in imports:
                        code_iri = prepare_standard_code_iri(
                            standard=standard,
                            code=code,
                            imports=imports,
                            config=config
                        )
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[code_iri],
                            prop_path=property_path,
                            scope_notes={scope_note} if scope_note else set(),
                            config=config
                        )
                    else:
                        logger.warning(f"{standard} not in imports; Defaulting to Code")
                        code_iri = config.iri.canonical_iri + "Code"
                        note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {standard}"
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            prop_path=property_path,
                            codes=[code_iri],
                            notes={note},
                            config=config
                        )
        if standards_in_valueset:
            original_standard_list = [x.strip() for x in standard.split(",")]
            # check if we find the "or other" as a element in the split, if so we add a Node, otherwise we continue
            if standard.lower().endswith("or other"):
                new_last_element = original_standard_list[-1][:-len("or other")].strip()
                if len(original_standard_list) == 1:
                    valid_standard_list = [new_last_element]
                else:
                    valid_standard_list = original_standard_list[:-1]+[new_last_element]
                # decide if "Code" or "Terminology" should be added for "or other"
                for i,s in enumerate(valid_standard_list):
                    s = s.upper()
                    if s in imports:
                        valid_standard_list[i] = s.upper()
                    if s == "SNOMED":
                        valid_standard_list[i] = "SNOMED CT"
                code_iri = config.iri.canonical_iri + "Code"
                add_code_as_restriction(
                    concept=concept,
                    prop=prop,
                    codes=[code_iri],
                    config=config
                )
                terminology_iri = config.concepts["Terminology"].identifier
                add_code_as_restriction(
                    concept=concept,
                    prop=prop,
                    codes=[terminology_iri],
                    config=config
                )
            else:
                valid_standard_list = original_standard_list
            note = None
            Code = False
            if not standard.lower().endswith("or other"):
                for stand in valid_standard_list:
                    if stand not in standards_in_valueset:
                        # add Code restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                        if not stand in imports and not stand == "Terminology":
                            if not "Terminology" in valid_standard_list:
                                code_iri = config.iri.canonical_iri + "Code"
                                add_code_as_restriction(
                                    concept=concept,
                                    prop=prop,
                                    codes=[code_iri],
                                    config=config
                                )
                            else:
                                Code = True
                for stand in valid_standard_list:
                    if stand not in standards_in_valueset:
                        # add other restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                        if (stand in imports and
                            not ("Terminology" in valid_standard_list and Code)
                            ):
                            standard_iri = imports[stand].iri.canonical_iri
                            if imports[stand].create_root_node:
                                root_node = imports[stand].root_node[0]
                                if root_node.startswith('http'):
                                    standard_concept = root_node
                                else:
                                    if f"{imports[stand].prefix}:" in root_node:
                                        root_node = root_node.split(":", 1)[1]
                                    standard_concept = standard_iri + root_node
                            else:
                                standard_iri = imports[f"SPHN-{stand}"].iri.canonical_iri
                                root_node = imports[f"SPHN-{stand}"].root_node[0]
                                if root_node.startswith('http'):
                                    standard_concept = root_node
                                else:
                                    if f"{imports[f'SPHN-{stand}'].prefix}:" in root_node:
                                        root_node = root_node.split(":", 1)[1]
                                    standard_concept = standard_iri + root_node

                            add_code_as_restriction(
                                concept=concept,
                                prop=prop,
                                codes=[standard_concept],
                                config=config,
                            )
                # add Terminology restriction only if its not sphn:Code sphn:Terminology for a hasCode property
                for stand in valid_standard_list:
                    if stand not in standards_in_valueset:
                        if stand == "Terminology" and not Code:
                            terminology_iri = config.concepts["Terminology"].identifier
                            add_code_as_restriction(
                                concept=concept,
                                prop=prop,
                                codes=[terminology_iri],
                                config=config
                            )
            # EDQM specific code in "additional information"
            if re.match(REGEX_MAP["for_standard"], add_info):
                try:
                    code_standard = extract_code(add_info, REGEX_MAP["for_standard"]).strip()
                except ValueError as ve:
                    logger.error(f"Could not extract code from '{add_info}'. This is usually due to the values in the '{COLUMN_MAP['additional_information']}' column not conforming to the guidelines.")
                    raise ve
                code_string = add_info.split(":", 1)[1].strip()
                if "descendant of" in add_info.lower():
                    try:
                        code = extract_code(code_string, REGEX_MAP["descendant_of"])
                    except ValueError as ve:
                        logger.error(f"Could not extract code from '{add_info}'. This is usually due to the values in the '{COLUMN_MAP['additional_information']}' column not conforming to the guidelines.")
                        raise ve
                    note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {standard}:{code}"
                    add_code_as_restriction(
                        concept=concept, prop=prop, notes={note}, config=config
                    )
            else:
                note = f"{prefix}:{get_reference(prop.identifier)} allowed coding system: {sanitize(', '.join(original_standard_list))}"
                add_code_as_restriction(
                    concept=concept, prop=prop, notes={note}, config=config
                )


def prepare_standard_code_iri(standard: str, code: str, imports: Dict, config: Config = get_config()) -> str:
    """
    Prepare the IRI for a code from a given standard.

    Args:
        standard: the standard to which the code belongs to
        code: the code identifier
        imports: a dictionary of imports
        config: the config

    Returns:
        Returns a formatted IRI for the code

    """
    if standard in imports:
        standard_iri = imports[standard].iri.canonical_iri
        if standard in OBO_ONTOLOGIES:
            if ":" in code:
                code = code.split(":", 1)[1]
        # elif standard == "EDAM":
        #     if ":" in code:
        #         code = code.replace(":", "_")
        else:
            if ":" in code:
                code = code.split(":", 1)[1]
        code_iri = standard_iri + code
    elif standard == 'sphn':
        standard_iri = config.iri.canonical_iri
        code_iri = standard_iri + code
    else:
        logger.warning(f"Import metadata for '{standard}' not defined in Config; Using default IRI https://example.org/")
        code_iri = f"https://example.org/{code}"
    return code_iri


def parse_cardinalities(cardinalities: pd.DataFrame, properties: Dict, config: Config = get_config()) -> Dict:
    """
    Parse cardinalities.

    Args:
        cardinalities: a DataFrame containing the cardinalities from the SPHN Dataset
        properties: a dictionary of properties
        config: the config

    Returns:
        A dictionary that contains cardinality information for each property.

    """
    property_cardinalities: Dict = {}
    cardinalities['row_number'] = cardinalities.reset_index().index
    for index, row in cardinalities.iterrows():
        record = row.to_dict()
        if sanitize(record[CARDINALITIES_COLUMN_MAP["active_status"]].lower()) == "yes":
            if sanitize(record[CARDINALITIES_COLUMN_MAP["category"]]) == "concept":
                if ":" in sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]):
                    concept_name = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]).split(":")[1]
                    concept_prefix = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]).split(":")[0].lower()
                else:
                    concept_name = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]])
                    concept_prefix = "sphn"
                concept_iri_header = prefixer(concept_prefix, config)
                property_iri_header = config.iri.canonical_iri
                administrative_case_cardinality = sanitize(record[CARDINALITIES_COLUMN_MAP["hasAdministrativeCase"]])
                subject_pseudo_identifier_cardinality = sanitize(record[CARDINALITIES_COLUMN_MAP["hasSubjectPseudoIdentifier"]])
                if CARDINALITIES_COLUMN_MAP['hasSourceSystem'] in record:
                    source_system_cardinality = sanitize(record[CARDINALITIES_COLUMN_MAP["hasSourceSystem"]])
                    if source_system_cardinality:
                        key = f"{concept_name}.source system"
                        prop = prepare_cardinalities(
                            concept_name, 'source system', source_system_cardinality, concept_iri_header, property_iri_header
                        )
                        property_cardinalities[key] = prop
                if administrative_case_cardinality:
                    # if the concept IRI is not the SPHN prefix, then we need to create the three main case cardinalities in the project prefix if they not exist
                    key = f"{concept_name}.administrative case"
                    prop = prepare_cardinalities(
                        concept_name, "administrative case", administrative_case_cardinality, concept_iri_header, property_iri_header
                    )
                    property_cardinalities[key] = prop
                if CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"] in record:
                    data_provider_institute_cardinality = sanitize(record[CARDINALITIES_COLUMN_MAP["hasDataProviderInstitute"]])
                    if data_provider_institute_cardinality:
                        # if the concept IRI is not the SPHN prefix, then we need to create the three main case cardinalities in the project prefix if they not exist
                        if data_provider_institute_cardinality:
                        # if the concept IRI is not the SPHN prefix, then we need to create the three main case cardinalities in the project prefix if they not exist
                            key = f"{concept_name}.data provider institute"
                            prop = prepare_cardinalities(
                                concept_name, "data provider institute", data_provider_institute_cardinality, concept_iri_header, property_iri_header
                            )
                            property_cardinalities[key] = prop
                if CARDINALITIES_COLUMN_MAP["hasDataProvider"] in record:
                    data_provider_cardinality = sanitize(record[CARDINALITIES_COLUMN_MAP["hasDataProvider"]])
                    if data_provider_cardinality:
                        key = f"{concept_name}.data provider"
                        prop = prepare_cardinalities(
                            concept_name, "data provider", data_provider_cardinality, concept_iri_header, property_iri_header
                        )
                        property_cardinalities[key] = prop
                if subject_pseudo_identifier_cardinality:
                    # if the concept IRI is not the SPHN prefix, then we need to create the three main case cardinalities in the project prefix if they not exist
                    key = f"{concept_name}.subject pseudo identifier"
                    prop = prepare_cardinalities(
                        concept_name, "subject pseudo identifier", subject_pseudo_identifier_cardinality, concept_iri_header, property_iri_header
                    )
                    property_cardinalities[key] = prop
            elif sanitize(record[CARDINALITIES_COLUMN_MAP["category"]]) in {"composedOf", "inherited"}:
                if ":" in sanitize(record[CARDINALITIES_COLUMN_MAP["reference"]]):
                    concept_name = sanitize(record[CARDINALITIES_COLUMN_MAP["reference"]]).split(":")[1]
                    concept_prefix = sanitize(record[CARDINALITIES_COLUMN_MAP["reference"]]).split(":")[0].lower()
                else:
                    concept_name = sanitize(record[CARDINALITIES_COLUMN_MAP["reference"]])
                    concept_prefix = "sphn"
                concept_iri_header = prefixer(concept_prefix, config)

                if ":" in sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]):
                    property_name = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]).split(":")[1]
                    property_prefix = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]]).split(":")[0].lower()
                else:
                    property_name = sanitize(record[CARDINALITIES_COLUMN_MAP["name"]])
                    property_prefix = "sphn"
                property_iri_header = prefixer(property_prefix, config)
                cardinality_pattern = sanitize(record[CARDINALITIES_COLUMN_MAP["cardinality"]])
                if cardinality_pattern:
                    key = f"{concept_name}.{property_name}"
                    prop = prepare_cardinalities(concept_name, property_name, cardinality_pattern, concept_iri_header, property_iri_header)
                    property_cardinalities[key] = prop
                else:
                    raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] No cardinality defined for {concept_name}.{property_name}")
    return property_cardinalities


def parse_coding_system_information(coding_system_info: pd.DataFrame, config: Config = get_config()) -> Dict:
    """
    Parse information about coding system.

    Args:
        coding_system_info: a DataFrame containing the information about coding system
        config: the config

    Returns:
        A dictionary that contains information such as prefix, canonical_iri. versioned_iri for
        all the terminologies defined in the SPHN Dataset.

    """
    data = {}
    if CODING_SYSTEM_INFO_COLUMN_MAP['provided_by'] in coding_system_info:
        provided_by_list = [x for x in coding_system_info[CODING_SYSTEM_INFO_COLUMN_MAP['provided_by']].unique() if x]
        if len(provided_by_list) > 2:
            raise ValueError(f"Terminologies can only be provided by 'sphn' or the project. Found more than two providers in 'Coding System and Version' sheet: {provided_by_list}")

    for row in coding_system_info.to_dict('records'):
        if CODING_SYSTEM_INFO_COLUMN_MAP["short_name"] in coding_system_info:
            name = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP['short_name']])
            if not name:
                logger.warning(f"'{CODING_SYSTEM_INFO_COLUMN_MAP['short_name']}' is empty in 'Coding System and Version' sheet for {row}")
        if CODING_SYSTEM_INFO_COLUMN_MAP["full_name"] in coding_system_info:
            full_name = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP['full_name']])
            if not full_name:
                logger.warning(f"'{CODING_SYSTEM_INFO_COLUMN_MAP['full_name']}' is empty in 'Coding System and Version' sheet for {row}")
        if CODING_SYSTEM_INFO_COLUMN_MAP["version_pattern"] in coding_system_info:
            pattern = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP['version_pattern']])
            if not pattern:
                logger.warning(f"'{CODING_SYSTEM_INFO_COLUMN_MAP['version_pattern']}' is empty in 'Coding System and Version' sheet for {row}")
        if CODING_SYSTEM_INFO_COLUMN_MAP["example_version_pattern"] in coding_system_info:
            example = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP['example_version_pattern']])
            if not example:
                logger.warning(f"'{CODING_SYSTEM_INFO_COLUMN_MAP['example_version_pattern']}' is empty in 'Coding System and Version' sheet for {row}")

        if CODING_SYSTEM_INFO_COLUMN_MAP["provided_by"] not in row:
            continue
        name = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP['short_name']])
        provided_in_rdf = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["provided_in_rdf"]]).lower()
        canonical_iri = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["canonical_iri"]])
        versioned_iri = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["versioned_iri"]])
        if  provided_in_rdf == "yes":
            if not canonical_iri or not versioned_iri:
                raise ValueError(f"Terminology {name} is a terminology that is provided in RDF; Then both '{CODING_SYSTEM_INFO_COLUMN_MAP['canonical_iri']}' and '{CODING_SYSTEM_INFO_COLUMN_MAP['versioned_iri']}' must be defined in 'Coding System and Version' sheet")

        provided_by = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["provided_by"]])
        if provided_by:
            key = name.upper()
            prefix = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["prefix"]])
            root_node = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["root_node"]])
            if provided_in_rdf == "yes":
                if not prefix or not root_node:
                    raise ValueError(f"Terminology {name} is a terminology that is provided in RDF; Then both '{CODING_SYSTEM_INFO_COLUMN_MAP['prefix']}' and '{CODING_SYSTEM_INFO_COLUMN_MAP['root_node']}' must be defined in 'Coding System and Version' sheet")
            info = {
                "prefix": prefix,
                "iri": {
                    "canonical_iri": canonical_iri,
                    "versioned_iri": versioned_iri
                },
                "provided_by": provided_by
            }
            resource_prefix = sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["resource_prefix"]])
            if resource_prefix:
                resource_key = resource_prefix.upper()
                resource_info = {
                    "prefix": resource_prefix,
                    "iri": {
                        "canonical_iri": sanitize(row[CODING_SYSTEM_INFO_COLUMN_MAP["resource_iri"]]),
                        "versioned_iri": versioned_iri
                    },
                    "provided_by": provided_by
                }
                del info["iri"]["versioned_iri"]
                if root_node:
                    if sanitize(root_node.upper()) == key:
                        resource_info["root_node"] = [x.strip() for x in root_node.split(";")]
                        resource_info["create_root_node"] = True
                    else:
                        info["root_node"] = [x.strip() for x in root_node.split(";")]
                        info["create_root_node"] = True
                data[resource_key] = ImportConfig(**resource_info)
            else:
                if root_node:
                    info["root_node"] = [x.strip() for x in root_node.split(";")]
                    info["create_root_node"] = True
            data[key] = ImportConfig(**info)
    return data


def add_concept_mappings(concept: Concept, mappings: List[str]) -> None:
    """
    Add a list of mappings to the given Concept.

    Args:
        concept: An instance of ``dataset2rdf.models.Concept``
        mappings: A list of mappings

    """
    for mapping in mappings:
        if ":" not in mapping:
            raise ValueError(f'Formatting error found in meaning binding "{mappings}". PLease check, if entry follows the pattern "terminology:code" for each code.')
        try:
            standard = extract_code(code_str=mapping, pattern=REGEX_MAP["meaning_binding_standard"]).strip()
        except ValueError as ve:
            logger.error(f"Could not extract name of standard from '{mapping}'. This is usually due to the values in the '{COLUMN_MAP['meaning_binding']}' column not conforming to the guidelines.")
            raise ve
        try:
            identifier = extract_code(code_str=mapping, pattern=REGEX_MAP["meaning_binding_code"]).strip()
        except ValueError as ve:
            logger.error(f"Could not code from '{mapping}'. This is usually due to the values in the '{COLUMN_MAP['meaning_binding']}' column not conforming to the guidelines.")
            raise ve
        mapping_obj = Mapping(
            standard=standard,
            identifier=identifier,
            predicate=OWL.equivalentClass
        )
        concept.mappings.append(mapping_obj)


def prepare_cardinalities(concept_name: str, property_name: str, cardinality_pattern: str, iri_header_concept: str, iri_header_property :str) -> Dict:
    """
    Prepare cardinality representation for a given concept's property.

    Args:
        concept_name: The name of the concept
        property_name: The name of the property
        cardinality_pattern: The cardinality pattern
        iri_header_concept: The IRI for the concept
        iri_haeder_property: The IRI for the property

    Returns:
        A dictionary that is the cardinality representation

    """
    cardinality_regex = r"([\d]+:[\d]+|[\d]+:[n])"
    prop = {
        "property_name": property_name,
        "property_identifier": iri_header_property+format_property_name(property_name),
        "concept_name": concept_name,
        "concept_identifier": iri_header_concept+format_class_name(concept_name),
    }
    try:
        extracted_pattern = extract_code(cardinality_pattern, cardinality_regex)
    except ValueError as ve:
        logger.error(f"Could not cardinality from '{cardinality_pattern}'. This is usually due to the values in the cardinality column not conforming to the guidelines.")
        raise ve
    if extracted_pattern:
        min_cardinality, max_cardinality = cardinality_pattern.split(":", 1)
    else:
        raise ValueError(f"Malformed cardinality pattern '{cardinality_pattern}' for {concept_name}.{property_name}.")
    prop["min_cardinality"] = min_cardinality
    if max_cardinality != "n":
        prop["max_cardinality"] = max_cardinality
    return prop


def infer_domain(concepts: Dict, properties: Dict) -> None:
    """
    Infer domain for all properties.

    Args:
        concepts: A dictionary of concepts
        properties: A dictionary of properties

    """
    for concept in concepts.values():
        for restriction in concept.restrictions.values():
            if restriction.property_identifier == "http://purl.org/dc/terms/conformsTo":
                continue
            prop = properties[restriction.property_identifier]
            _infer_domain(concept, prop, properties)


def _infer_domain(concept: Concept, prop: Property, properties: Dict, config : Config = get_config()) -> None:
    """
    Infer domain for a property and its parent property.

    Args:
        concept: An instance of ``dataset2rdf.models.Concept``
        prop: An instance of ``dataset2rdf.models.Property``
        properties: A dictionary of properties
        config: the config

    """
    if concept not in prop.domain and concept.identifier not in prop.domain:
        prop.domain.append(concept)
    if prop.parent and prop.parent != prop.identifier and not prop.parent.endswith("AttributeObject") and not prop.parent.endswith("AttributeDatatype"):
        if prop.parent in properties:
            parent_property = properties[prop.parent]
        else:
            logger.warning(f"{prop.identifier} has a never seen before property as a parent: {prop.parent}")
            parent_property = Property(identifier=prop.parent, parent="SPHNAttributeObject")
            properties[prop.parent] = parent_property
        _infer_domain(concept, parent_property, properties)


def apply_cardinalities(concepts: Dict, properties: Dict, cardinalities: Dict) -> None:
    """
    Apply cardinalities as a restriction on a concept.

    Args:
        concepts: A dictionary of concepts
        properties: A dictionary of properties
        cardinalities: A dictionary of cardinalities

    """
    for key, cardinality in cardinalities.items():
        concept_identifier = cardinality["concept_identifier"]
        property_identifier = cardinality["property_identifier"]
        if property_identifier in properties:
            if concept_identifier in concepts:
                concept = concepts[concept_identifier]
                r_key = f"{concept_identifier}-{property_identifier}"
                if r_key in concept.restrictions:
                    r1 = concept.restrictions[r_key]
                else:
                    r1 = Restriction(
                        class_identifier=concept_identifier, property_identifier=property_identifier
                    )
                    concept.restrictions[r_key] = r1
                if "min_cardinality" in cardinality:
                    r1.min_cardinality = int(cardinality["min_cardinality"])
                if "max_cardinality" in cardinality:
                    r1.max_cardinality = int(cardinality["max_cardinality"])
            else:
                logger.warning(
                    f"{concept_identifier}.{property_identifier} cardinality defined but {concept_identifier} does not exist as a valid/active Concept in the Dataset"
                )
        else:
            logger.warning(
                f"{concept_identifier}.{property_identifier} cardinality defined but {property_identifier} does not exist as a valid/active property in the Dataset"
            )


def set_property_range(dataset: pd.DataFrame, concepts: Dict, properties: Dict, config: Config = get_config()) -> None:
    """
    Define the ranges of each property

    Args:
        dataset: a DataFrame containing the content of the SPHN Dataset
        concepts: A dictionary of concepts
        properties: A dictionary of properties
        config: the config

    """
    for index, row in dataset.iterrows():
        record = row.to_dict()
        if sanitize(record[COLUMN_MAP["active_status"]].lower()) == "yes":
            if ":" in sanitize(record[COLUMN_MAP["name"]]):
                prefix = sanitize(record[COLUMN_MAP["name"]]).split(":")[0].lower()
            else:
                prefix = "sphn"
            iri_header = prefixer(prefix, config)
            category = sanitize(record[COLUMN_MAP["category"]])
            if category in {"composedOf", "inherited"}:
                if ":" in sanitize(record[COLUMN_MAP["name"]]):
                    property_identifier = format_property_name(sanitize(record[COLUMN_MAP["name"]]).split(":")[1].lower())
                else:
                    property_identifier = format_property_name(sanitize(record[COLUMN_MAP["name"]]))
                # check for empty cells
                if sanitize(record[COLUMN_MAP["type"]]) == "":
                    raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] No value found in column type for property {property_identifier}. Please enter an objecttype (concept) or datatype.")
                prop = properties[iri_header+property_identifier]
                if ";" in sanitize(record[COLUMN_MAP["type"]]):
                    prop_types = sanitize(record[COLUMN_MAP["type"]]).split(";")
                else:
                    prop_types = sanitize(record[COLUMN_MAP["type"]]).split(",")

                for prop_type in prop_types:
                    prop_type_f = sanitize(format_class_name(prop_type))
                    if is_curie(prop_type_f):
                        prefix, prop_type_identifier = [x.strip() for x in prop_type_f.split(":", 1)]
                    else:
                        prefix = config.prefix
                        prop_type_identifier = prop_type_f
                    type_iri_header = prefixer(prefix, config)
                    if type_iri_header+prop_type_identifier in concepts:
                        # prop is an objectProperty and range is a concept
                        if type_iri_header+prop_type_identifier == type_iri_header+"Code":
                            if concepts[type_iri_header+'Terminology'] not in prop.range:
                                prop.range.append(concepts[config.iri.canonical_iri + "Terminology"])
                        if concepts[type_iri_header+prop_type_identifier] not in prop.range:
                            prop.range.append(concepts[type_iri_header+prop_type_identifier])
                    elif prop_type_identifier in {'datetime', 'string', 'double', 'qualitative', 'quantitative', 'temporal', 'anyURI'}:
                        if iri_header+property_identifier in {config.iri.canonical_iri + "hasIdentifier", config.iri.canonical_iri + "hasName", config.iri.canonical_iri + "hasCodingSystemAndVersion"}:
                            if config.iri.canonical_iri + "string" not in prop.range:
                                prop.range.append(config.iri.canonical_iri + "string")
                        else:
                            if type_iri_header+prop_type_identifier not in prop.range:
                                prop.range.append(type_iri_header+prop_type_identifier)
                    elif prop_type_identifier.lower() in {'datetime', 'string', 'double', 'qualitative', 'quantitative', 'temporal'}:
                        raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] Invalid property type: {prop_type_identifier} for property {property_identifier}. Please use only lower case for datatypes.")
                    else:
                        prop_type_f = sanitize(format_class_name(prop_type.title()))
                        if is_curie(prop_type_f):
                            prefix, prop_type_identifier = [x.strip() for x in prop_type_f.split(":", 1)]
                        else:
                            prefix = config.prefix
                            prop_type_identifier = prop_type_f
                        if type_iri_header+prop_type_identifier in concepts:
                            raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] unexpected property type: {prop_type} for property {property_identifier}. Please use title case for concepts.")
                        else:
                            raise ValueError(f"[Row {record['row_number']+IDX_OFFSET}] unexpected property type: {prop_type} for property {property_identifier}. Property type should be a concept or a datatype ('datetime', 'string', 'double', 'qualitative', 'quantitative' or 'temporal').")

                if iri_header+property_identifier in {config.iri.canonical_iri + 'hasValue'}:
                    concept_reference = sanitize(record[COLUMN_MAP["reference"]])
                    concept_identifier = format_class_name(concept_reference)
                    if is_curie(concept_identifier):
                        concept_prefix, concept_identifier = concept_identifier.split(":", 1)
                        concept_iri_header = config.project_metadata.iri.canonical_iri
                    else:
                        concept_prefix = "sphn"
                        concept_iri_header = iri_header
                    concept = concepts[concept_iri_header+concept_identifier]
                    code_iri = None
                    if prop_type == "double":
                        code_iri = XSD.double
                    elif prop_type == "string":
                        code_iri = XSD.string
                    elif prop_type == "quantitative":
                        code_iri = XSD.double

                    if code_iri:
                        add_code_as_restriction(
                            concept=concept,
                            prop=prop,
                            codes=[code_iri]
                        )

def apply_type_restrictions(concepts: Dict[str, Concept], properties: Dict[str, Property], config: Config = get_config()) -> None:
    """
    Apply type restrictions on concepts.

    Args:
        concepts: A dictionary of concepts
        properties: A dictionary of properties
        config: the config

    """
    typical_ranges = {config.iri.canonical_iri + 'Code', config.iri.canonical_iri + 'Terminology'}
    for concept in concepts.values():
        for r_key, res in concept.type_restrictions.items():
            prop = properties[res.property_identifier]
            if len(prop.range) > 1:
                prop_ranges = []
                for r in prop.range:
                    if isinstance(r, str):
                        range_concept = concepts[r]
                        range_concept_identifier = range_concept.identifier
                        prop_ranges.append(range_concept_identifier)
                    else:
                        prop_ranges.append(r.identifier)
                prop_ranges = set(prop_ranges)
                if any(x in prop_ranges for x in typical_ranges):
                    continue
                else:
                    if r_key in concept.restrictions:
                        merged = merge_restriction(concept.restrictions[r_key], res)
                        concept.restrictions[r_key] = merged
                    else:
                        concept.restrictions[r_key] = res
            else:
                prop_range = prop.range[0]
                if not isinstance(prop_range, str):
                    if prop_range.identifier in typical_ranges:
                        continue
                    else:
                        if r_key in concept.restrictions:
                            merged = merge_restriction(concept.restrictions[r_key], res)
                            concept.restrictions[r_key] = merged
                        else:
                            concept.restrictions[r_key] = res



def merge_restriction(r1: Restriction, r2: Restriction) -> Restriction:
    """
    Merge two instances of Restrictions together where r2 extends r1.

    Args:
        r1: An instance of Restriction
        r2: An instance of Restriction

    Returns:
        A merged instance of Restriction

    """
    if r1.min_cardinality is not None:
        if r2.min_cardinality:
            logger.debug(f"[merge_restriction] Merging min cardinality from {r1.min_cardinality} to {r2.min_cardinality}")
            r1.min_cardinality = r2.min_cardinality
    else:
        r1.min_cardinality = r2.min_cardinality
    if r1.max_cardinality is not None:
        if r2.max_cardinality:
            logger.debug(f"[merge_restriction] Merging max cardinality from {r1.max_cardinality} to {r2.max_cardinality}")
            r1.max_cardinality = r2.max_cardinality
    else:
        r1.max_cardinality = r2.max_cardinality
    if r1.some_values_from:
        if r2.some_values_from:
            logger.debug(f"[merge_restriction] Merging svf from {r1.some_values_from} to {r2.some_values_from}")
            for value in r2.some_values_from:
                if value not in r1.some_values_from:
                    r1.some_values_from.add(value)
    else:
        r1.some_values_from = r2.some_values_from
    return r1


def parse_element(element: str) -> str:
    """
    Parse an element according to certain formatting expectations.

    Args:
        element: A string that needs to be parsed or formatted

    Returns:
        A parsed or formatted string

    """
    parsed_element = element.strip()
    if "%" in parsed_element:
        parsed_element = parsed_element.replace("%","percent")
    if "[" in parsed_element and "]" in parsed_element:
        parsed_element = parsed_element.replace("[", "sbl").replace("]", "sbr")
    if "{" in parsed_element and "}" in parsed_element:
        parsed_element = parsed_element.replace("{", "cbl").replace("}", "cbr")
    if "(" in parsed_element and ")" in parsed_element:
        parsed_element = parsed_element.replace("(", "rbl").replace(")", "rbr")
    if "/" in parsed_element:
        parsed_element = parsed_element.replace("/", "per")
    if "<=" in parsed_element:
        parsed_element = parsed_element.replace("<=", "LessThanOrEqual")
    if ">=" in parsed_element:
        parsed_element = parsed_element.replace(">=", "GreaterThanOrEqual")
    if "<" in parsed_element:
        parsed_element = parsed_element.replace("<", "LessThan")
    if ">" in parsed_element:
        parsed_element = parsed_element.replace(">", "GreaterThan")
    if "=" in parsed_element:
        parsed_element = parsed_element.replace("=", "Equal")
    if "#" in parsed_element:
        parsed_element = parsed_element.replace("#", "nb")
    if "." in parsed_element:
        parsed_element = parsed_element.replace(".", "dot")
    if "*" in parsed_element:
        parsed_element = parsed_element.replace("*", "exp")
    if "'" in parsed_element:
        parsed_element = parsed_element.replace("'", "apo")
    return parsed_element


def add_code_as_restriction(
    concept: Concept,
    prop: Property,
    prop_path: List = [],
    codes: List = [],
    notes: Set = set(),
    scope_notes: Set = set(),
    config: Config = get_config(),
) -> Restriction:
    """
    Add a list of codes as ``owl:someValuesFrom`` restriction
    on a given property of a given concept.

    If ``prop_path`` is defined then the list of codes
    are treated as ``owl:hasValuesFrom`` restriction
    on the last property in ``prop_path``.

    Args:
        concept: An instance of concept
        prop: An instance of a property
        prop_path: A list of property names that represents a property path
        codes: A list of one or more codes that defines the value space
        notes: A set of one or more notes
        scope_notes: A set of one or more scope notes
        config: The config

    Returns:
        An instance of ``dataset2rdf.models.Restriction``

    """
    logger.debug("adding code as restriction: " + str(codes) +" for proppath: " + str(prop_path) + " and prop " + str(prop))
    r_key = f"{concept.identifier}-{prop.identifier}"
    if r_key in concept.restrictions:
        restriction = concept.restrictions[r_key]
    else:
        restriction = Restriction(
            class_identifier=concept.identifier, property_identifier=prop.identifier
        )
        concept.restrictions[r_key] = restriction
    if prop_path:
        property_path_key = r_key + '-' + '-'.join(prop_path)
        if property_path_key not in restriction.property_paths:
            property_path_restriction = PropertyPathRestriction(property_path=prop_path)
            restriction.property_paths[property_path_key] = property_path_restriction
        else:
            property_path_restriction = restriction.property_paths[property_path_key]
        if codes:
            for code in codes:
                if code not in property_path_restriction.some_values_from:
                    property_path_restriction.some_values_from.add(code)
    else:
        if codes:
            for code in codes:
                if code not in restriction.some_values_from:
                    restriction.some_values_from.add(code)
    if notes:
        restriction.notes.update(notes)
    if scope_notes:
        restriction.scope_notes.update(scope_notes)
    return restriction


def add_named_individuals_as_restriction(
    concept: Concept,
    prop: Property,
    collection_concept: Concept,
    scope_notes: Set = set(),
    config: Config = get_config(),
) -> Restriction:
    """
    Add a collection as ``owl:someValuesFrom`` restriction
    on a given property of a given concept.

    Args:
        concept: An instance of concept
        prop: An instance of a property
        collection_concept: An instance of concept that represents the collection of Named Individuals
        scope_notes: A set of one or more scope notes
        config: The config

    Returns:
        An instance of ``dataset2rdf.models.Restriction``

    """
    r_key = f"{concept.identifier}-{prop.identifier}"
    if r_key in concept.restrictions:
        restriction = concept.restrictions[r_key]
    else:
        restriction = Restriction(
            class_identifier=concept.identifier, property_identifier=prop.identifier
        )
        concept.restrictions[r_key] = restriction

    collection_concept_iri = f"{collection_concept.identifier}"
    restriction.some_values_from.add(collection_concept_iri)
    return restriction


def add_excluded_type_as_note(concept: Concept, property: Property, excluded_types: List, config = get_config()) -> None:
    """
    Add a scope note to a property that signifies that a given list
    of types are excluded as possible values.

    Args:
        concept: An instance of Concept
        Property: An instance of Property
        excluded_types: A list of excluded types
        config: The config

    """
    if property.identifier.startswith(config.iri.canonical_iri):
        property_prefix = config.prefix
        property_identifier = property.identifier.replace(config.iri.canonical_iri, '')
    else:
        property_identifier = property.identifier.replace(config.project_metadata.iri.canonical_iri, '')
        property_prefix = config.project_metadata.prefix

    if excluded_types:
        formatted_excluded_types = [format_class_name(x) for x in excluded_types]
        formatted_excluded_types_with_prefix = [f"{config.prefix}:{x}" if not is_curie(x) else x for x in formatted_excluded_types]
        note = f"For {property_prefix}:{property_identifier}, instances of {', '.join(formatted_excluded_types_with_prefix)} are not allowed"
        r_key = f"{concept.identifier}-{property.identifier}"
        if r_key in concept.restrictions:
            restriction = concept.restrictions[r_key]
            restriction.scope_notes.add(note)
        else:
            note_restriction = Restriction(
                class_identifier=concept.identifier,
                property_identifier=property.identifier,
                scope_notes=[note]
            )
            concept.restrictions[r_key] = note_restriction


def extract_code(code_str: str, pattern: str) -> str:
    """
    Extract a concept code from a given string based on a pattern.

    Args:
        code_str: The code string
        pattern: A regex pattern

    Returns:
        The concept code extracted using the pattern.

    """
    m = re.findall(pattern, code_str)
    if m: 
        code = m[0]
    else: 
        code = None
    if not code:
        raise ValueError(f"Could not extract code from '{code_str}' with regex '{pattern}'")
    return code


def format_named_individual(name: str) -> str:
    """
    Format Named Individuals.

    Args:
        name: The name of a named individual

    Returns:
        A formatted representation of the name

    """
    formatted_name = name
    char_map = {
        "%": "percent",
        "[": "sbl",
        "]": "sbr",
        "(": "",
        ")": "",
        "<=": "LessThanOrEqual",
        ">=": "GreaterThanOrEqual",
        "<": "LessThan",
        ">": "GreaterThan",
        "=": "Equal",
        ".": " dot ",
        "®": "",
        "+": "plus",
        "°": "degrees",
        "/": " ",
        ",": "",
        "'s": "",
        "Nonviable": "Non viable",
        "nonviable": "Non viable",
        "nonblood": "Non blood",
    }
    exception_map = {
        "Blood (whole)": "BloodWhole",
        "Cells from nonblood specimen type (e.g. dissociated tissue), nonviable": "CellsFromNonBloodSpecimenTypeNonViableTissue",
        "Cells from nonblood specimen type (e.g. dissociated tissue), viable": "CellFromNonBloodSpecimenTypeViableTissue",
    }

    if formatted_name in exception_map:
        formatted_name = exception_map[formatted_name]
    else:
        formatted_name = re.sub(r"\(e\.g\..+\)", " ", formatted_name)

        if "(-)" in formatted_name:
            formatted_name = formatted_name.replace("-", "minus")

        for k, v in char_map.items():
            if k in formatted_name:
                formatted_name = formatted_name.replace(k, v)

        if "-" in formatted_name:
            if formatted_name not in {"X-ray"}:
                formatted_name = formatted_name.replace("-", " ")

        if "  " in formatted_name:
            formatted_name = formatted_name.replace("  ", " ")

        new_name_substr = []
        if " " in formatted_name:
            for substr in formatted_name.split(" "):
                if substr:
                    if substr in {"mL", "uL", "dot"}:
                        new_name_substr.append(substr)
                    else:
                        if substr[0].istitle():
                            new_name_substr.append(substr)
                        else:
                            new_name_substr.append(substr.title())
            formatted_name = "".join(new_name_substr)
        else:
            if not formatted_name[0].istitle():
                formatted_name = formatted_name.title()

        if formatted_name == "PPTubeGreaterThanOrEqual5mL":
            formatted_name = "PPTube5mLOrAbove"
        if formatted_name == "24HUrine":
            formatted_name = "24hUrine"
        if formatted_name == "UrineRandom“Spot”":
            formatted_name = "UrineRandom"

    logger.debug(f"Formatting '{name}' to '{formatted_name}'")
    return formatted_name


def parse_code_from_sheet(
    filename: str, sheet_name: str, standard: str, imports: Dict, config: Config = get_config()
) -> List:
    """
    Parse codes from a specific sheet from a given Excel spreadsheet.

    Args:
        filename: The Excel filename
        sheet_name: The name of the sheet
        standard: The standard the codes belong to
        imports: A dictionary of imports
        config: The config

    Returns:
        A list of codes that belong to the ``standard`` as extracted
        from the spreadsheet.

    """
    codes: List = []
    df = pd.read_excel(filename, sheet_name, keep_default_na=False)
    df = df.applymap(sanitize)
    for record in df.to_dict('records'):
        if standard in record["coding system and version"]:
            code = record["identifier (zcode)"].strip()
            if code.startswith("Z"):
                code = code[1:]
            code_iri = imports[standard].iri.canonical_iri + code
            codes.append(code_iri)
    return codes


def parse_extendable_valueset(standard: str, values: List) -> List:
    """
    Parse extendable valuesets and return the list of codes.

    Args:
        standard: The standard
        values: The list of values

    Returns:
        A list of parsed values

    """
    recommended_codes = []
    for value in values:
        if "extendable value set" in value:
            c = value.split(':', 1)[1]
            if ':' in c:
                c_standard, c_identifier = [x.strip() for x in c.split(':', 1)]
            else:
                c_standard = standard
                c_identifier = c.strip()
            recommended_code = f"{c_standard}: {c_identifier}"
            recommended_codes.append(recommended_code)
        else:
            c = value
            if ':' in c:
                c_standard, c_identifier = [x.strip() for x in c.split(':', 1)]
            else:
                c_standard = standard
                c_identifier = c.strip()
            recommended_code = f"{c_standard}: {c_identifier}"
            recommended_codes.append(recommended_code)
    return recommended_codes


def get_reference(s: str) -> str:
    """
    Get the reference from a given IRI.

    Args:
        s: An IRI string

    Returns:
        The reference

    """
    elements = s.split("/")
    last_element = elements[-1]
    if "#" in last_element:
        reference = last_element.split("#")[1]
    else:
        reference = last_element
    return reference


def parse_restricted_to_expression(valueset: str) -> List:
    """
    Parse a given valueset string and check if there are 'restricted to' expressions.
    If so, then split the string at the right delimiter and return a list where each
    element is a 'restricted to' expression.

    Args:
        valueset: The valueset string

    Return:
        A list of 'restricted to' expressions

    """
    keywords = {'descendant of', 'child of', 'for '}
    prepared_valueset = []
    prepared_value = None
    for value in valueset.split(";"):
        if 'restricted to' in value:
            # First property path restriction
            if prepared_value:
                prepared_valueset.append(prepared_value)
            prepared_value = value
        elif any(x not in value for x in keywords):
            prepared_value += f";{value}"
    prepared_valueset.append(prepared_value)
    return prepared_valueset


def apply_inherited_restrictions(concepts: Dict, properties: Dict) -> None:
    """
    Apply inherited restrictions from parent to child concept.

    If a concept is a subClassOf another concept then pull down its parent's restrictions
    to the current concept.

    Args:
        concepts: A dictionary of concepts
        properties: A dictionary of properties

    """
    for concept in concepts.values():
        _apply_inherited_restrictions(concepts=concepts, properties=properties, concept=concept)


def _apply_inherited_restrictions(concepts: Dict, properties: Dict, concept: Concept) -> Dict:
    """
    Apply restrictions inherited from parent to a given concept.

    Args:
        concepts: A dictionary of concepts
        properties: A dictionary of properties
        concept: A concept

    Returns:
        A dictionary of updated restrictions for the concept

    """
    logger.debug(f"[_apply_inherited_restrictions] concept: {concept.identifier}")
    if concept.parent:
        new_restrictions = {}
        if concept.parent in concepts:
            parent_concept = concepts[concept.parent]
            restrictions_from_parent = _apply_inherited_restrictions(concepts, properties, parent_concept)
            for parent_restriction_key, parent_restriction in restrictions_from_parent.items():
                property_parents = {}
                for r in concept.restrictions.values():
                    p = properties[r.property_identifier]
                    property_parents[p.identifier] = p.parent
                if parent_restriction.property_identifier in {x.property_identifier for x in concept.restrictions.values()}:
                    # there exist a restriction on property in the current concept which is also in the parent
                    logger.debug(f"[_apply_inherited_restrictions] parent {parent_concept.identifier} has a restriction on property {parent_restriction.property_identifier} which is also in the current concept {concept.identifier}")
                    for restriction_key, restriction in concept.restrictions.items():
                        if parent_restriction.property_identifier == restriction.property_identifier:
                            if restriction.min_cardinality is None and parent_restriction.min_cardinality is not None:
                                logger.debug(f"[_apply_inherited_restrictions] Updating min_cardinality for concept from parent from {restriction.min_cardinality} to {parent_restriction.min_cardinality}")
                                restriction.min_cardinality = parent_restriction.min_cardinality
                            if restriction.max_cardinality is None and parent_restriction.max_cardinality is not None:
                                logger.debug(f"[_apply_inherited_restrictions] Updating max_cardinality for concept from parent from {restriction.max_cardinality} to {parent_restriction.max_cardinality}")
                                restriction.max_cardinality = parent_restriction.max_cardinality
                            if not restriction.has_value and parent_restriction.has_value:
                                logger.debug(f"[_apply_inherited_restrictions] Updating has_value restriction for concept from parent from {restriction.has_value} to {parent_restriction.has_value}")
                                restriction.has_value = parent_restriction.has_value
                            if not restriction.some_values_from and parent_restriction.some_values_from:
                                logger.debug(f"[_apply_inherited_restrictions] Updating some_values_from restriction for concept from parent from {restriction.some_values_from} to {parent_restriction.some_values_from}")
                                restriction.some_values_from = parent_restriction.some_values_from
                            if parent_restriction.property_paths:
                                new_pprs = set(parent_restriction.property_paths.keys())
                                for parent_ppr_key, parent_ppr_restriction in parent_restriction.property_paths.items():
                                    for ppr_key, ppr_restriction in restriction.property_paths.items():
                                        if parent_ppr_restriction.property_path == ppr_restriction.property_path:
                                            new_pprs.remove(parent_ppr_key)
                                if new_pprs:
                                    for new_ppr_key in new_pprs:
                                        parent_ppr = parent_restriction.property_paths[new_ppr_key]
                                        key = f"{restriction.class_identifier}-{restriction.property_identifier}-{'-'.join(parent_ppr.property_path)}"
                                        logger.debug(f"[_apply_inherited_restrictions] Updating property path restrictions for concept from parent. Adding {parent_ppr.dict()} to concept")
                                        restriction.property_paths[key] = parent_ppr
                elif parent_restriction.property_identifier in property_parents.values():
                    # there exist a restriction on a property in the current concept which is a subproperty of a property in the parent
                    logger.debug(f"In subclass {concept.identifier}, there exists a property which is a subproperty of {parent_restriction.property_identifier} from the parent class {parent_restriction.class_identifier}")
                    # merge non-conflicting restrictions
                    for restriction_key, restriction in concept.restrictions.items():
                        if parent_restriction.property_identifier == property_parents[restriction.property_identifier]:
                            if restriction.min_cardinality is None and parent_restriction.min_cardinality is not None:
                                logger.debug(f"[_apply_inherited_restrictions] Updating min_cardinality for concept from parent from {restriction.min_cardinality} to {parent_restriction.min_cardinality}")
                                restriction.min_cardinality = parent_restriction.min_cardinality
                            if restriction.max_cardinality is None and parent_restriction.max_cardinality is not None:
                                logger.debug(f"[_apply_inherited_restrictions] Updating max_cardinality for concept from parent from {restriction.max_cardinality} to {parent_restriction.max_cardinality}")
                                restriction.max_cardinality = parent_restriction.max_cardinality
                            if not restriction.has_value and parent_restriction.has_value:
                                logger.debug(f"[_apply_inherited_restrictions] Updating has_value restriction for concept from parent from {restriction.has_value} to {parent_restriction.has_value}")
                                restriction.has_value = parent_restriction.has_value
                            if not restriction.some_values_from and parent_restriction.some_values_from:
                                logger.debug(f"[_apply_inherited_restrictions] Updating some_values_from restriction for concept from parent from {restriction.some_values_from} to {parent_restriction.some_values_from}")
                                restriction.some_values_from = parent_restriction.some_values_from
                            if parent_restriction.property_paths:
                                new_pprs = set(parent_restriction.property_paths.keys())
                                for parent_ppr_key, parent_ppr_restriction in parent_restriction.property_paths.items():
                                    for ppr_key, ppr_restriction in restriction.property_paths.items():
                                        if parent_ppr_restriction.property_path == ppr_restriction.property_path:
                                            new_pprs.remove(parent_ppr_key)
                                if new_pprs:
                                    for new_ppr_key in new_pprs:
                                        parent_ppr = parent_restriction.property_paths[new_ppr_key]
                                        key = f"{restriction.class_identifier}-{restriction.property_identifier}-{'-'.join(parent_ppr.property_path)}"
                                        logger.debug(f"[_apply_inherited_restrictions] Updating property path restrictions for concept from parent. Adding {parent_ppr.dict()} to concept")
                                        restriction.property_paths[key] = parent_ppr
                else:
                    logger.debug(f"[_apply_inherited_restrictions] concept: {concept.identifier} does not have a restriction on property {parent_restriction.property_identifier}; pulling down restrictions from parent: {parent_restriction.class_identifier}")
                    new_restriction = Restriction(
                        class_identifier=concept.identifier,
                        property_identifier=parent_restriction.property_identifier,
                        min_cardinality=parent_restriction.min_cardinality,
                        max_cardinality=parent_restriction.max_cardinality,
                        some_values_from=parent_restriction.some_values_from,
                        has_value=parent_restriction.has_value,
                        property_paths=parent_restriction.property_paths
                    )
                    key = f"{concept.identifier}-{parent_restriction.property_identifier}"
                    new_restrictions[key] = new_restriction
            concept.restrictions.update(new_restrictions)
    return concept.restrictions


def get_iso_datetime():
    return datetime.datetime.now().replace(microsecond=0).isoformat()
